import { getData, postData, putData, deleteData } from './index';
import types from './types';

// Customer actions
export function createCustomer(stripeToken, plan, lastFour) {
  const data = { stripeToken, plan, lastFour };
  const url = '/pay/customer';
  return dispatch => postData(types.CREATE_CUSTOMER, types.BILLING_ERROR, true, url, dispatch, data);
}

export function fetchCustomer() {
  const url = '/pay/customer';
  return dispatch => getData(types.FETCH_CUSTOMER, types.BILLING_ERROR, true, url, dispatch);
}

export function cancelSubscription() {
  const url = '/pay/customer';
  return dispatch => getData(types.CANCEL_SUBSCRIPTION, types.BILLING_ERROR, true, url, dispatch);
}

export function updateSubscription(newPlan) {
  const data = { newPlan };
  const url = '/pay/subscription';
  return dispatch => putData(types.CHANGE_SUBSCRIPTION, types.BILLING_ERROR, true, url, dispatch, data);
}

export function updateBilling(stripeToken) {
  const data = { stripeToken };
  const url = '/pay/customer';
  return dispatch => putData(types.UPDATE_BILLING, types.BILLING_ERROR, true, url, dispatch, data);
}
