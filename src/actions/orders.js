import { API_URL, CLIENT_ROOT_URL, errorHandler } from './index';
import types from './types';
import { action } from '../reducers/app';

export function fetchOrders() {
  return function (dispatch) {
    return fetch(API_URL + '/orders/provider', {
      headers: {
        'content-type': 'application/json'
      },
      method: 'GET',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
    }).then((response) => {
      response.json().then(json => {
        dispatch({
          type: types.FETCH_ORDERS,
          payload: json // json.user,
        });
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}

export function fetchOrder() {
  return function (dispatch) {
    return fetch(API_URL + '/orders/me', {
      headers: {
        'content-type': 'application/json'
      },
      method: 'GET',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
    }).then((response) => {
      response.json().then(json => {
        dispatch({
          type: types.FETCH_ORDER,
          payload: json // json.user,
        });
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}

export function fetchEditOrder(order) {
  return function (dispatch) {
    return fetch(API_URL + '/orders/' + order._id, {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      method: 'PUT',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      body: JSON.stringify(order),
      withCredentials: true,
      withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        dispatch({ type: types.FETCH_ORDERS });
        if (response.ok) {
          dispatch(action.showNotification('Successfully edited order!', true));
          window.location.href = CLIENT_ROOT_URL + '/dashboard';
        }
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch((error) => {
      errorHandler(dispatch, error.response, types.AUTH_ERROR);
    });
  };
}

export function fetchRemoveOrder(order) {
  return function (dispatch) {
    return fetch(API_URL + '/orders/' + order._id, {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      method: 'DELETE',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      body: JSON.stringify(order),
      withCredentials: true,
      withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        dispatch({ type: types.REMOVE_ORDER });
        if (response.ok) {
          dispatch(action.showNotification('Successfully deleted order!', true));
          window.location.href = CLIENT_ROOT_URL + '/dashboard';
        }
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch((error) => {
      errorHandler(dispatch, error.response, types.AUTH_ERROR);
    });
  };
}
