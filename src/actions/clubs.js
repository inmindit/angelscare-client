import { API_URL, CLIENT_ROOT_URL, errorHandler } from './index';
import types from './types';
import { action } from '../reducers/app';

export function fetchClubs() {
  return function (dispatch) {
    return fetch(API_URL + '/clubs', {
      headers: {
        'content-type': 'application/json'
      },
      method: 'GET',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        dispatch({
          type: types.FETCH_CLUBS,
          payload: json // json.user,
        });
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}

export function fetchCreateClubs(clubs) {
  console.log('createClubs:', JSON.stringify(clubs));
  return function (dispatch) {
    return fetch(API_URL + '/clubs', {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      method: 'POST',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      body: JSON.stringify(clubs),
      withCredentials: true,
      withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        dispatch({ type: types.CREATE_CLUBS });
        if (response.ok) {
          if (clubs.image.name) {
            fetchClubsPicture('POST', { _id: json._id, image: clubs.image }, dispatch);
            dispatch(action.showNotification('Successfully created club!', true));
          } else {
            dispatch(action.showNotification('Successfully uploaded news!', true));
            window.location.href = CLIENT_ROOT_URL + '/';
          }
        }
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch((error) => { errorHandler(dispatch, error.response, types.AUTH_ERROR); });
  };
}

export function fetchClub(clubs) {
  console.log('editClubs:', JSON.stringify(clubs));
  return function (dispatch) {
    return fetch(API_URL + '/clubs/' + clubs._id, {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      method: 'POST',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      body: JSON.stringify(clubs),
      withCredentials: true,
      withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        console.log('editClubs json', json);
        dispatch({ type: types.FETCH_CLUB });
        if (response.ok) {
          if (clubs.image.hasOwnProperty('name')) {
            fetchClubsPicture('POST', { _id: json._id, image: clubs.image }, dispatch);
            dispatch(action.showNotification('Successfully edited clubs!', true));
          } else {
            dispatch(action.showNotification('Successfully edited clubs!', true));
            // window.location.href = CLIENT_ROOT_URL + '/';
          }
        }
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch((error) => { errorHandler(dispatch, error.response, types.AUTH_ERROR); });
  };
}

export function fetchRemoveClub(event) {
  console.log('deleteClubs:', JSON.stringify(event));
  return function (dispatch) {
    return fetch(API_URL + '/clubs/' + event._id, {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      method: 'POST',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      body: JSON.stringify(event),
      withCredentials: true,
      withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        console.log('json', json);
        dispatch({ type: types.REMOVE_CLUBS });
        if (response.ok) {
          dispatch(action.showNotification('Successfully deleted clubs!', true));
          // window.location.href = CLIENT_ROOT_URL + '/';
        }
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch((error) => { errorHandler(dispatch, error.response, types.AUTH_ERROR); });
  };
}

export function fetchClubsPicture(method, club, dispatch) {
  let data = new FormData();
  data.set('clubs', club.image);
  return fetch(API_URL + '/uploads/clubs/picture/' + club._id, {
    // headers: {
    //   'Content-Type': 'multipart/form-data',
    //   'Accept': 'application/json'
    // },
    method: method,
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, same-origin, *omit
    withCredentials: true,
    body: data,
    withAuthentication: true
  }).then((response) => {
    response.json().then(json => {
      dispatch({
        type: types.FETCH_NEWS_PICTURE,
        payload: json // json.user,
      });
      if (response.ok) {
        dispatch(action.showNotification('Successfully created clubs!', true));
        window.location.href = CLIENT_ROOT_URL + '/';
      }
    })
  }).then(({ json, response }) => {
    if (!response.ok) {
      return Promise.reject(json);
    }
    return json;
  }).then(
    response => response,
    error => error
  ).catch(response => dispatch(errorHandler(response.data.error)));
}

export function fetchEditClubPromise(club) {
  console.log('editNews:', JSON.stringify(club));
  return function (dispatch) {
    return fetch(API_URL + '/clubs/' + club._id, {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      method: 'PUT',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      body: JSON.stringify(club),
      withCredentials: true,
      withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        console.log('json', json);
        dispatch({ type: types.EDIT_CLUBS });
        if (response.ok) {
          fetchClubsPicture('POST', { _id: json._id, image: club.image }, dispatch);
        }
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch((error) => {
      errorHandler(dispatch, error.response, types.AUTH_ERROR);
    });
  };
}
