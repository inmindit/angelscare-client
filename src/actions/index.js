// import { logoutUser } from './auth';
import types from './types';
import { action } from '../reducers/app';
import cookie from "react-cookie";

// Set Development modes checks
const isProdMode = (process.env.NODE_ENV === 'production');

// let API_URL = 'http://localhost:8080/api';
// let CLIENT_ROOT_URL = 'http://localhost:8080'; // Should to run locally on http
let API_URL = '/api';
let CLIENT_ROOT_URL = ''; // Should to run locally on http

// console.log(process.env.NODE_ENV);

if (isProdMode) {
  API_URL = 'http://18.184.177.91:8080/api';
  CLIENT_ROOT_URL = 'http://18.184.177.91:8080'; // Should to run locally on http
}
export { API_URL, CLIENT_ROOT_URL };

// axios.defaults.withAuthentication = true;

export function fetchUser(method, body) {
  return function (dispatch) {
    return fetch(API_URL + '/users/me', {
      headers: { 'content-type': 'application/json' },
      method: method,
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      body: JSON.stringify(body),
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        console.log('fetchUser json', json);
        // cookie.save('sessionID', json.user.sessionID, { path: '/profile' });
        // cookie.save('user', json, { path: '/profile' });
        dispatch({
          type: types.FETCH_USER,
          payload: json // json.user,
        });
        if (method === 'PUT') {
          if (body.password.length > 0) {
            fetchUpdatePassword('POST', { password: body.password, confirmPassword: body.confirmPassword }, dispatch);
          }
          dispatch(action.showNotification('Successfully updated profile!', true));
        }
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}

export function fetchLanguage(method, body) {
  // return function (dispatch) {
  return fetch(API_URL + '/users/me/language', {
    headers: { 'content-type': 'application/json' },
    method: method,
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, same-origin, *omit
    withCredentials: true,
    body: JSON.stringify(body),
    // withAuthentication: true
  }).then((response) => {
    console.log('fetchLanguage json', response);
    response.json().then(json => {
      let user = cookie.load('user');
      user.settings = json.settings;
      cookie.save('user', user, { path: '/' });
      console.log('fetchLanguage json', json);
    })
  }).then(({ json, response }) => {
    if (!response.ok) {
      return Promise.reject(json);
    }

    return json;
  }).then(
    response => response,
    error => error
  )
  // .catch(response => dispatch(errorHandler(response.data.error)));
  // };
}

export function fetchUpdatePassword(method, body, dispatch) {
  return fetch(API_URL + '/users/password', {
    headers: { 'content-type': 'application/json' },
    method: method,
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, same-origin, *omit
    withCredentials: true,
    body: JSON.stringify(body),
    // withAuthentication: true
  }).then((response) => {
    if (!response.ok) {
      response.json().then(json => {
        // console.log(json.message);
        // errorHandler(response.data.error)
      });
    } else {
      response.json().then(json => {
        dispatch({
          type: types.FETCH_UPDATE_PASSWORD,
          payload: json // json.user,
        });
      })
    }
  }).then(({ json, response }) => {
    if (!response.ok) {
      return Promise.reject(json);
    }
    return json;
  }).then(
    response => response,
    error => error
  ).catch(response => dispatch(errorHandler(response.data.error)));
}

export function fetchSendContact(body) {
  return function (dispatch) {
    return fetch(API_URL + '/contact', {
      headers: { 'content-type': 'application/json' },
      method: 'POST',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      body: JSON.stringify(body),
      // withAuthentication: true
    }).then((response) => {
      if (!response.ok) {
        response.json().then(json => {
        });
      } else {
        response.json().then(json => {
          dispatch({
            type: types.SEND_CONTACT_FORM,
            payload: json // json.user,
          });
        })
      }
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  }
}

export function errorHandler(dispatch, error, type) {
  console.log('Error type: ', type);
  console.log(error);

  let errorMessage = error.error ? error.error : error;

  // NOT AUTHENTICATED ERROR
  if (error.status === 401 || error.response.status === 401) {
    errorMessage = 'You are not authorized to do this.';
    // return dispatch(logoutUser(errorMessage));
  }

  dispatch({
    type,
    payload: errorMessage,
  });
}

// Post Request
/*export function postData(action, errorType, isAuthReq, url, dispatch, data) {
  const requestUrl = API_URL + url;
  let headers = {};

  if (isAuthReq) {
    headers = { withAuthentication: true };
  }

  axios.post(requestUrl, data, headers)
  .then((response) => {
    dispatch({
      type: action,
      payload: response.data,
    });
  })
  .catch((error) => {
    errorHandler(dispatch, error, errorType);
  });
}*/

// Get Request
/*export function getData(action, errorType, isAuthReq, url, dispatch) {
  const requestUrl = API_URL + url;
  let headers = {};

  if (isAuthReq) {
    headers = { withAuthentication: true };
  }

  axios.get(requestUrl, headers)
  .then((response) => {
    dispatch({
      type: action,
      payload: response.data,
    });
  })
  .catch((error) => {
    errorHandler(dispatch, error, errorType);
  });
}*/

// Put Request
/*export function putData(action, errorType, isAuthReq, url, dispatch, data) {
  const requestUrl = API_URL + url;
  let headers = {};

  if (isAuthReq) {
    headers = { withAuthentication: true };
  }

  axios.put(requestUrl, data, headers)
  .then((response) => {
    dispatch({
      type: action,
      payload: response.data,
    });
  })
  .catch((error) => {
    errorHandler(dispatch, error, errorType);
  });
}*/

// Delete Request
/*export function deleteData(action, errorType, isAuthReq, url, dispatch) {
  const requestUrl = API_URL + url;
  let headers = {};

  if (isAuthReq) {
    headers = { withAuthentication: true };
  }

  axios.delete(requestUrl, headers)
  .then((response) => {
    dispatch({
      type: action,
      payload: response.data,
    });
  })
  .catch((error) => {
    errorHandler(dispatch, error, errorType);
  });
}*/

// Static Page actions
/*export function sendContactForm({ name, emailAddress, message }) {
  return function (dispatch) {
    axios.post(API_URL + '/communication/contact', { name, emailAddress, message })
    .then((response) => {
      dispatch({
        type: types.SEND_CONTACT_FORM,
        payload: response.data.message,
      });
    })
    .catch((error) => {
      errorHandler(dispatch, error, types.STATIC_ERROR);
    });
  };
}*/
