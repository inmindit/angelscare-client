// import { browserHistory } from 'react-router';
import { API_URL, errorHandler } from './index';
import types from './types';
import { action } from '../reducers/app';

export function fetchInviteUser(body) {
  return function (dispatch) {
    return fetch(API_URL + '/admin/user/invite', {
      headers: {'content-type': 'application/json'},
      method: 'POST',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      body: JSON.stringify(body),
      withCredentials: true,
    }).then((response) => {
      if (!response.ok) {
        response.json().then(error => {
          errorHandler(dispatch, {
            errorMessage: error,
            status: response.status
          }, types.AUTH_ERROR);
        });
      } else {
        response.json().then(json => {
          console.log('INVITE USER:', json);
          dispatch({
            type: types.INVITE_USER,
            payload: json // json.user,
          });
        });
      }
    }).then(({json, response}) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch((error) => {
      errorHandler(dispatch, error, types.AUTH_ERROR);
    });
  };
}

export function fetchAllUsers() {
  return function (dispatch) {
    return fetch(API_URL + '/admin/users', {
      headers: {
        'content-type': 'application/json'
      },
      method: 'GET',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        // cookie.save('sessionID', json.user.sessionID, { path: '/admin' });
        // cookie.save('user', json, { path: '/admin' });
        dispatch({
          type: types.FETCH_ALL_USERS,
          payload: json // json.user,
        });
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}

export function fetchUserByAdmin(method, body) {
  return function (dispatch) {
    return fetch(API_URL + '/admin/users/:clientId', {
      headers: {
        'content-type': 'application/json'
      },
      method: method,
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      body: JSON.stringify(body),
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        if (method === 'PUT') {
          dispatch({
            type: types.SET_USER_ROLE,
            payload: json // json.user,
          });
          dispatch(action.showNotification('Successfully updated user role!', true));
        } else if (method === 'DELETE') {
          dispatch({
            type: types.REMOVE_USER,
            payload: json // json.user,
          });
          dispatch(action.showNotification('Successfully deleted user!', true));
        } else {
          dispatch({
            type: types.GET_USER,
            payload: json // json.user,
          });
        }
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}

