import { API_URL, CLIENT_ROOT_URL, errorHandler } from './index';
import types from './types';
import { action } from '../reducers/app';

export function fetchNews() {
  return function (dispatch) {
    return fetch(API_URL + '/news', {
      headers: {
        'content-type': 'application/json'
      },
      method: 'GET',
      cache: 'no-cache',
      credentials: 'same-origin',
      withCredentials: true,
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        dispatch({
          type: types.FETCH_NEWS,
          payload: json // json.user,
        });
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}

export function fetchCreateNews(news) {
  console.log('createNews:', news);
  return function (dispatch) {
    return fetch(API_URL + '/news', {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      method: 'POST',
      cache: 'no-cache',
      credentials: 'same-origin',
      body: JSON.stringify(news),
      withCredentials: true,
      withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        console.log('json', json);
        dispatch({ type: types.CREATE_NEWS });
        if (response.ok) {
          if (news.image && news.image.name) {
            fetchNewsPicture('POST', { _id: json._id, image: news.image }, dispatch);
          } else {
            window.location.href = CLIENT_ROOT_URL + '/';
          }
          dispatch(action.showNotification('Successfully uploaded news!', true));
        }
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch((error) => {
      errorHandler(dispatch, error.response, types.AUTH_ERROR);
    });
  };
}

export function fetchEditNewsPromise(news) {
  console.log('editNews:', JSON.stringify(news));
  return function (dispatch) {
    return fetch(API_URL + '/news/' + news._id, {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      method: 'PUT',
      cache: 'no-cache',
      credentials: 'same-origin',
      body: JSON.stringify(news),
      withCredentials: true,
      withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        console.log('json', json);
        dispatch({ type: types.CREATE_NEWS });
        if (response.ok) {
          fetchNewsPicture('POST', { _id: json._id, image: news.image }, dispatch);
          window.location.href = CLIENT_ROOT_URL + '/';
        }
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch((error) => {
      errorHandler(dispatch, error.response, types.AUTH_ERROR);
    });
  };
}

export function fetchRemoveNews(news) {
  return function (dispatch) {
    return fetch(API_URL + '/news/' + news._id, {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      method: 'DELETE',
      cache: 'no-cache',
      credentials: 'same-origin',
      body: JSON.stringify(news),
      withCredentials: true,
      withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        console.log('json', json);
        dispatch({ type: types.REMOVE_NEWS });
        if (response.ok) {
          dispatch(action.showNotification('Successfully deleted news!', true));
        }
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch((error) => {
      errorHandler(dispatch, error.response, types.AUTH_ERROR);
    });
  };
}

export function fetchNewsPicture(method, news, dispatch) {
  let data = new FormData();
  data.set('news', news.image);
  return fetch(API_URL + '/uploads/news/picture/' + news._id, {
    // headers: {
    //   'Content-Type': 'multipart/form-data',
    //   'Accept': 'application/json'
    // },
    method: method,
    cache: 'no-cache',
    credentials: 'same-origin',
    withCredentials: true,
    body: data,
    withAuthentication: true
  }).then((response) => {
    response.json().then(json => {
      dispatch({
        type: types.FETCH_NEWS_PICTURE,
        payload: json // json.user,
      });
      if (response.ok) {
        dispatch(action.showNotification('Successfully uploaded news!', true));
      }

    })
  }).then(({ json, response }) => {
    if (!response.ok) {
      return Promise.reject(json);
    }
    return json;
  }).then(
    response => response,
    error => error
  ).catch(response => dispatch(errorHandler(response.data.error)));
}

export function fetchNewsById(method, newsId) {
  return function (dispatch) {
    return fetch(API_URL + '/news/' + newsId, {
      headers: { 'content-type': 'application/json' },
      method: method,
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        dispatch({
          type: types.FETCH_NEWSBYID,
          payload: json // json.user,
        });
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}
