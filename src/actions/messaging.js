// import { reset } from 'redux-form';
import { browserHistory } from 'react-router';
// import io from 'socket.io-client';
import { getData, postData, putData, deleteData } from './index';
import types from './types';

// Connect to socket.io server
// export const socket = io.connect('http://localhost:3000');


// Messaging actions
export function fetchConversations() {
  const url = '/chat';
  return dispatch => getData(types.FETCH_CONVERSATIONS, types.CHAT_ERROR, true, url, dispatch);
}

export function fetchConversation(conversation) {
  const url = '/chat/' + conversation;
  return dispatch => getData(types.FETCH_SINGLE_CONVERSATION, types.CHAT_ERROR, true, url, dispatch);
}

export function startConversation({ recipient, composedMessage }) {
  const data = { composedMessage };
  const url = '/chat/new/' + recipient;
  return (dispatch) => {
    postData(types.START_CONVERSATION, types.CHAT_ERROR, true, url, dispatch, data);

    // Clear form after message is sent
    // dispatch(reset('composeMessage'));
    browserHistory.push('/dashboard/conversation/view/' + response.data.conversationId);
  };
}

export function fetchRecipients() {
  const url = '/chat/recipients';
  return dispatch => getData(types.FETCH_RECIPIENTS, types.CHAT_ERROR, true, url, dispatch);
}

export function sendReply(replyTo, { composedMessage }) {
  const data = { composedMessage };
  const url = '/chat/' + replyTo;
  return (dispatch) => {
    postData(types.SEND_REPLY, types.CHAT_ERROR, true, url, dispatch, data);

    // Clear form after message is sent
    // dispatch(reset('replyMessage'));
    // socket.emit('new message', replyTo);
  };
}
