// import { browserHistory } from 'react-router';
import cookie from 'react-cookie';
import { API_URL, CLIENT_ROOT_URL, errorHandler } from './index';
import types from './types';

// Authentication actions
// TODO Add expiration to cookie
export function fetchSignIn(data) {
  return function (dispatch) {
    return fetch(API_URL + '/auth/signin', {
      headers: {'content-type': 'application/json'},
      method: 'POST',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      body: JSON.stringify(data),
      withCredentials: true,
      // withAuthentication: true
    }).then((response) => {
      console.log('loginUser', response);
      if (!response.ok) {
        response.json().then(error => {
          errorHandler(dispatch, {
            errorMessage: error,
            status: response.status
          }, types.AUTH_ERROR);
        });
      } else {
        response.json().then(json => {
          cookie.save('sessionID', json.user.sessionID, {path: '/'});
          cookie.save('user', json.user, {path: '/'});
          dispatch({type: types.AUTH_USER});
          window.location.href = CLIENT_ROOT_URL + '/dashboard';
        })
      }
    }).then(({json, response}) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch((error) => {
      errorHandler(dispatch, error, types.AUTH_ERROR);
    });
  };
}

export function fetchSignUp({email, firstName, lastName, username, password}) {
  return function (dispatch) {
    return fetch(API_URL + '/auth/signup', {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      method: 'POST',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      body: JSON.stringify({email, firstName, lastName, username, password}),
      withCredentials: true,
      withAuthentication: true
    }).then((response) => {
      if (!response.ok) {
        response.json().then(json => {
          errorHandler(dispatch, json.message, types.AUTH_ERROR);
        });
      } else {
        response.json().then(json => {
          // cookie.save('sessionID', json.user.sessionID, { path: '/dashboard' });
          cookie.save('user', json.user, { path: '/' });
          dispatch({type: types.AUTH_USER});
          window.location.href = CLIENT_ROOT_URL + '/dashboard';
        })
      }
    }).then(({json, response}) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch((error) => {
      errorHandler(dispatch, error.response, types.AUTH_ERROR);
    });
  };
}

export function fetchLogout(error) {
  return function (dispatch) {
    dispatch({type: types.UNAUTH_USER, payload: error || ''});
    cookie.remove('sessionId', {path: '/'});
    cookie.remove('user', {path: '/'});
    window.location.href = CLIENT_ROOT_URL + '/login';
  };
}

export function fetchGetForgotPasswordToken({ email }) {
  return function (dispatch) {
    return fetch(API_URL + '/auth/forgot-password', {
      headers: {
        'content-type': 'application/json'
      },
      method: 'POST',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      body: email,
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        dispatch({
          type: types.FORGOT_PASSWORD_REQUEST,
          payload: json // json.user,
        });
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}

export function fetchResetPassword(token, { password }) {
  return function (dispatch) {
    return fetch(API_URL + '/auth/reset-password/' + token, {
      headers: {
        'content-type': 'application/json'
      },
      method: 'POST',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      body: { password },
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        dispatch({
          type: types.RESET_PASSWORD_REQUEST,
          payload: json // json.user,
        });
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}
