// import { browserHistory } from 'react-router';
// import cookie from 'react-cookie';
import { API_URL, CLIENT_ROOT_URL, errorHandler } from './index';
import types from './types';
import { action } from '../reducers/app';

export function fetchAllServices() {
  return function (dispatch) {
    return fetch(API_URL + '/services', {
      headers: {
        'content-type': 'application/json'
      },
      method: 'GET',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        dispatch({
          type: types.FETCH_SERVICES,
          payload: json // json.user,
        });
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}

export function fetchCreateService(event) {
  console.log('createService:', JSON.stringify(event));
  return function (dispatch) {
    return fetch(API_URL + '/services', {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      method: 'POST',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      body: JSON.stringify(event),
      withCredentials: true,
      withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        console.log('json', json);
        dispatch({ type: types.CREATE_SERVICE });
        if (response.ok) {
          fetchPicture('POST', { _id: json._id, image: event.image }, dispatch);
          window.location.href = CLIENT_ROOT_URL + '/';
        }
        dispatch(action.showNotification('Successfully created service!', true));
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch((error) => {
      errorHandler(dispatch, error.response, types.AUTH_ERROR);
    });
  };
}

export function fetchPicture(method, event, dispatch) {
  let data = new FormData();
  data.set('service', event.image);
  return fetch(API_URL + '/uploads/services/picture/' + event._id, {
    // headers: {
    //   'Content-Type': 'multipart/form-data',
    //   'Accept': 'application/json'
    // },
    method: method,
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, same-origin, *omit
    withCredentials: true,
    body: data,
    withAuthentication: true
  }).then((response) => {
    response.json().then(json => {
      dispatch({
        type: types.FETCH_SERVICE_PICTURE,
        payload: json // json.user,
      });
    })
  }).then(({ json, response }) => {
    if (!response.ok) {
      return Promise.reject(json);
    }
    return json;
  }).then(
    response => response,
    error => error
  ).catch(response => dispatch(errorHandler(response.data.error)));
}

export function orderRequest(event) {
  return function (dispatch) {
    return fetch(API_URL + '/orders/me', {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      method: 'POST',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      body: JSON.stringify(event),
      withCredentials: true,
      withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        console.log('json', json);
        dispatch({ type: types.ORDER_SERVICE });
        // window.location.href = CLIENT_ROOT_URL + '/admin/events/create';
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      dispatch(action.showNotification('Successfully created order!', true));
      return json;
    }).then(
      response => response,
      error => error
    ).catch((error) => {
      errorHandler(dispatch, error.response, types.AUTH_ERROR);
    });
  };
}

export function fetchServices(method, body) {
  return function (dispatch) {
    let currentURL = '';
    if (method === 'POST') {
      currentURL = '/services';
    } else if (method === 'DELETE') {
      currentURL = '/services/' + body._id;
    } else if (method === 'GET') {
      currentURL = '/services';
    } else if (method === 'PUT') {
      currentURL = '/services/' + body._id;
    }
    return fetch(API_URL + currentURL, {
      headers: { 'content-type': 'application/json' },
      method: method,
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      body: JSON.stringify(body),
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        console.log('json', json);
        dispatch({
          type: types.FETCH_SERVICES,
          payload: json // json.user,
        });
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}

export function searchServices(method, body) {
  return function (dispatch) {
    let currentURL = '';
    if (method === 'POST') {
      currentURL = '/services/search';
    } else {

    }
    return fetch(API_URL + currentURL, {
      headers: { 'content-type': 'application/json' },
      method: method,
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      body: JSON.stringify(body),
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        console.log('json', json);
        dispatch({
          type: types.FETCH_SERVICES,
          payload: json // json.user,
        });
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}

export function fetchServiceCategory(method, body) {
  return function (dispatch) {
    let currentURL = '';
    if (method === 'POST') {
      currentURL = '/categories/category';
    } else if (method === 'DELETE') {
      currentURL = '/categories/category/' + body._id;
    } else if (method === 'GET') {
      currentURL = '/categories';
    } else if (method === 'PUT') {
      currentURL = '/categories/category/' + body._id;
    }
    return fetch(API_URL + currentURL, {
      headers: { 'content-type': 'application/json' },
      method: method,
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      body: JSON.stringify(body),
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        console.log('fetchServiceCategory - json', json);
        dispatch({
          type: types.FETCH_SERVICES_CATEGORIES,
          payload: json // json.user,
        });
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}

export function fetchCategoryTranslation(method, body, id) {
  return function (dispatch) {
    let currentURL = '';
    if (method === 'POST' || method === 'DELETE' || method === 'GET') {
      return;
    } else if (method === 'PUT') {
      currentURL = '/categories/category/' + id + '/translation';
    }
    return fetch(API_URL + currentURL, {
      headers: { 'content-type': 'application/json' },
      method: method,
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      body: JSON.stringify(body),
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        console.log('fetchServiceCategory - json', json);
        dispatch({
          type: types.FETCH_SERVICES_CATEGORIES,
          payload: json // json.user,
        });
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}

export function fetchServiceLanguage(method, body, id) {
  return function (dispatch) {
    let currentURL = '';
    if (method === 'POST') {
      currentURL = '/language';
    } else if (method === 'DELETE') {
      currentURL = '/language/' + id;
    } else if (method === 'GET') {
      currentURL = '/language';
    } else if (method === 'PUT') {
      currentURL = '/language/' + id;
    }
    return fetch(API_URL + currentURL, {
      headers: { 'content-type': 'application/json' },
      method: method,
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      body: JSON.stringify(body),
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        console.log('fetchServiceCategory - json', json);
        dispatch({
          type: types.FETCH_SERVICES_LANGUAGES,
          payload: json // json.user,
        });
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}

export function fetchServicesTypes(method, body) {
  return function (dispatch) {
    let currentURL = '';
    if (method === 'POST') {
      currentURL = '/types';
    } else if (method === 'DELETE') {
      currentURL = '/types/type/' + body._id;
    } else if (method === 'GET') {
      currentURL = '/types';
    } else if (method === 'PUT') {
      currentURL = '/types/type/' + body._id;
    }
    return fetch(API_URL + currentURL, {
      headers: { 'content-type': 'application/json' },
      method: method,
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      body: JSON.stringify(body),
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        console.log('json', json);
        if (method === 'POST') {
          dispatch({
            type: types.FETCH_SERVICES_TYPES,
            payload: json // json.user,
          });
        } else if (method === 'DELETE') {
          dispatch({
            type: types.DELETE_SERVICES_TYPES,
            payload: json // json.user,
          });
        } else if (method === 'GET') {
          dispatch({
            type: types.GET_SERVICES_TYPES,
            payload: json // json.user,
          });
        } else if (method === 'PUT') {
          dispatch({
            type: types.FETCH_SERVICES_TYPES,
            payload: json // json.user,
          });
        }
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}

export function fetchGetServiceType() {
  return function (dispatch) {
    return fetch(API_URL + '/types/current', {
      headers: { 'content-type': 'application/json' },
      method: 'GET',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        console.log('json', json);
        dispatch({
          type: types.FETCH_MAIN_SERVICE_TYPE,
          payload: json // json.user,
        });
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}

export function fetchTypeTranslation(method, body, id) {
  return function (dispatch) {
    let currentURL = '';
    if (method === 'POST' || method === 'DELETE' || method === 'GET') {
      return;
    } else if (method === 'PUT') {
      currentURL = '/types/type/' + id + '/translation';
    }
    return fetch(API_URL + currentURL, {
      headers: { 'content-type': 'application/json' },
      method: method,
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      body: JSON.stringify(body),
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        console.log('fetchServiceType - json', json);
        dispatch({
          type: types.FETCH_SERVICES_TYPES,
          payload: json // json.user,
        });
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}


export function fetchGroupServices() {
  return function (dispatch) {
    return fetch(API_URL + '/services/current', {
      headers: { 'content-type': 'application/json' },
      method: 'GET',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        dispatch({
          type: types.FETCH_SERVICES_CATEGORIES,
          payload: json // json.user,
        });
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}

export function fetchCurrentServices() {
  return function (dispatch) {
    return fetch(API_URL + '/services/current', {
      headers: { 'content-type': 'application/json' },
      method: 'GET',
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        dispatch({
          type: types.FETCH_CURRENT_SERVICE,
          payload: json // json.user,
        });
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}

export function fetchService(method, serviceId) {
  return function (dispatch) {
    return fetch(API_URL + '/services/service/' + serviceId, {
      headers: { 'content-type': 'application/json' },
      method: method,
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, same-origin, *omit
      withCredentials: true,
      // withAuthentication: true
    }).then((response) => {
      response.json().then(json => {
        dispatch({
          type: types.FETCH_SERVICE,
          payload: json // json.user,
        });
      })
    }).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    }).then(
      response => response,
      error => error
    ).catch(response => dispatch(errorHandler(response.data.error)));
  };
}
