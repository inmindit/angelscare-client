import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, browserHistory } from 'react-router';
import reduxThunk from 'redux-thunk';
import cookie from 'react-cookie';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import { orange, yellow, red, gray } from 'material-ui/colors';
// import ReactGA from 'react-ga';
import routes from './routes';
import reducers from './reducers/index';
import types from './actions/types';
import i18n from './services/i18n';
import { I18nextProvider } from 'react-i18next'; // as we build ourself via webpack

// Import stylesheets
import './public/stylesheets/base.scss';

// Initialize Google Analytics
// ReactGA.initialize('UA-000000-01');

function logPageView() {
  // ReactGA.pageview(window.location.pathname);
}

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const store = createStoreWithMiddleware(reducers);

console.log('i18n: ', i18n);
const user = cookie.load('user');
console.log('User: ', user);

if (user && user.sessionID) {
  // Update application state. User has token and is probably authenticated
  store.dispatch({ type: types.AUTH_USER });
}

const theme = createMuiTheme({
  palette: {
    primary: orange,
    secondary: yellow,
    error: red,
    // Used by `getContrastText()` to maximize the contrast between the background and
    // the text.
    contrastThreshold: 3,
    // Used to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    tonalOffset: 0.2,
  },
});

const element = (
  <MuiThemeProvider theme={theme}>
    <I18nextProvider i18n={i18n}>
      <Provider store={store}>
        <Router history={browserHistory} routes={routes} onUpdate={logPageView} />
      </Provider>
    </I18nextProvider>
  </MuiThemeProvider>
);

ReactDOM.render(element, document.getElementById('root'));
