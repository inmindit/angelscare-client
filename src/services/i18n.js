import i18n from 'i18next';
import XHR from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import { reactI18nextModule } from 'react-i18next';
// import de from '../locales/de/core.json';
// import en from '../locales/en/core.json';

function loadLocales(url, options, callback, data) {
  try {
    const waitForLocale = require('bundle!../locales/' + url + '/core.json');
    waitForLocale((locale) => {
      callback(locale, { status: '200' });
    });
  } catch (error) {
    console.log('Load Locales error: ', error);
    callback(null, { status: '404' });
  }
}

const options = {
  // appendNamespaceToMissingKey: true,
  // load: 'current',
  // default: ['en'],
  // fallbackLng: 'en',
  // fallbackLng: ['en', 'de', 'fr', 'it', 'bg'],
  fallbackLng: ['en', 'de', 'bg'],
  // load: 'all',
  // ns: ['core'],
  // defaultNS: 'core',
  attributes: ['t', 'i18n'],
  // allowMultiLoading: true,
  backend: {
    loadPath: '{{lng}}',
    parse: (data) => data, // comment to have working i18n switch
    ajax: loadLocales // comment to have working i18n switch
  },
  // getAsync: true,
  // // debug: true,
  // react: {
  //   wait: true,
  //   bindI18n: 'languageChanged loaded',
  //   bindStore: 'added removed',
  //   nsMode: 'default'
  // },
  // interpolation: {
  //   escapeValue: false,
  // }
};

i18n.use(XHR).use(LanguageDetector).use(reactI18nextModule).init(options, (err, t) => {
  // console.log('Test i18next: ' + t('core:name')); // -> same as i18next.t
  // console.log('Test i18next: ' + i18n.t('core:name')); // -> same as i18next.t
  // console.log(i18n.t('core:name')); // -> same as i18next.t
  if (err) {
    return console.log('something went wrong loading', err);
  }
});

// catch the event and make changes accordingly
i18n.on('languageChanged', (lng) => {
  // E.g. set the moment locale with the current language
  // console.log('languageChanged: ', lng)
});

export default i18n;
