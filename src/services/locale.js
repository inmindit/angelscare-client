const en = {
  title: "AgeWeb.care"
};
const bg = {
  title: "AgeWeb.care - Б"
};
const de = {
  title: "AgeWeb.care - Buchungsplattform"
};

function getUserLocale() {
  let locale = "de";
  if (locale === "bg"){
    return bg;
  } else if (locale === "de"){
    return de;
  } else {
    return en;
  }
}

export function translate(key) {
  let language = getUserLocale();
  // console.log(language, language[key]);
  return language[key] || `STRING KEY [${key}] NOT FOUND`;
}
