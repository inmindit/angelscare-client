import React from 'react';
import { Route, IndexRoute } from 'react-router';

// Import miscellaneous routes and other requirements
import App from './containers/App';
import NotFoundPage from './containers/NotFoundPage';

// Import static pages
import HomePage from './containers/MainPage';
import ContactPage from './containers/ContactPage';
import PrivacyPolicy from './containers/PrivacyPolicy';
import TermsOfService from './containers/TermsOfService';
import OrderRequestPage from './containers/OrderRequestPage';
import CreateServicePage from './containers/CreateServicePage';
import CreateClubPage from './containers/CreateClubPage';
import CreateNewsPage from './containers/CreateNewsPage';

// Import authentication related pages
import Register from './components/auth/SingUp';
import Login from './components/auth/SignIn';
import Logout from './components/auth/Logout';
import ForgotPassword from './components/auth/ForgotPassword';
import ResetPassword from './components/auth/ResetPassword';

// Import dashboard pages
import Dashboard from './components/dashboard/Dashboard';
import UserProfile from './components/dashboard/profile/UserProfile';
import Inbox from './components/dashboard/messaging/inbox';
import Conversation from './components/dashboard/messaging/conversation';
import ComposeMessage from './components/dashboard/messaging/compose-message';
import BillingSettings from './components/billing/Settings';
import ServicesProviderDashboard from './containers/ServicesProviderDashboard';
import OrdersProviderDashboard from './containers/OrdersProviderDashboard';

// Import billing pages
import InitialCheckout from './components/billing/InitialCheckout';

// Import admin pages
import AdminDashboard from './containers/admin/AdminDashboard';
import ServiceDashboard from './containers/admin/ServiceDashboard';
import RetirementClubsDashboard from './containers/admin/RetirementClubsDashboard';
import NewsDashboard from './containers/admin/NewsDashboard';
import OrdersDashboard from './containers/admin/OrdersDashboard';
// import InviteUserList from './components/admin/Invite/InviteUserList';
import ServicesPage from "./containers/ServicesPage";
import NewsPreviewPage from './containers/NewsPreviewPage';
import EntryPage from './containers/EntryPage';

// Import higher order components
import RequireAuth from './components/auth/RequireAuth';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage} />
    <Route path="contacts" component={ContactPage} />
    <Route path="terms" component={TermsOfService} />
    <Route path="privacy" component={PrivacyPolicy} />

    <Route path="register" component={Register} />
    <Route path="login" component={Login} />
    <Route path="logout" component={Logout} />
    <Route path="forgot-password" component={ForgotPassword} />
    <Route path="reset-password/:resetToken" component={ResetPassword} />

    <Route path="checkout/:plan" component={RequireAuth(InitialCheckout)} />
    <Route path="billing/settings" component={RequireAuth(BillingSettings)} />

    <Route path="profile" component={RequireAuth(UserProfile)} />

    <Route path="services">
      <IndexRoute component={ServicesPage} />
      <Route path="order/:serviceId" component={RequireAuth(OrderRequestPage)} />
    </Route>

    <Route path="news/:newsId" component={NewsPreviewPage} />

    <Route path="entry/:categoryId/:typeId" component={EntryPage} />

    <Route path="admin">
      <IndexRoute component={RequireAuth(AdminDashboard)} />
      <Route path="services" component={RequireAuth(ServiceDashboard)} />
      <Route path="news" component={RequireAuth(NewsDashboard)} />
      <Route path="retirement-clubs" component={RequireAuth(RetirementClubsDashboard)} />
      <Route path="all-orders" component={RequireAuth(OrdersDashboard)} />

      <Route path="service/create" component={RequireAuth(CreateServicePage)} />
      <Route path="news/create" component={RequireAuth(CreateNewsPage)} />
      <Route path="club/create" component={RequireAuth(CreateClubPage)} />
      {/*<Route path="invite/list" component={RequireAuth(InviteUserList)} />*/}
    </Route>
    <Route path="dashboard">
      <IndexRoute component={RequireAuth(Dashboard)} />
      <Route path="inbox" component={RequireAuth(Inbox)} />
      <Route path="services" component={RequireAuth(ServicesProviderDashboard)} />
      <Route path="service/orders" component={RequireAuth(OrdersProviderDashboard)} />
      <Route path="conversation/new" component={RequireAuth(ComposeMessage)} />
      <Route path="conversation/view/:conversationId" component={RequireAuth(Conversation)} />
    </Route>

    <Route path="*" component={NotFoundPage} />
  </Route>
);
