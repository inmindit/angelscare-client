export default {
  dataURL: 'http://localhost:80',
  indexerLimit: 200000,
  maxSearchResult: 100,
  beginTagContainer: '[',
  endTagContainer: ']',
  tagDelimiter: ' ',
  isProdMode: '',
  API_URL: '/api',
  CLIENT_ROOT_URL: '',
}
