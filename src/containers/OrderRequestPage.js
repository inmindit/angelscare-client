import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { withStyles } from 'material-ui/styles';
import { translate } from 'react-i18next';
import Button from 'material-ui/Button';
import Input, { InputLabel, InputAdornment } from 'material-ui/Input';
import { FormControl, FormHelperText } from 'material-ui/Form';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import InfiniteCalendar from 'react-infinite-calendar';
import Grid from 'material-ui/Grid';
import 'react-infinite-calendar/styles.css'; // Make sure to import the default stylesheet
import { actions as ServicesAction, getService } from '../reducers/user';
import { fetchService, orderRequest } from '../actions/services';
import i18n from '../services/i18n';
import PreviewServiceComponent from '../components/PreviewServiceComponent';
import { isAuthenticated } from '../reducers/auth';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginLeft: "3%",
    justify: 'center',
    width: "91%"
  }),
  rootRequest: theme.mixins.gutters({
    marginRight: 25,
    paddingTop: 16,
    paddingBottom: 16,
    marginLeft: "3%",
    justify: 'center',
    width: "91%"
  }),
  mainContent: theme.mixins.gutters({
    marginTop: '3%',
    marginBottom: "5%",
    marginLeft: '1.5%',
    marginRight: '4.5%'
  }),
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  buttonAlign: {
    marginTop: 15,
    width: '100%'
  },
  bookButton: {
    marginTop: 15
  },
  infiniteCalendar: {
    marginTop: 25,
    marginBottom: 25,
    marginLeft: '3%',
  },
  gridContainer: {
    display: 'grid',
    width: '97%',
    gridTemplateColumns: 'repeat(auto-fit,minmax(45%,0fr))',
    gridAutoRows: 'minmax(45%,auto)',
    gridGap: '10px 15px',
  },
});

type Props = {
  isAuthenticated: boolean
};

class ServiceRequestPage extends Component<Props> {
  state = {
    firstName: '',
    lastName: '',
    email: '',
    numberOfAdults: '',
    numberOfChildren: '',
    date: '',
    location: '',
    phoneNumber: '',
    errorEmail: false
  };

  componentDidMount(nextProps, nextState) {
    console.log('Order Page componentDidMount', this.props);
    this.props.fetchService('GET', this.props.params.serviceId);
  }

  handleServiceRequest = () => {
    this.props.orderRequest({
      type: this.state.type,
      image: this.state.image,
      title: this.state.title,
      price: this.state.price,
      description: this.state.description,
      location: this.state.location,
      date: this.state.date
    });
  };

  handleOrderRequest = () => {
    this.props.orderRequest();
  };

  // handleBack = (link) => {
  //   this.props.router.push(link);
  // };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  handleValidation() {
    // const pathRegex = '^((\.\./|[a-zA-Z0-9_/\-\\])*\.[a-zA-Z0-9]+)$';
    // const nameRegex = '^[A-Z][-a-zA-Z]+$';
    if (this.state.email.length > 0) {
      this.setState({ inputError: false, disableConfirmButton: false });
    } else {
      this.setState({ inputError: true, disableConfirmButton: true });
    }
  }

  handleDateChange = (date) => {
    this.setState({ date: Date.parse(date) });
  };

  render() {
    console.log('Order Page: ', this.props);
    console.log('Order Page: ', this.state);

    const { classes, service, isAuthenticated } = this.props;
    const today = new Date();
    // const lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
    const lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1);

    return (
      <div data-tid="serviceRequestPage" className={classes.mainContent}>
        <Grid item xs={12} sm={12}>
          {this.props.service.title && (
            <PreviewServiceComponent
              handleOrderRequest={this.handleOrderRequest}
              isAuthenticated={isAuthenticated}
              orderButton={true}
              service={this.props.service}
            />
          )}
        </Grid>

        {!isAuthenticated && (
          <Grid container spacing={40}>
            <Grid item xs={12} sm={12}>
              <Paper className={classes.rootRequest} elevation={4}>
                <Grid container spacing={40}>
                  <Grid item xs={12} sm={6}>

                    <Typography variant="headline" component="h3">
                      {i18n.t('core:orderRequest')}
                    </Typography>

                    <FormControl fullWidth={true}>
                      <TextField
                        helperText={i18n.t('core:helperTextFirstName')}
                        label={i18n.t('core:firstName')}
                        name="firstName"
                        value={this.state.firstName}
                        onChange={(event) => this.setState({ firstName: event.target.value })}
                        fullWidth
                      />
                    </FormControl>

                    <FormControl fullWidth={true}>
                      <TextField
                        helperText={i18n.t('core:helperTextLastName')}
                        label={i18n.t('core:lastName')}
                        name="lastName"
                        value={this.state.lastName}
                        onChange={this.handleInputChange}
                        fullWidth
                      />
                    </FormControl>

                    <FormControl
                      fullWidth={true}
                      error={this.state.errorEmail}
                    >
                      <TextField
                        helperText={i18n.t('core:helperTextEmail')}
                        label={i18n.t('core:email')}
                        name="email"
                        value={this.state.email}
                        onChange={this.handleInputChange}
                        fullWidth
                      />
                      {this.state.errorEmail && <FormHelperText>{i18n.t('core:InvalidEmailMessage')}</FormHelperText>}
                    </FormControl>

                    <FormControl fullWidth={true}>
                      <TextField
                        id="number"
                        label={i18n.t('core:numberOfAdults')}
                        name="numberOfAdults"
                        value={this.state.numberOfAdults}
                        onChange={this.handleInputChange}
                        type="number"
                        InputLabelProps={{
                          shrink: true,
                        }}
                        margin="normal"
                      />
                    </FormControl>

                    <FormControl fullWidth={true}>
                      <TextField
                        id="number"
                        label={i18n.t('core:numberOfChildren')}
                        name="numberOfChildren"
                        value={this.state.numberOfChildren}
                        onChange={this.handleInputChange}
                        type="number"
                        InputLabelProps={{
                          shrink: true,
                        }}
                        margin="normal"
                      />
                    </FormControl>

                    <FormControl fullWidth={true}>
                      <TextField
                        helperText={i18n.t('core:enterYourLocation')}
                        label={i18n.t('core:location')}
                        name="location"
                        value={this.state.location}
                        onChange={this.handleInputChange}
                        fullWidth
                      />
                    </FormControl>

                    <FormControl fullWidth className={classes.margin}>
                      <InputLabel htmlFor="ServiceRequestPagePhoneNumber">{i18n.t('core:phoneNumber')}</InputLabel>
                      <Input
                        id="serviceRequestPagePhoneNumber"
                        label={i18n.t('core:phoneNumber')}
                        value={this.state.phoneNumber}
                        name="phoneNumber"
                        onChange={this.handleInputChange}
                        startAdornment={<InputAdornment position="start">+</InputAdornment>}
                      />
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} sm={6}>
                    {this.props.service.settings && this.props.service.settings.isDateCalendar && (
                      <FormControl fullWidth={true}>
                        <InfiniteCalendar
                          className={classes.infiniteCalendar}
                          width={400}
                          height={250}
                          selected={this.state.date}
                          disabledDays={[0, 2]}
                          minDate={lastWeek}
                          onSelect={this.handleDateChange}
                        />
                      </FormControl>
                    )}
                  </Grid>
                </Grid>

                <Typography component="p">
                  <Button
                    variant="raised"
                    color="primary"
                    className={classes.bookButton}
                    onClick={() => this.handleServiceRequest()}
                  >
                    {i18n.t('core:book')}
                  </Button>
                  {/*{this.renderAlert()}*/}
                </Typography>
              </Paper>
            </Grid>
          </Grid>
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isAuthenticated: isAuthenticated(state),
    service: getService(state)
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...ServicesAction, fetchService, orderRequest }, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(translate(['core'], { wait: true })(ServiceRequestPage))));
