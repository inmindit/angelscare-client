import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginLeft: "3%",
    justify: 'center',
    width: "91%",
    marginBottom: "5%",
  }),
  mainContent: theme.mixins.gutters({
    marginTop: '2%',
    marginBottom: "2%"
  }),
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  buttonAlign: {
    marginTop: 15,
    width: '100%'
  },
  headTitle: {
    color: '#fc3752',
    paddingTop: 15,
    paddingBottom: 15,
    marginTop: 15,
    marginBottom: 10
  },
  typographyOffset: {
    color: '#444444',
    lineHeight: '1.54',
    paddingBottom: 10,
    marginTop: 10,
    fontSize: 15,
  },
});

class TermsOfService extends Component {
  render() {
    const { classes } = this.props;

    return (
      <div data-tid="serviceRequestPage" className={classes.root}>
        <Grid container spacing={40}>
          <Grid item xs={12} sm={12}>
            {/*<Paper className={classes.mainContent} elevation={4}>*/}

              <Typography variant="headline" component="h3" className={classes.headTitle}>AgeWeb Term of Service</Typography>
              <Typography variant="headline" component="h4" className={classes.headTitle}>Welcome to AgeWeb!</Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                Thanks for using our products and services (“Services”). The Services are provided by AgeWeb (“AgeWeb”), located at Sofia, full address, Bulgaria.
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                By using our Services, you are agreeing to these terms. Please read them carefully.
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                Our Services are very diverse, so sometimes additional terms or product requirements (including age requirements) may apply.
                Additional terms will be available with the relevant Services, and those additional terms become part of your
                agreement with us if you use those Services.
              </Typography>

              <Typography variant="headline" component="h3" className={classes.headTitle}>Using our Services</Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                You must follow any policies made available to you within the Services.
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                Don’t misuse our Services. For example, don’t interfere with our Services or try to access them using a method other than the interface
                and the instructions that we provide. You may use our Services only as permitted by law, including applicable export and re-export control
                laws and regulations. We may suspend or stop providing our Services to you if you do not comply with our terms or
                policies or if we are investigating suspected misconduct.
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                Using our Services does not give you ownership of any intellectual property rights in our Services or the content you access.
                You may not use content from our Services unless you obtain permission from its owner or are otherwise permitted by law.
                These terms do not grant you the right to use any branding or logos used in our Services.
                Don’t remove, obscure, or alter any legal notices displayed in or along with our Services
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                Our Services display some content that is not AgeWeb.
                This content is the sole responsibility of the entity that makes it available.
                We may review content to determine whether it is illegal or violates our policies,
                and we may remove or refuse to display content that we reasonably believe violates our policies or the law.
                But that does not necessarily mean that we review content, so please don’t assume that we do.
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                In connection with your use of the Services, we may send you service announcements,
                administrative messages, and other information. You may opt out of some of those communications.
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                Some of our Services are available on mobile devices.
                Do not use such Services in a way that distracts you and prevents you from obeying traffic or safety laws.
              </Typography>

              <Typography variant="headline" component="h3" className={classes.headTitle}>Privacy and Copyright Protection</Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                AgeWeb's privacy policies explain how we treat your personal data and protect your privacy when you use our Services.
                By using our Services, you agree that AgeWeb can use such data in accordance with our privacy policies.
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                We respond to notices of alleged copyright infringement and
                terminate accounts of repeat infringers according to the process set out in the U.S. Digital Millennium Copyright Act.
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                We provide information to help copyright holders manage their intellectual property online.
                If you think somebody is violating your copyrights and want to notify us,
                you can find information about submitting notices and AgeWeb’s policy about responding to notices in our Help Center.
              </Typography>

              <Typography variant="headline" component="h3" className={classes.headTitle}>Your Content in our Services</Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                Some of our Services allow you to upload, submit, store, send or receive content.
                You retain ownership of any intellectual property rights that you hold in that content. In short, what belongs to you stays yours.
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                You can find more information about how AgeWeb uses and stores content in the privacy policy or additional terms for particular Services.
                If you submit feedback or suggestions about our Services, we may use your feedback or suggestions without obligation to you.
              </Typography>

              <Typography variant="headline" component="h3" className={classes.headTitle}>About Software in our Services</Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                When a Service requires or includes downloadable software, this software may update automatically on your device once a new version or feature is available.
                Some Services may let you adjust your automatic update settings.
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                AgeWeb gives you a personal, worldwide, royalty-free, non-assignable and non-exclusive license to use the software provided to you by AgeWeb as part of the Services.
                This license is for the sole purpose of enabling you to use and enjoy the benefit of the Services as provided by AgeWeb, in the manner permitted by these terms. You may not copy, modify, distribute, sell,
                or lease any part of our Services or included software, nor may you reverse engineer or attempt to extract the source code of that software, unless laws prohibit those restrictions or you have our written permission.
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                Open source software is important to us. Some software used in our Services may be offered under an open source license that we will make available to you.
                There may be provisions in the open source license that expressly override some of these terms.
              </Typography>

              <Typography variant="headline" component="h3" className={classes.headTitle}>Modifying and Terminating our Services</Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                We are constantly changing and improving our Services.
                We may add or remove functionalities or features, and we may suspend or stop a Service altogether.
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                You can stop using our Services at any time, although we’ll be sorry to see you go.
                AgeWeb may also stop providing Services to you, or add or create new limits to our Services at any time.
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                We believe that you own your data and preserving your access to such data is important.
                If we discontinue a Service, where reasonably possible, we will give you reasonable advance notice and a chance to get information out of that Service.
              </Typography>

              <Typography variant="headline" component="h3" className={classes.headTitle}>Business uses of our Services</Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                If you are using our Services on behalf of a business, that business accepts these terms. It will hold harmless and indemnify AgeWeb
                and its affiliates, officers, agents, and employees from any claim, suit or action arising from or related to the use of
                the Services or violation of these terms, including any liability or expense arising from claims, losses, damages, suits, judgments, litigation costs and attorneys’ fees.
              </Typography>

              <Typography variant="headline" component="h3" className={classes.headTitle}>About these Terms</Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                We may modify these terms or any additional terms that apply to a Service to, for example, reflect changes to the law or changes to our Services. You should look at the terms regularly.
                We’ll post notice of modifications to these terms on this page.
                We’ll post notice of modified additional terms in the applicable Service.
                Changes will not apply retroactively and will become effective no sooner than fourteen days after they are posted.
                However, changes addressing new functions for a Service or changes made for legal reasons will be effective immediately.
                If you do not agree to the modified terms for a Service, you should discontinue your use of that Service.
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                If there is a conflict between these terms and the additional terms, the additional terms will control for that conflict.
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                These terms control the relationship between AgeWeb and you. They do not create any third party beneficiary rights.
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                If you do not comply with these terms, and we don’t take action right away, this doesn’t mean that we are giving up any rights that we may have (such as taking action in the future).
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                If it turns out that a particular term is not enforceable, this will not affect any other terms.
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                For information about how to contact AgeWeb, please visit our *contact page.
              </Typography>
            {/*</Paper>*/}
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(TermsOfService);
