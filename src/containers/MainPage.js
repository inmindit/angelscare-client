import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { translate } from 'react-i18next';
import Tabs, { Tab } from 'material-ui/Tabs';
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import SwipeableViews from 'react-swipeable-views';
import { connect } from 'react-redux';
import cookie from 'react-cookie';
import { Carousel } from 'react-responsive-carousel';
import { fetchCurrentServices } from '../actions/services';
import { fetchNews } from '../actions/news';
import { actions as App, getMainSliderData, getMainDataSlide } from '../reducers/app';
import { actions as AppAuthActions, isAuthenticated, getContent } from '../reducers/auth';
import { actions as ServicesAction, getCurrentService } from '../reducers/user';
import { actions as NewsAction, getNews } from '../reducers/news';
import i18n from '../services/i18n';
import AutoRotatingCarouselComponent from '../components/AutoRotatingCarouselComponent';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: '3%',
    marginBottom: "5%",
    // marginLeft: "3%",
    justify: 'center',
    // width: "91%"
  }),
  card: {
    margin: '5px',
    minWidth: 340,
    maxWidth: 340,
  },
  newsCard: {
    margin: '5px',
    minWidth: '100%',
    maxWidth: '100%',
    minHeight: '600px',
    maxHeight: '600px'
  },
  newsMedia: {
    height: 350,
  },
  media: {
    height: 200,
  },
  eventsList: {
    marginLeft: '3%',
    display: 'flex'
  },
  mainPage: {
    marginLeft: '3%',
    marginRight: '3%',
    marginBottom: '5%'
  },
  gridContainer: {
    display: 'grid',
    width: '97%',
    gridTemplateColumns: 'repeat(auto-fit,minmax(340px,0fr))',
    gridAutoRows: 'minmax(250px,auto)',
    gridGap: '10px 15px',
    marginLeft: '2.3%'
  },
  gridNewsContainer: {
    display: 'grid',
    // width: '90%',
    gridTemplateColumns: 'repeat(auto-fit,minmax(550px,0fr))',
    gridAutoRows: 'minmax(550px,auto)',
    gridGap: '10px 15px',
    marginLeft: '10%'
  },
  rowContainer: {
    display: 'grid',
    width: '99%',
    gridTemplateColumns: 'auto',
    gridGap: '1px 1px',
    padding: 1,
    margin: 1,
    marginTop: 62
  },
  gridCell: {
    paddingTop: 3,
    paddingLeft: 3,
    paddingRight: 3
  },
  gridCellThumb: {
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    height: 150
  },
  gridCellTitle: {
    padding: 5
  },
  gridCellDescription: {
    padding: 5,
    height: 50
  },
  price: {
    fontWeight: 500,
    textTransform: 'uppercase',
    textDecoration: 'none',
    justify: 'center',
    marginTop: '-33.7px',
    width: '15%',
    padding: '2%',
    background: '#fc3752',
    color: '#fefefe',
  },
  date: {
    marginLeft: '80%',
    marginTop: '-12%',
    fontWeight: 450,
    textTransform: 'uppercase',
    textDecoration: 'none',
    padding: '1%',
    background: '#fc3752',
    color: '#fefefe',
    width: '4em',
    height: '4em',
    borderRadius: '3em',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    opacity: '0.9',
    margin: '1em',
  },
  slickContent: {
    marginTop: '-15%',
    marginLeft: '20%',
    marginRight: '20%',
    padding: 15,
    background: 'linear-gradient(to right, #e53935, #e35d5b)', // '#ffba4e',
    color: '#fff',
    opacity: '0.9'
  },
  slickContentHeadline: {
    fontSize: '30px',
    color: '#fff',
    lineHeight: '105%'
  },
  mainSubHeaderContainer: {
    marginTop: '2%',
    marginBottom: '1.5%'
  },
  slickAgendaDescription: {
    margin: 5
  },
  contentAbout: {
    marginTop: '5%',
    paddingTop: '5%',
    color: '#fff',
    paddingBottom: '5%',
    background: 'linear-gradient(35deg, #0c3d78 0%, #fc3752 100%)',
    '&::after': {
      background: '#efefef',
      height: '300px',
      left: '-10%',
      right: '-10%',
      transform: 'rotate(-4deg)',
      width: '120%'
    }
  },
  healthcareButton: {
    left: '43%',
    marginTop: '-2%',
    boxShadow: '0 5px 20px rgba(51, 51, 51, 0.35)',
    padding: '10px 40px 10px',
    background: '#fc5068',
    borderColor: '#fc1e3c',
    borderRadius: '20px',
    color: '#fff',
    '&:hover': {
      background: '#fc3752',
      borderColor: '#fc1e3c',
    }
  },
  serviceDescription: {
    height: '200px'
  }
});

function TabContainer(props) {
  const { children, dir } = props;

  return (
    <Typography component="div" dir={dir} style={{ marginLeft: "-2.8%" }}>
      {children}
    </Typography>
  );
}

type Props = {
  isAuthenticated: boolean
};

type State = {
  value: number,
  isLearnMoreDialogOpened: boolean,
  learnMoreAnchorEl: Object,
  selectedEvent: string
};

class MainPage extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.user = cookie.load("user"); //
  }

  state = {
    value: 0,
    isLearnMoreDialogOpened: false,
    learnMoreAnchorEl: null,
    selectedService: '',
    currentServices: '',
    services: [],
    types: [],
    openAutoRotatingCarousel: false
  };

  componentDidMount() {
    this.props.fetchCurrentServices();
    this.props.fetchNews();
  }

  toggleLearnMore  = (event, proposedService) => {
    this.setState((prevState) => ({
      isLearnMoreDialogOpened: !prevState.isLearnMoreDialogOpened,
      learnMoreAnchorEl: event ? event.currentTarget : null,
      selectedService: proposedService || ''
    }));
  };

  handleBooking = (event, service) => {
    if (this.user) {
      this.props.router.push('/services/order/' + service._id);
    } else {
      this.props.router.push('login');
    }
  };

  handleFindOutMore = (event, news) => {
    this.props.router.push('/news/' + news._id);
  };

  handleLearnMore = (event, proposedService) => {
    this.toggleLearnMore(event, proposedService);
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  onChangeSlick = () => {
    // console.log("onChange", arguments);
  };

  onClickSlickItem = () => {
    // console.log("onClickItem", arguments);
  };

  onClickSlickThumb = () => {
    // console.log("onClickThumb", arguments);
  };

  handleHealthCare = () => {
    this.setState({ openAutoRotatingCarousel: true });
  };

  onClose = () => {
    this.setState((prevState) => {
      return {
        openAutoRotatingCarousel: !prevState.openAutoRotatingCarousel
      }
    })
  };

  renderServiceCard = (proposedService) => {
    const { classes } = this.props;
    const date = new Date(proposedService.date).getDate();
    const months = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'August', 'Sept', 'Oct', 'Nov', 'Dec'];
    const month = new Date(proposedService.date).getMonth();

    return (
      <div className={classes.gridCell}>
        <Card className={classes.card} key={proposedService._id}>
          <CardMedia
            className={classes.media}
            image={proposedService.image}
            title={proposedService.title}
          />
          {proposedService.price && (
            <div className={classes.price}>$ {proposedService.price}</div>
          )}
          <div className={classes.date}>{date + " " + months[month]}</div>
          <CardContent>
            <Typography gutterBottom variant="headline" component="h2">
              {proposedService.title}
            </Typography>
            <Typography component="p" className={classes.serviceDescription}>
              {proposedService.description}
            </Typography>
          </CardContent>
          <CardActions>
            <Button
              size="small"
              color="primary"
              onClick={event => this.handleBooking(event, proposedService)}
            >
              {i18n.t('core:book')}
            </Button>
          {/* <Button
              size="small"
              color="primary"
              onClick={event => this.handleLearnMore(event, proposedService)}
            >
              {i18n.t('core:learnMore')}
            </Button>*/}
          </CardActions>
        </Card>
      </div>
    );
  };

  renderCardNews = news => {
    return (
      <div className={this.props.classes.gridCell}>
        <Card className={this.props.classes.newsCard} key={news._id}>
          <CardMedia
            className={this.props.classes.newsMedia}
            component={news.youtube ? 'iframe' : 'img'}
            image={news.youtube ? news.youtube : news.image}
            src={news.youtube ? news.youtube : news.image}
            title={news.title}
          />
          <CardContent>
            <Typography gutterBottom variant="headline" component="h2">
              {news.title}
            </Typography>
            <Typography component="p">
              {news.description}
            </Typography>
          </CardContent>
          <CardActions>
            <Button
              size="small"
              color="primary"
              onClick={event => this.handleFindOutMore(event, news)}
            >
              {i18n.t('core:findOutMore')}
            </Button>
          </CardActions>
        </Card>
      </div>
    );
  };

  renderSlickImages = slick => (
    <div key={slick.uuid}>
      <img src={slick.image} height="100%" />

      {slick.style && (
        <div className={slick.style.content}>

          {slick.style.headline && (
            <h1 className={slick.style.headline}>
              {slick.title}
            </h1>
          )}

          {slick.style.description && (
            <p className={slick.style.description}>
              {slick.description}
            </p>
          )}

          {slick.style.button && (
            <Button
              variant="raised"
              onClick={this.handleServiceRequest}
            >
              {slick.button}
            </Button>
          )}
        </div>
      )}

    </div>
  );

  renderSubHeader = (classes) => (
    <Grid container className={classes.mainSubHeaderContainer}>
      <Grid item xs={12} sm={4}>
        <Typography
          variant="title"
          color="inherit"
          style={{ float: 'left', width: '10%', display: 'inline-flex' }}
          onClick={() => {this.handleClick("/")}}
        >
          <img src="/src/public/assets/img/logo_400.png" height="50" />
        </Typography>
      </Grid>

      <Grid item xs={12} sm={8} className="approach-intro animated fadeInLeft">
        <Button
          onClick={() => this.refs.careGiving.scrollIntoView({ behavior: "smooth" })}
          style={{ float: 'right', color: '#fc3752' }}
        >
          {i18n.t('core:careGiving')}
        </Button>
        <Button
          onClick={() => this.refs.dailyLiving.scrollIntoView({ behavior: "smooth" })}
          style={{ float: 'right', color: '#fc3752' }}
        >
          {i18n.t('core:dailyLiving')}
        </Button>
        <Button
          onClick={() => this.refs.activeAging.scrollIntoView({ behavior: "smooth" })}
          style={{ float: 'right', color: '#fc3752' }}
        >
          {i18n.t('core:activeAging')}
        </Button>
      </Grid>
    </Grid>
  );

  renderActiveAging = (classes) => (
    <div className={classes.contentAbout} ref="activeAging">
      <Typography component="div">
        <h4 className="main-headline-about">
          THOUGHTFUL AND FLEXIBLE DELIVERY OF RCM HEALTHCARE SERVICES
        </h4>
        <div className="main-headline-text-about">
          Developed from extensive practical experience and analysis, our service offers three ways to initiate a high performance,
          end-to-end revenue cycle relationship. We know that successful RCM healthcare services today must be flexible and responsive
          to meet the needs of organizations facing constant updates to payment models and requirements.
          Organizations can choose from modular services that offer a customized approach and target specific challenges or
          two partnership models that let us take on a greater responsibility for the success of your RCM processes.
        </div>
      </Typography>
    </div>
  );

  renderDailyLiving = () => {
    return (
      <div ref="dailyLiving">
        <Grid container className="approach-bs">
          <h1 className="approach-headline">
            A STRONG COMMERCIAL INFRASTRUCTURE TURNS REVENUE CYCLE INTO A COMPETITIVE ADVANTAGE
          </h1>
          <Grid item xs={12} sm={3} className="approach-intro animated fadeInLeft">
            <img src="https://www.visionariodolucro.com.br/wp-content/uploads/2017/11/Elderly-Couple-2-e14683750774331.jpg" height="100%" width="100%" />
          </Grid>
          <Grid item xs={12} sm={3} className="approach-workflow animated fadeInUp">
            <h4>WORKFLOW</h4>
            <span>
              We offer the only fully catalogued, standardized methodology for revenue cycle
              execution from order intake and scheduling to claim reimburesment
            </span>
          </Grid>
          <Grid item xs={12} sm={2} className="approach-analytics animated fadeInUp">
            <h4>ANALYTICS</h4>
            <span>
              Our proprietary performance management system helps enable front-line
              operators to deliver on business outcomes via daily operating measures.
            </span>
          </Grid>
          <Grid item xs={12} sm={2} className="approach-operation animated fadeInDown">
            <h4>OPERATIONS</h4>
            <span>
              Scaled, global delivery including leading human capital, shared services, centralized analytics and monitoring,
              and a dedicated deployment team — delivering on one operating platform.
            </span>
          </Grid>
          <Grid item xs={12} sm={2} className="approach-ns animated fadeInRight">
            <h4>TECHNOLOGY</h4>
            <span>
              We have the only comprehensive RCM workflow that hard-wires standard methods,
              operating metrics, and daily routines into an end-to-end technology platform.
            </span>
          </Grid>
        </Grid>
      </div>
    )
  };

  renderHealthCareService = () => (
    <div ref="healthCareService">
      <Typography component="div">
        <h4 className="main-headline">
          THOUGHTFUL AND FLEXIBLE DELIVERY OF RCM HEALTHCARE SERVICES
        </h4>
        <div className="main-headline-text">
          Developed from extensive practical experience and analysis, our service offers three ways to initiate a high performance,
          end-to-end revenue cycle relationship. We know that successful RCM healthcare services today must be flexible and responsive
          to meet the needs of organizations facing constant updates to payment models and requirements.
          Organizations can choose from modular services that offer a customized approach and target specific challenges or
          two partnership models that let us take on a greater responsibility for the success of your RCM processes.
        </div>
      </Typography>
    </div>
  );

  renderCareGiving = () => (
    <div ref="careGiving">
      <Typography component="div" >
        <h4 className="main-headline">
          THOUGHTFUL AND FLEXIBLE DELIVERY OF RCM HEALTHCARE SERVICES
        </h4>
        <div className="main-headline-text">
          Developed from extensive practical experience and analysis, our service offers three ways to initiate a high performance,
          end-to-end revenue cycle relationship. We know that successful RCM healthcare services today must be flexible and responsive
          to meet the needs of organizations facing constant updates to payment models and requirements.
          Organizations can choose from modular services that offer a customized approach and target specific challenges or
          two partnership models that let us take on a greater responsibility for the success of your RCM processes.
        </div>
      </Typography>
    </div>
  );

  render() {
    console.log("MainPage:", this.props);
    const { classes, theme } = this.props;

    return (
      <div className={classes.mainPage} data-tid="mainPage">
        {/*<Grid container>*/}
          {/*<Grid item xs={12} sm={12}>*/}

          {this.renderSubHeader(classes)}

          <div id="mainPageSliderContent">
            <Carousel
              data-tid="mainPageSlider"
              showArrows={true}
              showStatus={false}
              showThumbs={false}
              infiniteLoop={true}
              dynamicHeight={true}
              centerMode={false}
              autoPlay={true}
              onClickItem={this.onClickSlickItem}
              onChange={this.onChangeSlick}
              onClickThumb={this.onClickSlickThumb}
            >
              {this.props.slickImages.map(this.renderSlickImages)}
            </Carousel>
          </div>

          {this.props.currentServices && (
            <div>
              <Typography component="div">
                <h4 className="main-headline">{i18n.t('core:ourServices')}</h4>
              </Typography>

              <Tabs
                value={this.state.value}
                onChange={this.handleChange}
                indicatorColor="primary"
                textColor="primary"
                fullWidth
              >
                {this.props.currentServices && this.props.currentServices.map((category) => {
                  return (<Tab key={category._id} label={category.name} />)
                })}
              </Tabs>
              <SwipeableViews
                axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                index={this.state.value}
                onChangeIndex={this.handleChangeIndex}
              >
                {
                  this.props.currentServices && this.props.currentServices.map(category => {
                    return (
                      <TabContainer key={category._id} dir={theme.direction}>
                        <div className={classes.gridContainer} data-tid="mainPageCardsEvents">
                          {category.services[0] && category.services.map(service => {
                            return this.renderServiceCard(service);
                          })}
                        </div>
                      </TabContainer>
                    )
                  })
                }
              </SwipeableViews>
            </div>
          )}

          <div>
            {this.renderActiveAging(classes)}

            <Button
              onClick={this.handleHealthCare}
              className={classes.healthcareButton}
            >
              {i18n.t('core:healthCare')}
            </Button>

            {this.props.news && (
              <Typography component="div" ref="latestNews">
                <h4 className="main-headline">{i18n.t('core:latestNews')}</h4>
                <div className={classes.gridNewsContainer} data-tid="mainPageGridNewsContainer">
                  {this.props.news[0] && this.props.news.map((n, index) => {
                    if (index < 4) {
                      return (this.renderCardNews(n));
                    }
                  })}
                </div>
              </Typography>
             )}

            {this.renderDailyLiving(classes)}
            {this.renderHealthCareService(classes)}
            {this.renderCareGiving(classes)}
          </div>

          {this.state.openAutoRotatingCarousel &&  (
            <AutoRotatingCarouselComponent
              open={this.state.openAutoRotatingCarousel}
              onClose={this.onClose}
              dataSlide={this.props.dataSlide}
            />
          )}

        {/* <LearnMoreDialog
            open={this.state.isLearnMoreDialogOpened}
            onClose={this.toggleLearnMore}
            selectedService={this.state.selectedService}
            handleBooking={this.handleBooking}
          />*/}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isAuthenticated: isAuthenticated(state),
    content: getContent(state),
    currentServices: getCurrentService(state),
    news: getNews(state),
    slickImages: getMainSliderData(state),
    dataSlide: getMainDataSlide(state),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    ...App, ...AppAuthActions, ...ServicesAction, ...NewsAction, fetchNews, fetchCurrentServices
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(translate(['core'], { wait: true })(withStyles(styles, { withTheme: true })(MainPage)));

