import React, {Component} from 'react';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid'
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import i18n from 'i18next';
import { translate } from 'react-i18next';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3,
    width: '96.5%'
  }),
  pageNotFound: {
    padding: '10%',
    margin: '10%'
  }
});

class NotFoundPage extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    const {classes} = this.props;
    return (
      <div className={classes.root}>
        <Grid container spacing={24} style={{marginTop: 20}}>
          <Grid item xs={4}>
          </Grid>
          <Grid item xs={4}>
            <Paper className={classes.pageNotFound} elevation={4}>
              <Typography variant="headline" component="h3">
                {i18n.t('core:404Header')}
              </Typography>
              <Typography component="p">
                {i18n.t('core:404Description')}
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={4}>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default translate(['core'], { wait: true })(withStyles(styles)(NotFoundPage));
