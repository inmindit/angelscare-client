import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withStyles } from 'material-ui/styles/index';
import { translate } from 'react-i18next';
import ServiceProviderTable from '../components/admin/ServiceProviderTable';
import ConfirmDialog from '../components/dialogs/ConfirmDialog';
import { fetchAllServices, fetchServicesTypes, fetchServiceCategory, fetchGetServiceType, fetchService } from '../actions/services';
import { actions as ServicesAction, getAdminServices, getServicesTypes, getServicesCategories } from '../reducers/user';

type Props = {};

type State = {
  isDeleteServiceDialogOpened: string,
  isAddServiceDialogOpened: string,
  isEditServiceDialogOpened: string
};

class ServiceProviderDashboard extends Component<Props, State> {
  constructor(props) {
    super(props);
  }

  state = {
    isDeleteServiceDialogOpened: false,
    isAddServiceDialogOpened: false,
    isEditServiceDialogOpened: false,
    selectedService: {}
  };

  componentDidMount () {
    // Fetch all services
    this.props.fetchAllServices();
  }

  showDeleteServiceDialog = (service) => {
    this.setState({
      isDeleteServiceDialogOpened: true,
      selectedService: service
    });
  };

  showAddServiceDialog = () => {
    this.setState({
      isAddServiceDialogOpened: true
    });
  };

  showEditServiceDialog = (service) => {
    this.setState({
      isEditServiceDialogOpened: true,
      selectedService: service
    });
  };

  handleCloseDialogs = () => {
    this.setState({
      isDeleteServiceDialogOpened: false,
      isAddServiceDialogOpened: false,
      isEditServiceDialogOpened: false,
    });
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  render() {
    console.log('Provider Services Dashboard(State): ', this.state);
    console.log('Provider Services Dashboard(Props): ', this.props);

    return (
      <div>
        <ServiceProviderTable
          services={this.props.services}
          showDeleteServiceDialog={this.showDeleteServiceDialog}
          showAddServiceDialog={this.showAddServiceDialog}
          showEditServiceDialog={this.showEditServiceDialog}
        />

        <ConfirmDialog
          open={this.state.isDeleteServiceDialogOpened}
          onClose={this.handleCloseDialogs}
          title={'Delete Service'}
          content={'Do you want to delete this service: {{service}} ?'}
          confirmCallback={event => {
            if (event) {
              this.props.fetchService('DELETE', this.state.selectedService._id);
            }
          }}
          confirmDialogContent={'confirmDialogContent'}
          cancelDialogActionId={'cancelDeleteServiceDialog'}
          confirmDialogActionId={'confirmDeleteServiceDialog'}
        />
      </div>
    );
  }
}

{/*<AddEventDialog
  open={this.state.isAddServiceProviderDialogOpened}
  onClose={this.handleCloseDialogs}
/>
<EditEventDialog
open={this.state.isEditServiceProviderDialogOpened}
onClose={this.handleCloseDialogs}
/>*/}

function mapStateToProps(state) {
  return {
    content: state.auth.content,
    services: getAdminServices(state),
    serviceTypes: getServicesTypes(state),
    serviceCategories: getServicesCategories(state),
    types: fetchGetServiceType(state)
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...ServicesAction, fetchAllServices, fetchService, fetchServiceCategory, fetchServicesTypes, fetchGetServiceType }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles({ withTheme: true })(translate(['core'], { wait: true })(ServiceProviderDashboard)));
