import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { translate } from 'react-i18next';
import Tabs, { Tab } from 'material-ui/Tabs';
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';
import SwipeableViews from 'react-swipeable-views';
import { connect } from 'react-redux';
import cookie from 'react-cookie';
import { fetchCurrentServices } from '../actions/services';
import { fetchNews } from '../actions/news';
import { actions as AppAuthActions, isAuthenticated } from '../reducers/auth';
import { actions as ServicesAction, getCurrentService } from '../reducers/user';
import { actions as NewsAction, getNews } from '../reducers/news';
import i18n from '../services/i18n';

function TabContainer(props) {
  const { children, dir } = props;

  return (
    <Typography component="div" dir={dir} style={{ marginLeft: "-2.8%" }}>
      {children}
    </Typography>
  );
}

type Props = {
  isAuthenticated: boolean
};

type State = {
  value: number,
  isLearnMoreDialogOpened: boolean,
  learnMoreAnchorEl: Object,
  selectedEvent: string
};

class MainPage extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.user = cookie.load("user"); //
  }

  state = {
    value: 0,
    isLearnMoreDialogOpened: false,
    learnMoreAnchorEl: null,
    selectedService: '',
    currentServices: '',
    services: [],
    types: [],
    openAutoRotatingCarousel: false
  };

  componentDidMount() {
    this.props.fetchCurrentServices();
    this.props.fetchNews();
  }

  handleBooking = (event, service) => {
    if (this.user) {
      this.props.router.push('/services/order/' + service._id);
    } else {
      this.props.router.push('login');
    }
  };

  handleFindOutMore = (event, news) => {
    this.props.router.push('/news/' + news._id);
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  renderServiceCard = (proposedService) => {
    const { classes } = this.props;
    const date = new Date(proposedService.date).getDate();
    const months = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'August', 'Sept', 'Oct', 'Nov', 'Dec'];
    const month = new Date(proposedService.date).getMonth();

    return (
      <div className={classes.gridCell}>
        <Card className={classes.card} key={proposedService._id}>
          <CardMedia
            className={classes.media}
            image={proposedService.image}
            title={proposedService.title}
          />
          {proposedService.price && (
            <div className={classes.price}>$ {proposedService.price}</div>
          )}
          <div className={classes.date}>{date + " " + months[month]}</div>
          <CardContent>
            <Typography gutterBottom variant="headline" component="h2">
              {proposedService.title}
            </Typography>
            <Typography component="p" className={classes.serviceDescription}>
              {proposedService.description}
            </Typography>
          </CardContent>
          <CardActions>
            <Button
              size="small"
              color="primary"
              onClick={event => this.handleBooking(event, proposedService)}
            >
              {i18n.t('core:book')}
            </Button>
          </CardActions>
        </Card>
      </div>
    );
  };

  renderCardNews = news => {
    return (
      <div className={this.props.classes.gridCell}>
        <Card className={this.props.classes.newsCard} key={news._id}>
          <CardMedia
            className={this.props.classes.newsMedia}
            component={news.youtube ? 'iframe' : 'img'}
            image={news.youtube ? news.youtube : news.image}
            src={news.youtube ? news.youtube : news.image}
            title={news.title}
          />
          <CardContent>
            <Typography gutterBottom variant="headline" component="h2">
              {news.title}
            </Typography>
            <Typography component="p">
              {news.description}
            </Typography>
          </CardContent>
          <CardActions>
            <Button
              size="small"
              color="primary"
              onClick={event => this.handleFindOutMore(event, news)}
              // onClick={event => this.handleLearnMore(event, news)}
            >
              {i18n.t('core:findOutMore')}
            </Button>
          </CardActions>
        </Card>
      </div>
    );
  };

  render() {
    console.log("MainPage:", this.props);
    const { classes, theme } = this.props;

    return (
      <div className={classes.mainPage} data-tid="mainPage">
        {this.props.currentServices && (
          <div>
            <Typography component="div">
              <h4 className="main-headline">{i18n.t('core:ourServices')}</h4>
            </Typography>

            <Tabs
              value={this.state.value}
              onChange={this.handleChange}
              indicatorColor="primary"
              textColor="primary"
              fullWidth
            >
              {this.props.currentServices && this.props.currentServices.map((category) => {
                return (<Tab key={category._id} label={category.name} />)
              })}
            </Tabs>
            <SwipeableViews
              axis={theme.direction === "rtl" ? "x-reverse" : "x"}
              index={this.state.value}
              onChangeIndex={this.handleChangeIndex}
            >
              {
                this.props.currentServices && this.props.currentServices.map(category => {
                  return (
                    <TabContainer key={category._id} dir={theme.direction}>
                      <div className={classes.gridContainer} data-tid="mainPageCardsEvents">
                        {category.services[0] && category.services.map(service => {
                          return this.renderServiceCard(service);
                        })}
                      </div>
                    </TabContainer>
                  )
                })
              }
            </SwipeableViews>
          </div>
        )}

        <div>
          {this.props.news && (
            <Typography component="div" ref="latestNews">
              <h4 className="main-headline">{i18n.t('core:latestNews')}</h4>
              <div className={classes.gridNewsContainer} data-tid="mainPageGridNewsContainer">
                {this.props.news[0] && this.props.news.map((n, index) => {
                  if (index < 4) {
                    return (this.renderCardNews(n));
                  }
                })}
              </div>
            </Typography>
          )}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isAuthenticated: isAuthenticated(state),
    currentServices: getCurrentService(state),
    news: getNews(state),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    ...App,
    ...AppAuthActions,
    ...ServicesAction,
    ...NewsAction,
    fetchNews,
    fetchCurrentServices
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(translate(['core'], { wait: true })(withStyles({ withTheme: true })(MainPage)));

