import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import FolderIcon from '@material-ui/icons/Folder';
import Switch from 'material-ui/Switch';
import { FormControl, FormHelperText, FormControlLabel } from 'material-ui/Form';
import Input, { InputLabel, InputAdornment } from 'material-ui/Input';
import Chip from 'material-ui/Chip';
import AddIcon from '@material-ui/icons/Add';
import MenuItem from 'material-ui/Menu/MenuItem';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { translate } from 'react-i18next';
import { fetchCreateNews } from '../actions/news';
import { fetchServiceCategory } from '../actions/services';
import { getNews } from '../reducers/news';
import { getServicesCategories } from '../reducers/user';
import { getSupportedCountries } from '../reducers/app';
import RichTextEditorComponent from '../components/RichTextEditorComponent';
import i18n from '../services/i18n';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: '3%',
    marginBottom: "5%",
    marginLeft: "3%",
    justify: 'center',
    width: "91%"
  }),
  textField: {
    marginBottom: 15,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '80%'
  },
  input: {
    display: 'none',
  },
  contentHeader: {
    margin: '-40px 15px 30px',
    padding: '15px',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    fontWeight: 300,
    lineHeight: '1.5em',
    borderRadius: '3px',
    background: 'linear-gradient(60deg, #ff9800, #8e24aa)',
    boxShadow: '0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2)',
    color: '#fff'
  },
  typographyTitle: {
    color: '#fff'
  },
  typographyOffset: {
    marginTop: 5,
    fontSize: 15,
    color: '#fff'
  },
  imgPreviewCS: {
    boxShadow: '0 10px 25px 0 rgba(0, 0, 0, 0.3)'
  },
  topOffset: {
    marginTop: '20%'
  },
  tag: {
    margin: theme.spacing.unit / 2,
    backgroundColor: '#f57c00'
  },
  paper: {
    width: '75%',
    margin: '10px',
    padding: theme.spacing.unit * 2,
    color: theme.palette.text.secondary,
  },
  inputLabel: {
    paddingLeft: 10
  }
});

type Props = {
  fetchServiceCategory: () => void
};

type State = {
  title: string,
  description: string,
};

class CreateNewsPage extends Component<Props, State> {
  state = {
    title: '',
    youtube: '',
    description: '',
    image: {},
    imagePreviewURL: '',
    content: '',
    newsCategory: '',
    newsType: '',
    serviceTypes: '',
    selectedNewsCategory: '',
    selectedNewsType: '',
    supportedCountries: [],
    tag: '',
    tags: [],
    country: '',
    countries: [],
    errorText: false,
    isVideo: true,
  };

  componentDidMount() {
    this.props.fetchServiceCategory('GET');
    this.setState({
      supportedCountries: this.props.supportedCountries
    });
  }

  handleCreateNews = () => {
    this.props.createNews({
      category: this.state.selectedNewsCategory._id,
      type: this.state.selectedNewsType._id || '',
      image: this.state.image,
      youtube: this.state.youtube,
      title: this.state.title,
      content: this.state.content,
      description: this.state.description,
      tags: this.state.tags,
      location: this.state.location,
      countries: this.state.countries,
    });
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    }, this.handleValidation());
  };

  handleInputImageChange = (event) => {
    event.preventDefault();
    let reader = new FileReader();
    let file = event.target.files[0];

    reader.onloadend = () => {
      this.setState({
        image: file,
        imagePreviewURL: reader.result
      });
    };

    reader.readAsDataURL(file);
  };

  handleServiceCategoryChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.props.servicesCategories.map(category => {
      if (category.slug === value) {
        this.setState({
          [name]: value,
          selectedNewsCategory: category,
          serviceTypes: category.types
        });
      }
    });
  };

  handleServiceTypeChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.state.serviceTypes.map(type => {
      if (type.slug === value) {
        this.setState({
          [name]: value,
          selectedNewsType: type
        });
      }
    });
  };

  handleValidation() {
    // const pathRegex = '^((\.\./|[a-zA-Z0-9_/\-\\])*\.[a-zA-Z0-9]+)$';
    // const nameRegex = '^[A-Z][-a-zA-Z]+$';
    if (this.state.title.length > 0) {
      this.setState({ inputError: false, disableConfirmButton: false });
    } else {
      this.setState({ inputError: true, disableConfirmButton: true });
    }
  }

  setContent = (content) => {
    this.setState({ content });
  };

  handleAddTag = () => {
    if (this.state.tag.length > 0) {
      let { tags, tag } = this.state;

      tags.push({
        name: tag,
        slug: tag.toLowerCase()
      });
      this.setState({ tag: '', tags });
    }
  };

  handleDeleteTag = data => {
    const tags = [...this.state.tags];
    const deleteServiceTag = tags.indexOf(data);
    tags.splice(deleteServiceTag, 1);
    this.setState({ tags });
  };

  handleAddCountryTag = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    let { countries, } = this.state;

    countries.push(value);

    this.setState({
      [name]: '',
      countries,
    });
  };

  handleDeleteCountryTag = data => {
    const countries = [...this.state.countries];
    const deleteServiceTag = countries.indexOf(data);
    countries.splice(deleteServiceTag, 1);
    this.setState({ countries });
  };

  handleMouseDown = event => {
    event.preventDefault();
  };

  renderTags = (classes) => {
    const tags = this.state.tags;
    return (
      tags.map(data => (
          <Chip
            key={data.name}
            avatar={null}
            label={data.name}
            onDelete={() => this.handleDeleteTag(data)}
            className={classes.tag}
          />
        )
      )
    )
  };

  renderCountryTags = (classes) => {
    const countries = this.state.countries;
    return (
      countries.map(country => (
          <Chip
            key={country}
            avatar={null}
            label={country}
            onDelete={() => this.handleDeleteCountryTag(country)}
            className={classes.tag}
          />
        )
      )
    )
  };

  render() {
    const { classes } = this.props;
    console.log('Create News Page - Props:', this.props);
    console.log('Create News Page - State:', this.state);

    return (
      <div data-tid="createNewsPage">
        <Paper className={classes.root} elevation={4}>
          <div className={classes.contentHeader}>
            <Typography variant="headline" component="h3" className={classes.typographyTitle}>
              {i18n.t('core:createNews')}
            </Typography>
            <Typography variant="headline" component="p" className={classes.typographyOffset}>
              {i18n.t('core:createNewsDescription')}
            </Typography>
          </div>

          <div>
            <Grid container spacing={40} className={classes.mainSubHeaderContainer}>
              <Grid item xs={12} sm={6}>

                {this.state.isVideo && (
                  <div className={classes.textField}>
                    <img
                      className={classes.imgPreviewCS}
                      src={this.state.imagePreviewURL ? this.state.imagePreviewURL : '/src/public/assets/img/image_placeholder.jpg'} height="150"
                    />
                  </div>
                )}

                {this.state.isVideo ? (
                  <FormControl
                    encType="multipart/form-data"
                    fullWidth={true}
                    error={this.state.errorText}
                  >
                    <Input
                      required
                      margin="dense"
                      disabled={true}
                      placeholder={i18n.t('core:choosePhoto')}
                      fullWidth={true}
                      data-tid="createNewsUploadImage"
                      value={this.state.image.name}
                      className={classes.textField}
                      endAdornment={
                        <InputAdornment position="end" style={{ height: 32 }}>
                          <IconButton>
                            <label htmlFor="createNewsUploadImage">
                              <FolderIcon/>
                            </label>
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                    {this.state.errorText && <FormHelperText>{i18n.t('core:InvalidUrlAddressMessage')}</FormHelperText>}
                  </FormControl>
                ) : (
                  <FormControl fullWidth={true}>
                    <TextField
                      className={classes.textField}
                      helperText={i18n.t('core:helperTextYoutubeEmbedded')}
                      label={i18n.t('core:youtubeEmbedded')}
                      name="youtube"
                      onChange={this.handleInputChange}
                      fullWidth
                    />
                  </FormControl>
                )
                }

                <FormControl fullWidth={true}>
                  <RichTextEditorComponent setContent={this.setContent}/>
                </FormControl>
              </Grid>

              <Grid item xs={12} sm={6}>

                <FormControl fullWidth={true} className={classes.topOffset}>
                  <FormControlLabel
                    control={
                      <Switch
                        data-tid="isVideo"
                        name="isVideo"
                        checked={this.state.isVideo}
                        onChange={this.handleInputChange}
                      />
                    }
                    label={i18n.t('core:enableVideoOrPhotoChooser')}
                  />
                </FormControl>

                <FormControl fullWidth={true}>
                  <TextField
                    className={classes.textField}
                    helperText={i18n.t('core:newsTitle')}
                    label={i18n.t('core:newsTitle')}
                    name="title"
                    onChange={this.handleInputChange}
                    fullWidth
                  />
                </FormControl>

                <FormControl fullWidth={true}>
                  <TextField
                    id="newsDescription"
                    placeholder={i18n.t('core:newsDescription')}
                    rows="4"
                    rowsMax="6"
                    multiline
                    name="description"
                    className={classes.textField}
                    value={this.state.description}
                    onChange={this.handleInputChange}
                    margin="normal"
                    fullWidth={true}
                  />
                </FormControl>

                <FormControl fullWidth={true}>
                  <TextField
                    id="createNewsCategory"
                    select
                    label={i18n.t('core:selectNewsCategory')}
                    className={classes.textField}
                    value={this.state.newsCategory}
                    name="newsCategory"
                    data-tid="createNewsCategory"
                    onChange={this.handleServiceCategoryChange}
                    SelectProps={{
                      MenuProps: {
                        className: classes.menu,
                      },
                    }}
                    helperText={i18n.t('core:helperTextSelectServiceCategory')}
                    margin="normal"
                  >
                    {this.props.servicesCategories[0] && this.props.servicesCategories.map(service => (
                      <MenuItem key={service.slug} value={service.slug}>
                        {service.name}
                      </MenuItem>
                    ))}
                  </TextField>
                </FormControl>

                {this.state.serviceTypes && (
                  <FormControl fullWidth={true}>
                    <TextField
                      id="createNewsType"
                      select
                      label={i18n.t('core:selectNewsType')}
                      className={classes.textField}
                      value={this.state.newsType}
                      name="newsType"
                      data-tid="createNewsType"
                      onChange={this.handleServiceTypeChange}
                      SelectProps={{
                        MenuProps: {
                          className: classes.menu,
                        },
                      }}
                      helperText={i18n.t('core:helperTextSelectType')}
                      margin="normal"
                    >
                      {this.state.serviceTypes && this.state.serviceTypes.map(type =>
                        <MenuItem key={type.slug} value={type.slug}>
                          {type.name}
                        </MenuItem>
                      )}
                    </TextField>
                  </FormControl>
                )}

                <FormControl className={classes.margin} fullWidth={true}>
                  <InputLabel className={classes.inputLabel} htmlFor="news-tags">{i18n.t('core:addTagToNews')}</InputLabel>
                  <Input
                    id="news-tags"
                    type="text"
                    className={classes.textField}
                    value={this.state.tag}
                    name="tag"
                    fullWidth={true}
                    onChange={this.handleInputChange}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label={i18n.t('core:addTagToNews')}
                          onClick={this.handleAddTag}
                          onMouseDown={this.handleMouseDown}
                        >
                          <AddIcon/>
                        </IconButton>
                      </InputAdornment>
                    }
                  />

                  {this.state.tags.length > 0 && (
                    <Paper className={classes.paper}>
                      {this.renderTags(classes)}
                    </Paper>
                  )}
                </FormControl>

                <FormControl className={classes.margin} fullWidth={true}>
                  <TextField
                    id="news-country-tags"
                    select
                    label={i18n.t('core:addTargetCountries')}
                    className={classes.textField}
                    value={this.state.country}
                    name="country"
                    data-tid="news-country-tags"
                    onChange={this.handleAddCountryTag}
                    SelectProps={{
                      MenuProps: {
                        className: classes.menu,
                      },
                    }}
                    helperText={i18n.t('core:helperTextTargetCountries')}
                    margin="normal"
                  >
                    {this.state.supportedCountries && this.state.supportedCountries.map(country => {
                        if (this.state.countries.indexOf(country.value) < 0) {
                          return (
                            <MenuItem key={country.label} value={country.value}>
                              {country.value}
                            </MenuItem>
                          );
                        }
                      }
                    )}
                  </TextField>
                  {this.state.countries.length > 0 && (
                    <Paper className={classes.paper}>
                      {this.renderCountryTags(classes)}
                    </Paper>
                  )}
                </FormControl>
              </Grid>
            </Grid>

            <Button
              variant="raised"
              style={{ marginTop: '15px' }}
              color="primary"
              onClick={(event) => this.handleCreateNews(event)}
            >
              {i18n.t('core:create')}
            </Button>
          </div>

          <form encType="multipart/form-data" className={classes.input}>
            <input
              accept="image/*"
              onChange={this.handleInputImageChange}
              id="createNewsUploadImage"
              name="news"
              type="file"
            />
          </form>
          {/*{this.renderAlert()}*/}
        </Paper>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    news: getNews(state),
    servicesCategories: getServicesCategories(state),
    supportedCountries: getSupportedCountries(state),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ createNews: fetchCreateNews, fetchServiceCategory }, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(translate(['core'], { wait: true })(CreateNewsPage))));
