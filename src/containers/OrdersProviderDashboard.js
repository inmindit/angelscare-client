import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withStyles } from 'material-ui/styles/index';
import { translate } from 'react-i18next';
import OrderProviderTable from '../components/admin/OrderProviderTable';
import ConfirmDialog from '../components/dialogs/ConfirmDialog';
import { fetchOrders, fetchOrder } from '../actions/orders';
import { actions as OrderAction, getOrders } from '../reducers/user';

type Props = {};

type State = {
  isDeleteOrderDialogOpened: string,
  isEditOrderDialogOpened: string
};

class OrdersProviderDashboard extends Component<Props, State> {
  constructor(props) {
    super(props);
  }

  state = {
    isDeleteEventDialogOpened: false,
    isEditEventDialogOpened: false,
    selectedOrder: {}
  };

  componentDidMount () {
    // Fetch all events
    this.props.fetchOrders();
  }

  showDeleteOrderDialog = (order) => {
    this.setState({
      isDeleteServiceDialogOpened: true,
      selectedOrder: order
    });
  };

  showEditOrderDialog = (order) => {
    this.setState({
      isEditServiceDialogOpened: true,
      selectedOrder: order
    });
  };

  handleCloseDialogs = () => {
    this.setState({
      isDeleteServiceDialogOpened: false,
      isAddServiceDialogOpened: false,
      isEditServiceDialogOpened: false,
    });
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  render() {
    console.log('Provider Orders Dashboard(State): ', this.state);
    console.log('Provider Orders Dashboard(Props): ', this.props);

    return (
      <div>
        <OrderProviderTable
          orders={this.props.orders}
          showDeleteOrderDialog={this.showDeleteOrderDialog}
          showEditOrderDialog={this.showEditOrderDialog}
        />

        <ConfirmDialog
          open={this.state.isDeleteOrderDialogOpened}
          onClose={this.handleCloseDialogs}
          title={'Delete Service'}
          content={'Do you want to delete this service: {{service}} ?'}
          confirmCallback={event => {
            if (event) {
              this.props.fetchService('DELETE', this.state.selectedOrder._id);
            }
          }}
          confirmDialogContent={'confirmDialogContent'}
          cancelDialogActionId={'cancelDeleteOrderDialog'}
          confirmDialogActionId={'confirmDeleteOrderDialog'}
        />
      </div>
    );
  }
}

{/*
  <EditEventDialog
    open={this.state.isEditEventDialogOpened}
    onClose={this.handleCloseDialogs}
  />
*/}

function mapStateToProps(state) {
  return {
    content: state.auth.content,
    orders: getOrders(state)
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...OrderAction, fetchOrders, fetchOrder }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles({ withTheme: true })(translate(['core'], { wait: true })(OrdersProviderDashboard)));
