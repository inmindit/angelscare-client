import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles/index';
import { connect } from 'react-redux';
import Button from 'material-ui/Button';
import { FormControl } from 'material-ui/Form';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import { bindActionCreators } from 'redux';
import { translate } from 'react-i18next';
import { fetchSendContact } from '../actions/index';
import i18n from '../services/i18n';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3,
    // marginBottom: theme.spacing.unit * 3,
    marginLeft: "23%",
    width: "50%"
  }),
  buttonAlign: {
    marginTop: 15,
    width: '100%'
  }
});

class ContactPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      emailAddress: '',
      subject: '',
      message: '',
    }
  }

  handleFormSubmit({ firstName, lastName, emailAddress, subject, message }) {
    this.props.sendContact({ firstName, lastName, emailAddress, subject, message });
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.errorMessage}
        </div>
      );
    }
  }

  renderMessage() {
    if (this.props.message) {
      return (
        <div className="alert alert-success">
          <strong>{i18n.t('core:success')}!</strong> {this.props.message}
        </div>
      );
    }
  }

  render() {
    // const { handleSubmit, fields: { firstName, lastName, emailAddress, subject, message } } = this.props;
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Paper className={classes.root} elevation={4}>
          <Typography variant="headline" component="h3">
            {i18n.t('core:contactUsHeader')}
          </Typography>

          <FormControl fullWidth={true}>
            <TextField
              id="contactPageFirstName"
              helperText={i18n.t('core:helperTextFirstName')}
              label={i18n.t('core:firstName')}
              name="firstName"
              onChange = {this.handleInputChange}
              fullWidth
            />
            {/*{this.state.errorUsername && <FormHelperText>Invalid username</FormHelperText>}*/}
          </FormControl>

          <FormControl fullWidth={true}>
            <TextField
              id="contactPageLastName"
              helperText={i18n.t('core:helperTextLastName')}
              label={i18n.t('core:lastName')}
              name="lastName"
              onChange = {this.handleInputChange}
              fullWidth
            />
            {/*{this.state.errorUsername && <FormHelperText>{i18n.t('core:InvalidUsernameMessage')}</FormHelperText>}*/}
          </FormControl>

          <FormControl fullWidth={true}>
            <TextField
              id="contactPageEmailAddress"
              type="email"
              helperText={i18n.t('core:helperTextEmail')}
              label={i18n.t('core:email')}
              name="emailAddress"
              onChange = {this.handleInputChange}
              fullWidth
            />
            {/*{this.state.errorUsername && <FormHelperText>{i18n.t('core:InvalidEmailMessage')}</FormHelperText>}*/}
          </FormControl>

          <FormControl fullWidth={true}>
            <TextField
              id="contactPageSubject"
              name="subject"
              label="Subject"
              onChange = {this.handleInputChange}
              fullWidth
            />
            {/*{this.state.errorUsername && <FormHelperText>{i18n.t('core:InvalidUsernameMessage')}</FormHelperText>}*/}
          </FormControl>

          <FormControl fullWidth={true}>
            <TextField
              id="contactPageDescription"
              placeholder={i18n.t('core:description')}
              rowsMax="4"
              multiline
              name="description"
              className={classes.textField}
              value={this.state.description}
              onChange={e => this.handleInputChange(e)}
              margin="normal"
              fullWidth={true}
            />
          </FormControl>

          <Typography component="p">
            <Button
              id="loginSubmit"
              variant="raised"
              color="primary"
              style={{marginTop: 15, width: "100%"}}
              onClick={this.handleFormSubmit.bind(this)}
            >
              {i18n.t('core:send')}
            </Button>
            {this.renderAlert()}
          </Typography>
        </Paper>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.communication.error,
    message: state.communication.message,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ sendContact: fetchSendContact }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(translate(['core'], { wait: true })(withStyles(styles)(ContactPage)));
