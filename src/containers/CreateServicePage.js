import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Switch from 'material-ui/Switch';
import { FormControl, FormControlLabel } from 'material-ui/Form';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import MenuItem from 'material-ui/Menu/MenuItem';
import Stepper, { Step, StepLabel } from 'material-ui/Stepper';
import { connect } from 'react-redux';
import 'react-infinite-calendar/styles.css'; // Make sure to import the default stylesheet
import { bindActionCreators } from 'redux';
import { translate } from 'react-i18next';
import { fetchCreateService, fetchServiceCategory, fetchServiceLanguage } from '../actions/services';
import { getServicesCategories } from '../reducers/user';
import { getServicesLanguages } from '../reducers/app';
import PreviewServiceComponent from '../components/PreviewServiceComponent';
import CreateServiceComponent from '../components/CreateServiceComponent';
import i18n from '../services/i18n';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: '3%',
    marginBottom: "5%",
    marginLeft: "3%",
    justify: 'center',
    width: "91%"
  }),
  buttonAlign: {
    marginTop: '15px'
  },
  infiniteCalendar: {
    marginTop: 25,
    marginBottom: 25
  },
  input: {
    display: 'none',
  },
  contentHeader: {
    margin: '-40px 15px 30px',
    padding: '15px',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    fontWeight: 300,
    lineHeight: '1.5em',
    borderRadius: '3px',
    background: 'linear-gradient(60deg, #ff9800, #8e24aa)',
    boxShadow: '0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2)',
    color: '#fff'
  },
  typographyTitle: {
    color: '#fff'
  },
  typographyOffset: {
    marginTop: 5,
    fontSize: 15,
    color: '#fff'
  },
  backButton: {
    marginTop: '15px',
    marginRight: theme.spacing.unit,
  },
  instructions: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },
  imgPreviewCS: {
    boxShadow: '0 10px 25px 0 rgba(0, 0, 0, 0.3)'
  },
  textField: {
    marginBottom: 15,
    width: '80%'
  },
  tag: {
    margin: theme.spacing.unit / 2,
    backgroundColor: '#f57c00'
  },
  paper: {
    width: '75%',
    margin: '10px',
    padding: theme.spacing.unit * 2,
    color: theme.palette.text.secondary,
  },
  inputLabel: {
    paddingLeft: 10
  }
});

type Props = {};

type State = {};

const supportedCurrencies = [
  { value: 'USD', label: '$', },
  { value: 'EUR', label: '€', },
  { value: 'BG', label: 'BG', }
];

class CreateServicePage extends Component<Props, State> {
  constructor(props) {
    super(props);

    this.renderCreateService = this.renderCreateService.bind(this);
    this.renderCreateServiceSettings = this.renderCreateServiceSettings.bind(this);
    this.renderPreviewCreatedService = this.renderPreviewCreatedService.bind(this);
  }

  state = {
    service: {},
    serviceType: '',
    serviceTypes: '',
    serviceCategory: '',
    selectedServiceCategory: '',
    selectedServiceType: '',
    activeStep: 0,
    currency: 'EUR',
    language: {},
    settings: {
      isStartDate: true,
      isEndDate: true,
      isStartHour: false,
      isEndHour: false,
      isDateCalendar: false,
      isRecurring: false,
      isPayment: false,
      isLimitOrderQuantity: false,
      isAdultService: false,
      isChildrenService: false,
    },
    errorText: false,
    serviceSupportedLanguages: [],
    steps: [i18n.t('core:createServiceStep1'), i18n.t('core:createServiceStep2'), i18n.t('core:createServiceStep3')]
  };

  componentDidMount() {
    // Fetch all services
    this.props.fetchServiceCategory('GET');
    this.props.fetchServiceLanguage('GET');
    // IF ROUTE PARAM : ServiceID then fetchService
    //
  }

  handleUpdateService = (service) => {
    this.setState({
      service
    });
  };

  handleCreateService = () => {
    this.props.createService({
      category: this.state.selectedServiceCategory._id,
      type: this.state.selectedServiceType._id || '',
      title: this.state.service.title,
      price: this.state.service.price,
      content: this.state.service.content,
      description: this.state.service.description,
      language: this.state.language,
      location: this.state.service.location,
      currency: this.state.currency,
      place: this.state.service.place,
      date: this.state.service.endDate,
      startDate: this.state.service.startDate,
      endDate: this.state.service.endDate,
      startHours: this.state.service.startHours,
      endHours: this.state.service.endHours,
      orderQuantity: this.state.service.orderQuantity,
      image: this.state.service.image,
      settings: this.state.settings,
    });
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    const isSettings = Object.keys(this.state.settings);

    if (isSettings.indexOf(name) > -1) {
      let settings = this.state.settings;
      settings[name] = value;

      if (name === 'isRecurring' && value === true){
        settings.isDateCalendar = true;
      }
      if (name === 'isStartDate' && value === true){
        settings.isEndDate = true;
      }
      if (name === 'isStartHour' && value === true){
        settings.isEndHour = true;
      }

      this.setState({
        settings
      });
    } else {
      this.setState({
        [name]: value
      }, this.handleValidation());
    }

  };

  handleServiceCategoryChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.props.servicesCategories.map(category => {
      if (category.slug === value) {
        this.setState({
          [name]: value,
          selectedServiceCategory: category,
          serviceTypes: category.types
        });
      }
    });
  };

  handleServiceTypeChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.state.serviceTypes.map(type => {
      if (type.slug === value) {
        this.setState({
          [name]: value,
          selectedServiceType: type
        });
      }
    });
  };

  handleValidation() {
    // const pathRegex = '^((\.\./|[a-zA-Z0-9_/\-\\])*\.[a-zA-Z0-9]+)$';
    // const nameRegex = '^[A-Z][-a-zA-Z]+$';
    if (this.state.service.title && this.state.service.title.length > 0) {
      this.setState({ inputError: false, disableConfirmButton: false });
    } else {
      this.setState({ inputError: true, disableConfirmButton: true });
    }
  }

  handleNext = () => {
    this.setState((prevState) => ({
      activeStep: prevState.activeStep + 1,
    }));
  };

  handleBack = () => {
    this.setState((prevState) => ({
      activeStep: prevState.activeStep - 1,
    }));
  };

  handleReset = () => {
    this.setState({
      activeStep: 0,
    });
  };

  renderCreateServiceSettings = (classes, services) => {
    return (
      <Grid container spacing={40} className={classes.mainSubHeaderContainer}>
        <Grid item xs={12} sm={6}>

          <FormControl fullWidth={true}>
            <FormControlLabel
              control={
                <Switch
                  data-tid="enableAdults"
                  name="isAdultService"
                  checked={this.state.settings.isAdultService}
                  onChange={this.handleInputChange}
                />
              }
              label={i18n.t('core:isEnableAdults')}
            />
          </FormControl>

          <FormControl fullWidth={true}>
            <FormControlLabel
              control={
                <Switch
                  data-tid="enableChildren"
                  name="isChildrenService"
                  checked={this.state.settings.isChildrenService}
                  onChange={this.handleInputChange}
                />
              }
              label={i18n.t('core:isEnableChildren')}
            />
          </FormControl>

          <FormControl fullWidth={true}>
            <FormControlLabel
              control={
                <Switch
                  data-tid="enableOrderPayment"
                  name="isPayment"
                  checked={this.state.settings.isPayment}
                  onChange={this.handleInputChange}
                />
              }
              label={i18n.t('core:isPayment')}
            />
          </FormControl>

          <FormControl fullWidth={true}>
            <FormControlLabel
              control={
                <Switch
                  data-tid="enableLimitOrderQuantity"
                  name="isLimitOrderQuantity"
                  checked={this.state.settings.isLimitOrderQuantity}
                  onChange={this.handleInputChange}
                />
              }
              label={i18n.t('core:isLimitOrderQuantity')}
            />
          </FormControl>

          <FormControl fullWidth={true}>
            <FormControlLabel
              control={
                <Switch
                  data-tid="enableRecurring"
                  name="isRecurring"
                  checked={this.state.settings.isRecurring}
                  onChange={this.handleInputChange}
                />
              }
              label={i18n.t('core:isRecurring')}
            />
          </FormControl>

          <FormControl fullWidth={true}>
            <FormControlLabel
              control={
                <Switch
                  data-tid="enableDateCalendar"
                  name="isDateCalendar"
                  checked={this.state.settings.isDateCalendar}
                  onChange={this.handleInputChange}
                />
              }
              label={i18n.t('core:isDateCalendar')}
            />
          </FormControl>

          <FormControl fullWidth={true}>
            <FormControlLabel
              control={
                <Switch
                  data-tid="enableStartDate"
                  name="isStartDate"
                  checked={this.state.settings.isStartDate}
                  onChange={this.handleInputChange}
                />
              }
              label={i18n.t('core:isStartDate')}
            />
          </FormControl>

          <FormControl fullWidth={true}>
            <FormControlLabel
              control={
                <Switch
                  data-tid="enableEndDate"
                  name="isEndDate"
                  checked={this.state.settings.isEndDate}
                  onChange={this.handleInputChange}
                />
              }
              label={i18n.t('core:isEndDate')}
            />
          </FormControl>

          <FormControl fullWidth={true}>
            <FormControlLabel
              control={
                <Switch
                  data-tid="enableStartHour"
                  name="isStartHour"
                  checked={this.state.settings.isStartHour}
                  onChange={this.handleInputChange}
                />
              }
              label={i18n.t('core:isStartHour')}
            />
          </FormControl>

          <FormControl fullWidth={true}>
            <FormControlLabel
              control={
                <Switch
                  data-tid="enableEndHour"
                  name="isEndHour"
                  checked={this.state.settings.isEndHour}
                  onChange={this.handleInputChange}
                />
              }
              label={i18n.t('core:isEndHour')}
            />
          </FormControl>
        </Grid>

        <Grid item xs={12} sm={6}>
          <FormControl fullWidth={true}>
            <TextField
              id="createServiceCategory"
              select
              label={i18n.t('core:selectServiceCategory')}
              value={this.state.serviceCategory.length > 1 ? this.state.serviceCategory : services[0].name}
              name="serviceCategory"
              data-tid="createServiceCategory"
              onChange={this.handleServiceCategoryChange}
              SelectProps={{
                MenuProps: {
                  className: classes.menu,
                },
              }}
              helperText={i18n.t('core:helperTextSelectServiceCategory')}
              margin="normal"
            >
              {services[0] && services.map(service => (
                <MenuItem key={service.slug} value={service.slug}>
                  {service.name}
                </MenuItem>
              ))}
            </TextField>
          </FormControl>

          {this.state.serviceTypes && (
            <FormControl fullWidth={true}>
              <TextField
                id="createServiceType"
                select
                label={i18n.t('core:selectServiceCategory')}
                value={this.state.serviceType.length > 1 ? this.state.serviceType : services[0].name}
                name="serviceType"
                data-tid="createServiceType"
                onChange={this.handleServiceTypeChange}
                SelectProps={{
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
                helperText={i18n.t('core:helperTextSelectServiceType')}
                margin="normal"
              >
                {this.state.serviceTypes && this.state.serviceTypes.map(type =>
                  <MenuItem key={type.slug} value={type.slug}>
                    {type.name}
                  </MenuItem>
                )}
              </TextField>
            </FormControl>
          )}

          {this.state.settings.isPayment && (
            <FormControl fullWidth={true}>
              <TextField
                id="createServiceSelectCurrency"
                select
                label={i18n.t('core:selectCurrency')}
                value={this.state.currency}
                name="currency"
                onChange={this.handleInputChange}
                SelectProps={{
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
                helperText={i18n.t('core:helperTextSelectCurrency')}
                margin="normal"
              >
                {supportedCurrencies.map(option => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
            </FormControl>
          )}

          <FormControl fullWidth={true}>
            <TextField
              id="createServiceSelectLanguage"
              select
              label={i18n.t('core:selectLanguage')}
              value={this.state.language}
              name="language"
              onChange={this.handleInputChange}
              SelectProps={{
                MenuProps: {
                  className: classes.menu,
                },
              }}
              helperText={i18n.t('core:helperTextSelectLanguage')}
              margin="normal"
            >
              {this.props.serviceSupportedLanguages && this.props.serviceSupportedLanguages.map(option => (
                <MenuItem key={option.tag} value={option}>
                  {option.name}
                </MenuItem>
              ))}
            </TextField>
          </FormControl>

        </Grid>
      </Grid>
    );
  };

  renderCreateService = (classes) => {
    return (
        <CreateServiceComponent
          handleUpdateService={this.handleUpdateService}
          service={this.state.service}
          settings={this.state.settings}
          classes={classes}
        />
    );
  };

  renderPreviewCreatedService = () => {
    return (
      <PreviewServiceComponent
        service={{
          category: this.state.selectedServiceCategory,
          type: this.state.selectedServiceType,
          image: this.state.service.image,
          imagePreviewURL: this.state.service.imagePreviewURL,
          currency: this.state.currency,
          title: this.state.service.title,
          price: this.state.service.price,
          description: this.state.service.description,
          location: this.state.service.location,
          content: this.state.service.content,
          place: this.state.service.place,
          date: this.state.service.date || this.state.service.startDate || '',
        }}
      />
    )
  };

  getStepContent = (stepIndex, classes, services) => {
    switch (stepIndex) {
      case 0:
        return this.renderCreateServiceSettings(classes, services);
      case 1:
        return this.renderCreateService(classes);
      case 2:
        return this.renderPreviewCreatedService(classes);
      default:
        return this.renderCreateServiceSettings(classes, services);
    }
  };

  render() {
    const { classes } = this.props;
    const { activeStep, steps } = this.state;
    console.log('Create Service Page - Props:', this.props);
    console.log('Create Service Page - State:', this.state);

    return (
      <div data-tid="CreateServicePage">
        <Paper className={classes.root} elevation={4}>
          <div className={classes.contentHeader}>
            <Typography variant="headline" component="h3" className={classes.typographyTitle}>
              {i18n.t('core:createServiceHeader')}
            </Typography>
            <Typography variant="headline" component="p" className={classes.typographyOffset}>
              {i18n.t('core:createServiceHeaderDescription')}
            </Typography>
          </div>

          <Stepper activeStep={activeStep} alternativeLabel>
            {steps.map(label => {
              return (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                </Step>
              );
            })}
          </Stepper>

          <div>
            {this.state.activeStep === steps.length ? (
              <div>
                <Typography className={classes.instructions}>
                  {i18n.t('core:createServiceStepsCompleted')}
                </Typography>
                <Button onClick={this.handleReset}>{i18n.t('core:reset')}</Button>
              </div>
            ) : (
              <div>
                {/*<Typography className={classes.instructions}>*/}
                {this.props.servicesCategories[0] ? this.getStepContent(activeStep, classes, this.props.servicesCategories) :
                  'You should inform your admin about this action. To be able to create services, the admin needs to add the correct type to the service.'}
                {/*</Typography>*/}
                <div>
                  <Button
                    disabled={activeStep === 0}
                    onClick={this.handleBack}
                    className={classes.backButton}
                  >
                    {i18n.t('core:back')}
                  </Button>
                  {activeStep === steps.length - 1 ? (
                    <Button
                      disabled={!(activeStep === steps.length - 1)}
                      variant="raised"
                      color="primary"
                      className={classes.buttonAlign}
                      onClick={(event) => this.handleCreateService(event)}
                    >
                      {i18n.t('core:createService')}
                    </Button>
                  ) : (
                    <Button
                      variant="raised"
                      color="primary"
                      className={classes.buttonAlign}
                      onClick={this.handleNext}
                    >
                      {i18n.t('core:next')}
                    </Button>
                  )
                  }
                </div>
              </div>
            )}
          </div>
          {/*{this.renderAlert()}*/}
        </Paper>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    content: state.auth.content,
    users: state.user.services,
    servicesCategories: getServicesCategories(state),
    serviceSupportedLanguages: getServicesLanguages(state)
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ createService: fetchCreateService, fetchServiceCategory, fetchServiceLanguage }, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(translate(['core'], { wait: true })(CreateServicePage))));
