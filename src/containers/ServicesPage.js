import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card';
import { Typography, Button, Paper, Grid, withStyles } from 'material-ui';
import { connect } from 'react-redux';
import cookie from 'react-cookie';
import { fetchGroupServices } from '../actions/services';
import { actions as AppAuthActions,  getContent } from '../reducers/auth';
import { actions as ServicesAction, getServicesCategories } from '../reducers/user';
import i18n from '../services/i18n';
import LearnMoreDialog from '../components/dialogs/LearnMoreDialog';
import SearchBar from '../components/SearchBarComponent';

const styles = {
  card: {
    margin: '5px',
    minWidth: 340,
    maxWidth: 340,
  },
  media: {
    height: 200,
  },
  eventsList: {
    marginLeft: '3%',
    display: 'flex'
  },
  mainPage: {
    marginLeft: '2.7%',
    marginRight: '3.2%',
    marginBottom: '5%'
  },
  gridContainer: {
    display: 'grid',
    width: '97%',
    gridTemplateColumns: 'repeat(auto-fit,minmax(340px,0fr))',
    gridAutoRows: 'minmax(250px,auto)',
    gridGap: '10px 15px',
    marginLeft: '2.3%'
  },
  gridNewsContainer: {
    display: 'grid',
    width: '90%',
    gridTemplateColumns: 'repeat(auto-fit,minmax(550px,0fr))',
    gridAutoRows: 'minmax(550px,auto)',
    gridGap: '10px 15px',
    marginLeft: '10%'
  },
  rowContainer: {
    display: 'grid',
    width: '99%',
    gridTemplateColumns: 'auto',
    gridGap: '1px 1px',
    padding: 1,
    margin: 1,
    marginTop: 62
  },
  gridCell: {
    paddingTop: 3,
    paddingLeft: 3,
    paddingRight: 3
  },
  gridCellThumb: {
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    height: 150
  },
  gridCellTitle: {
    padding: 5
  },
  gridCellDescription: {
    padding: 5,
    height: 50
  },
  price: {
    fontWeight: 500,
    textTransform: 'uppercase',
    textDecoration: 'none',
    justify: 'center',
    marginTop: '-33.7px',
    width: '15%',
    padding: '2%',
    background: '#fc3752',
    color: '#fefefe',
  },
  date: {
    marginLeft: '80%',
    marginTop: '-12%',
    fontWeight: 450,
    textTransform: 'uppercase',
    textDecoration: 'none',
    padding: '1%',
    background: '#fc3752',
    color: '#fefefe',
    width: '3.7em',
    height: '3.7em',
    borderRadius: '3em',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    opacity: '0.9',
    margin: '1em',
  },
  mainSubHeaderContainer: {
    marginTop: '5px',
    marginBottom: '5px'
  },
};

function TabContainer(props) {
  const { children, dir } = props;

  return (
    <Typography component="div" dir={dir} style={{ marginLeft: "-2.8%" }}>
      {children}
    </Typography>
  );
}

type Props = {

};

type State = {
  value: number,
  isLearnMoreDialogOpened: boolean,
  learnMoreAnchorEl: Object,
  selectedEvent: string,
  searchCategories: Object,
  searchResults: Object,
};

class ServicesPage extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.user = cookie.load("user"); //
  }

  state = {
    searchCategories: [],
    searchResults: [],
    isLearnMoreDialogOpened: false,
    learnMoreAnchorEl: null,
    selectedService: '',
    servicesCategories: '',
    services: [],
    types: []
  };

  componentDidMount (nextProps, nextState) {
    this.props.fetchGroupServices();
    //this.props.fetchNews();
  }

  toggleLearnMore  = (event, proposedEvent) => {
    this.setState((prevState) => ({
      isLearnMoreDialogOpened: !prevState.isLearnMoreDialogOpened,
      learnMoreAnchorEl: event ? event.currentTarget : null,
      selectedService: proposedEvent || ''
    }));
  };

  handleBooking = (event, service) => {
    if (this.user) {
      this.props.router.push('/services/order/' + service._id);
    } else {
      this.props.router.push('login');
    }
  };

  handleLearnMore = (event, proposedEvent) => {
    this.toggleLearnMore(event, proposedEvent);
  };

  renderCardEvent = (proposedEvent) => {
    const { classes } = this.props;
    const date = new Date(proposedEvent.date).getDate();

    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    const month = new Date(proposedEvent.date).getMonth();

    return (
      <div className={classes.gridCell}>
        <Card className={classes.card} key={proposedEvent.uuid}>
          <CardMedia
            className={classes.media}
            image={proposedEvent.image}
            title={proposedEvent.title}
          />
          <div className={classes.price}>$ {proposedEvent.price}</div>
          <div className={classes.date}>{date + " " + months[month]}</div>
          <CardContent>
            <Typography gutterBottom variant="headline" component="h2">
              {proposedEvent.title}
            </Typography>
            <Typography component="p">
              {proposedEvent.description}
            </Typography>
          </CardContent>
          <CardActions>
            <Button
              size="small"
              color="primary"
              onClick={event => this.handleBooking(event, proposedEvent)}
            >
              {i18n.t('core:book')}
            </Button>
            <Button
              size="small"
              color="primary"
              onClick={event => this.handleLearnMore(event, proposedEvent)}
            >
              {i18n.t('core:learnMore')}
            </Button>
          </CardActions>
        </Card>
      </div>
    );
  };

  render() {
    console.log("ServicesPage:", this.props);
    const { classes } = this.props;

    return (
      <div className={classes.mainPage} data-tid="mainPage">

        {this.props.servicesCategories && (
          <div>

            <Typography component="div">
              <h4 className="main-headline">{i18n.t('core:ourServices')}</h4>
            </Typography>

            <Grid container spacing={40}>
              <Grid item xs={12} sm={4}>
                <SearchBar searchCategories={this.props.servicesCategories} />
              </Grid>
              <Grid item xs={12} sm={8}>
                <Paper>
                  <div className={classes.gridContainer} data-tid="mainPageCardsEvents">
                    {this.state.searchResults.length > 0 && this.state.searchResults.map(service => {
                      return this.renderCardEvent(service);
                    })}
                  </div>
                </Paper>
              </Grid>
            </Grid>
          </div>
        )}

        <LearnMoreDialog
          open={this.state.isLearnMoreDialogOpened}
          onClose={this.toggleLearnMore}
          selectedService={this.state.selectedService}
          handleBooking={this.handleBooking}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    content: getContent(state),
    servicesCategories: getServicesCategories(state),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    ...AppAuthActions, ...ServicesAction, fetchGroupServices
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, { withTheme: true })(ServicesPage));
