import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import ExpansionPanel from 'material-ui/ExpansionPanel/ExpansionPanel';
import ExpansionPanelSummary from 'material-ui/ExpansionPanel/ExpansionPanelSummary';
import ExpansionPanelDetails from 'material-ui/ExpansionPanel/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginLeft: "3%",
    justify: 'center',
    width: "91%",
    marginBottom: "5%",
  }),
  mainContent: theme.mixins.gutters({
    marginTop: '2%',
    marginBottom: "2%"
  }),
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  buttonAlign: {
    marginTop: 15,
    width: '100%'
  },
  headTitle: {
    color: '#fc3752',
    paddingTop: 15,
    paddingBottom: 15,
    marginTop: 15,
    marginBottom: 10
  },
  heading: {
    backgroundColor: '#fff0',
    color: '#fc3752',
  },
  typographyOffset: {
    color: '#444444',
    lineHeight: '1.54',
    paddingBottom: '2%',
    fontSize: 15,
  },
  expansionPanel: {
    boxShadow: 'none'
  }
});

class TermsOfService extends Component {
  state = {
    expanded: null,
  };

  handleChangeExpandedPanel = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  render() {
    const { expanded } = this.state;
    const { classes } = this.props;

    return (
      <div data-tid="serviceRequestPage" className={classes.root}>
        <Grid container spacing={40}>
          <Grid item xs={12} sm={12}>
            {/*<Paper className={classes.mainContent} elevation={4}>*/}
              <Typography variant="headline" component="h3" className={classes.headTitle}>AgeWeb Privacy Policy</Typography>

              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                When you use our services, you’re trusting us with your information. We understand this is a big responsibility and work hard to protect your information and put you in control.
              </Typography>

              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                This Privacy Policy is meant to help you understand what information we collect, why we collect it, and how you can update, manage, export, and delete your information.
              </Typography>

              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                We look forward to your visit to our website, where we offer you personalized information about our company and our services.
                We consider transparency and integrity important issues to consider in the processing of your personal data.
                We observe data protection regulations, namely the EU General Data Protection Regulation (“GDPR”), the Federal Data Protection Act (“BDSG”) and the Telemedia Act (“TMG”).
              </Typography>

              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                In this Privacy Notice, we explain what information (including personal data) we process during your visit and
                use of our above internet offering (“Website”) and what rights you have over your personal information.
              </Typography>

              <div className={classes.profileSettingPanel}>

                <ExpansionPanel className={classes.expansionPanel} expanded={expanded === 'panel1'} onChange={this.handleChangeExpandedPanel('panel1')}>

                  <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading} variant="headline" component="h3">I. Who is responsible for processing data?</Typography>
                  </ExpansionPanelSummary>

                  <ExpansionPanelDetails>
                    <Grid container spacing={24}>
                      <Typography variant="headline" component="p" className={classes.typographyOffset}>
                        The party responsible (under data protection law) for the processing of personal data is AgeWeb GmbH, Großenbaumer Weg, D-40472 Dusseldorf, 0180 6 320 320. Any reference to “we” or “us” in these data protection instructions refers in each case to the aforementioned company.
                      </Typography>
                    </Grid>
                  </ExpansionPanelDetails>
                </ExpansionPanel>

                <ExpansionPanel  className={classes.expansionPanel} expanded={expanded === 'panel2'} onChange={this.handleChangeExpandedPanel('panel2')}>

                  <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading} variant="headline" component="h3">II. What principles do we observe?</Typography>
                  </ExpansionPanelSummary>

                  <ExpansionPanelDetails>
                    <Grid container spacing={24}>
                      <Typography variant="headline" component="p" className={classes.typographyOffset}>
                        In compliance with data protection regulations, we process your personal data only if permitted by law or if you have given your consent. This also applies to the processing of personal data for advertising and marketing purposes.

                        We may also collect information on this website that cannot be used by itself to identify you personally.
                        In certain cases, especially when combined with other data, this information can nonetheless be considered “personal data” as defined by data protection legislation.
                        We may also collect information on this website that does not allow us to identify you, either directly or indirectly; this includes, for example, aggregated information about all users of this website.
                      </Typography>
                    </Grid>
                  </ExpansionPanelDetails>
                </ExpansionPanel>

                <ExpansionPanel  className={classes.expansionPanel} expanded={expanded === 'panel3'} onChange={this.handleChangeExpandedPanel('panel3')}>
                  <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading} variant="headline" component="h3">
                      III. What data do we process? For what purposes and on what legal basis does this processing take place?
                    </Typography>
                  </ExpansionPanelSummary>

                  <ExpansionPanelDetails>
                    <Grid container spacing={24}>
                      <Typography variant="headline" component="p" className={classes.typographyOffset}>
                        You can access our website without providing direct personal information (such as your name, postal address, or email address).
                        Again, we do need to collect and store certain information so as to enable you to access our website.
                        We also use certain analytical methods and integrated third-party functionalities on our website.
                        In addition, we offer some functions on our website, for which you must provide personal data.

                        We collect and process personal data in the following scope:
                      </Typography>
                    </Grid>
                  </ExpansionPanelDetails>
                </ExpansionPanel>

                <ExpansionPanel  className={classes.expansionPanel} expanded={expanded === 'panel4'} onChange={this.handleChangeExpandedPanel('panel4')}>
                  <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading} variant="headline" component="h3">
                      IV. Am I required to provide data?
                    </Typography>
                  </ExpansionPanelSummary>

                  <ExpansionPanelDetails>
                    <Grid container spacing={24}>
                      <Grid item xs={12} sm={12}>
                        <Typography variant="headline" component="p" className={classes.typographyOffset}>
                          In addition, if we collect personal information from you, we will tell you at the time we collect it whether the provision of that information is required by law or is required to execute a contract. In doing so, we generally identify any information that is provided on a voluntary basis and not according to any of the above obligations or not required to execute a contract.
                        </Typography>
                      </Grid>
                    </Grid>
                  </ExpansionPanelDetails>
                </ExpansionPanel>

                <ExpansionPanel  className={classes.expansionPanel} expanded={expanded === 'panel5'} onChange={this.handleChangeExpandedPanel('panel5')}>
                  <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading} variant="headline" component="h3">
                      V. Who receives my data?
                    </Typography>
                  </ExpansionPanelSummary>

                  <ExpansionPanelDetails>
                    <Grid container spacing={24}>
                      <Grid item xs={12} sm={12}>
                        <Typography variant="headline" component="p" className={classes.typographyOffset}>
                          Your personal data is generally processed within our company. Depending on the type of personal information, only certain departments/organizational units have access to your personal information. These include, in particular, the specialist departments involved in the provision of our services and our IT department. A role and authorization concept limits access within our organization to those functions and to the extent required for the particular purpose of the processing.

                          We may also transfer your personal information to third parties outside our company to the extent permitted by law. In particular, these external receivers may include the following:
                        </Typography>
                      </Grid>
                    </Grid>
                  </ExpansionPanelDetails>
                </ExpansionPanel>

                <ExpansionPanel  className={classes.expansionPanel} expanded={expanded === 'panel6'} onChange={this.handleChangeExpandedPanel('panel6')}>
                  <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading} variant="headline" component="h3">
                      VI. Is there automated decision-making?
                    </Typography>
                  </ExpansionPanelSummary>

                  <ExpansionPanelDetails>
                    <Grid container spacing={24}>
                      <Grid item xs={12} sm={12}>
                        <Typography variant="headline" component="p" className={classes.typographyOffset}>
                          In general we do not use any automated decision making (including profiling) in connection with users of our website, as per Article 22 of GDPR. If we use such procedures in individual cases, we will inform you separately about this to the legally required extent.
                        </Typography>
                      </Grid>
                    </Grid>
                  </ExpansionPanelDetails>
                </ExpansionPanel>

                <ExpansionPanel  className={classes.expansionPanel} expanded={expanded === 'panel7'} onChange={this.handleChangeExpandedPanel('panel7')}>
                  <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading} variant="headline" component="h3">
                      VII. Will data be transmitted to countries outside of the EU/EEA?
                    </Typography>
                  </ExpansionPanelSummary>

                  <ExpansionPanelDetails>
                    <Grid container spacing={24}>
                      <Grid item xs={12} sm={12}>
                        <Typography variant="headline" component="p" className={classes.typographyOffset}>
                          In principle, the processing of your personal data takes place within the EU or the European Economic Area.
                        </Typography>

                        <Typography variant="headline" component="p" className={classes.typographyOffset}>
                          In certain cases, information may be transmitted to recipients in so-called “third countries”. “Third countries” are countries outside of the European Union or the European Economic Area, for which it is not possible to assume a level of data protection comparable to that of the European Union.
                        </Typography>

                        <Typography variant="headline" component="p" className={classes.typographyOffset}>
                          If the information provided includes personal data and we do not have a legal obligation of disclosure (such as Advance Passenger Information),
                          we will ensure that the third country or the recipient in the third country has required a sufficient level of data protection.
                          This may be the result, in particular, of a so-called “adequacy decision” by the European Commission,
                          which establishes an adequate level of data protection for a given third country as a whole.
                          Alternatively, we may also base the transfer of data on so-called “EU standard contractual clauses” agreed with a recipient or,
                          in the case of US recipients, on compliance with the principles of the so-called “EU-US Privacy Shield”.
                          We will gladly provide you with further information at your request about the applicable guarantees for maintaining an adequate level of data protection;
                        </Typography>
                      </Grid>
                    </Grid>
                  </ExpansionPanelDetails>
                </ExpansionPanel>

                <ExpansionPanel className={classes.expansionPanel} expanded={expanded === 'panel8'} onChange={this.handleChangeExpandedPanel('panel8')}>
                  <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading} variant="headline" component="h3">
                      VIII. How long will my data be saved?
                    </Typography>
                  </ExpansionPanelSummary>

                  <ExpansionPanelDetails>
                    <Grid container spacing={24}>
                      <Grid item xs={12} sm={12}>
                        <Typography variant="headline" component="p" className={classes.typographyOffset}>
                          In principle, we store your personal data as long as we have a legitimate interest in its storage, and we do not consider our importance to outweigh your interests in the non-continuation of the storage.
                        </Typography>

                        <Typography variant="headline" component="p" className={classes.typographyOffset}>
                          Even without a legitimate interest, we can continue to store the data if we are legally obligated to do so (for example, to fulfil record-keeping obligations).
                          We also delete your personal data without your involvement as soon as its retention is no longer necessary to fulfil the purpose for which it was processed,
                          or in cases where storing your data is otherwise legally inadmissible.
                        </Typography>
                      </Grid>
                    </Grid>
                  </ExpansionPanelDetails>
                </ExpansionPanel>

                <ExpansionPanel className={classes.expansionPanel} expanded={expanded === 'panel9'} onChange={this.handleChangeExpandedPanel('panel9')}>
                  <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading} variant="headline" component="h3">
                      IX. What rights do I have?
                    </Typography>
                  </ExpansionPanelSummary>

                  <ExpansionPanelDetails>
                    <Grid container spacing={24}>
                      <Grid item xs={12} sm={12}>
                        <Typography variant="headline" component="p" className={classes.typographyOffset}>
                          In principle, we store your personal data as long as we have a legitimate interest in its storage, and we do not consider our importance to outweigh your interests in the non-continuation of the storage.
                        </Typography>
                      </Grid>
                    </Grid>
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              </div>

              <Typography variant="headline" component="h3" className={classes.headTitle}>Privacy and Copyright Protection</Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                AgeWeb's privacy policies explain how we treat your personal data and protect your privacy when you use our Services.
                By using our Services, you agree that AgeWeb can use such data in accordance with our privacy policies.
              </Typography>

              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                We respond to notices of alleged copyright infringement and
                terminate accounts of repeat infringers according to the process set out in the U.S. Digital Millennium Copyright Act.
              </Typography>

              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                We provide information to help copyright holders manage their intellectual property online.
                If you think somebody is violating your copyrights and want to notify us,
                you can find information about submitting notices and AgeWeb’s policy about responding to notices in our Help Center.
              </Typography>
            {/*</Paper>*/}
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(TermsOfService);
