import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withStyles } from 'material-ui/styles/index';
import { translate } from 'react-i18next';
import SwipeableDrawer from 'material-ui/SwipeableDrawer';
import RetirementClubsTable from '../../components/admin/RetirementClubsTable';
import EditRetirmentClubComponent from '../../components/EditRetirmentClubComponent';
import ConfirmDialog from '../../components/dialogs/ConfirmDialog';
import { fetchClubs, fetchClub, fetchEditClubPromise } from '../../actions/clubs';
import { actions as ClubsAction, getClubs } from '../../reducers/clubs';
import { fetchServiceCategory, fetchServicesTypes } from '../../actions/services';
import { getServicesCategories, getServicesTypes } from '../../reducers/user';

const styles = theme => ({
  paperDrawer: {
    width: '80%'
  }
});

type Props = {};

type State = {
  rightDrawer: string,
  isEditClubOpened: string,
  isDeleteClubsDialogOpened: string,
  isEditClubsDialogOpened: string
};

class RetirementClubsDashboard extends Component<Props, State> {
  constructor(props) {
    super(props);
  }

  state = {
    isDeleteClubsDialogOpened: false,
    isEditClubsDialogOpened: false,
    isEditClubOpened: false,
    selectedClub: {},
    right: false,
    top: false,
  };

  componentDidMount () {
    // Fetch all orders
    this.props.fetchClubs();
  }

  showDeleteClubDialog = (club) => {
    this.setState({
      isDeleteClubsDialogOpened: true,
      selectedClub: club
    });
  };

  showEditClubDialog = (selectedClub) => {
    this.setState({
      isEditClubsDialogOpened: true,
      selectedClub: selectedClub,
      right: true,
    });
  };

  handleCloseDialogs = () => {
    this.setState({
      isDeleteClubsDialogOpened: false,
      isEditClubsDialogOpened: false,
    });
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  toggleClubDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };

  render() {
    console.log(' Clubs Dashboard(State): ', this.state);
    console.log(' Clubs Dashboard(Props): ', this.props);

    return (
      <div>
        <RetirementClubsTable
          allClubs={this.props.allClubs}
          showDeleteClubDialog={this.showDeleteClubDialog}
          showEditClubDialog={this.showEditClubDialog}
        />

        <ConfirmDialog
          open={this.state.isDeleteClubsDialogOpened}
          onClose={this.handleCloseDialogs}
          title={'Delete Club'}
          content={'Do you want to delete this club: {{club}} ?'}
          confirmCallback={event => {
            if (event) {
              this.props.fetchClub('DELETE', this.state.selectedOrder._id);
            }
          }}
          confirmDialogContent={'confirmDialogContent'}
          cancelDialogActionId={'cancelDeleteClubDialog'}
          confirmDialogActionId={'confirmDeleteClubDialog'}
        />

        <SwipeableDrawer
          anchor="right"
          classes={{ paper: this.props.classes.paperDrawer }}
          open={this.state.right}
          onClose={this.toggleClubDrawer('right', false)}
          onOpen={this.toggleClubDrawer('right', true)}
        >
          <div
            tabIndex={0}
            role='button'
            // onClick={this.toggleClubDrawer('right', false)}
            // onKeyDown={this.toggleClubDrawer('right', false)}
          >
            <EditRetirmentClubComponent
              selectedClub={this.state.selectedClub}
              servicesCategories={this.props.servicesCategories}
              serviceTypes={this.props.serviceTypes}
              fetchEditClub={this.props.fetchEditClubPromise}
            />
          </div>
        </SwipeableDrawer>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    content: state.auth.content,
    allClubs: getClubs(state),
    servicesCategories: getServicesCategories(state),
    serviceTypes: getServicesTypes(state),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...ClubsAction, fetchClubs, fetchClub, fetchEditClubPromise, fetchServiceCategory, fetchServicesTypes }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, { withTheme: true })(translate(['core'], { wait: true })(RetirementClubsDashboard)));
