import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withStyles } from 'material-ui/styles/index';
import OrdersTable from '../../components/admin/OrderProviderTable';
import ConfirmDialog from '../../components/dialogs/ConfirmDialog';
import { fetchOrders, fetchOrder } from '../../actions/orders';
import { translate } from 'react-i18next';
import { actions as OrdersAction, getProviderOrders } from '../../reducers/orders';

type Props = {};

type State = {
  isDeleteOrdersDialogOpened: string,
  isEditOrdersDialogOpened: string
};

class OrdersDashboard extends Component<Props, State> {
  constructor(props) {
    super(props);
  }

  state = {
    isDeleteOrderDialogOpened: false,
    isEditOrderDialogOpened: false,
    selectedOrder: {}
  };

  componentDidMount () {
    // Fetch all orders
    this.props.fetchOrders();
  }

  showDeleteOrdersDialog = (order) => {
    this.setState({
      isDeleteOrderDialogOpened: true,
      selectedOrder: order
    });
  };

  showEditOrdersDialog = (order) => {
    this.setState({
      isEditOrderDialogOpened: true,
      selectedOrder: order
    });
  };

  handleCloseDialogs = () => {
    this.setState({
      isDeleteOrderDialogOpened: false,
      isEditOrderDialogOpened: false,
    });
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  render() {
    console.log('Provider Orders Dashboard(State): ', this.state);
    console.log('Provider Orders Dashboard(Props): ', this.props);

    return (
      <div>
        <OrdersTable
          allOrders={this.props.allOrders}
          showDeleteOrdersDialog={this.showDeleteOrdersDialog}
          showEditOrdersDialog={this.showEditOrdersDialog}
        />

        <ConfirmDialog
          open={this.state.isDeleteOrderDialogOpened}
          onClose={this.handleCloseDialogs}
          title={'Delete Order'}
          content={'Do you want to delete this order: {{order}} ?'}
          confirmCallback={event => {
            if (event) {
              this.props.fetchOrder('DELETE', this.state.selectedOrder._id);
            }
          }}
          confirmDialogContent={'confirmDialogContent'}
          cancelDialogActionId={'cancelDeleteOrdersDialog'}
          confirmDialogActionId={'confirmDeleteOrdersDialog'}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    content: state.auth.content,
    allOrders: getProviderOrders(state)
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...OrdersAction, fetchOrders, fetchOrder }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles({ withTheme: true })(translate(['core'], { wait: true })(OrdersDashboard)));
