import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withStyles } from 'material-ui/styles/index';
import { translate } from 'react-i18next';
import NewsTable from '../../components/admin/NewsTable';
import EditNewsComponent from '../../components/EditNewsComponent';
import ConfirmDialog from '../../components/dialogs/ConfirmDialog';
import SwipeableDrawer from 'material-ui/SwipeableDrawer';
import { fetchNews, fetchNewsById } from '../../actions/news';
import { actions as NewsAction, getNews } from '../../reducers/news';
import { fetchEditNewsPromise } from '../../actions/news';
import { fetchServiceCategory, fetchServicesTypes} from '../../actions/services';
import { getServicesCategories, getServicesTypes} from '../../reducers/user';

const styles = theme => ({
  paperDrawer: {
    width: '80%'
  }
});

type Props = {};

type State = {
  rightDrawer: string,
  isEditNewsOpened: string,
  isDeleteNewsDialogOpened: string,
  isEditNewsDialogOpened: string
};

class NewsDashboard extends Component<Props, State> {
  constructor(props) {
    super(props);
  }

  state = {
    isDeleteNewsDialogOpened: false,
    isEditNewsDialogOpened: false,
    isEditNewsOpened: false,
    selectedNews: {},
    right: false,
    top: false,
  };

  componentDidMount () {
    // Fetch all orders
    this.props.fetchNews();
    // this.props.fetchServiceCategory('GET');
    // this.props.fetchServicesTypes('GET');
  }

  showDeleteNewsDialog = (news) => {
    this.setState({
      isDeleteNewsDialogOpened: true,
      selectedNews: news
    });
  };

  showEditNewsDialog = (selectedNews) => {
    this.setState({
      right: true,
      selectedNews: selectedNews
    });
    // this.toggleNewsDrawer('right', true, selectedNews);
  };

  handleCloseDialogs = () => {
    this.setState({
      isDeleteNewsDialogOpened: false,
      isEditNewsDialogOpened: false,
      isEditNewsOpened: false,
    });
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  toggleNewsDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };

  render() {
    console.log('Admin News Dashboard(State): ', this.state);
    console.log('Admin News Dashboard(Props): ', this.props);

    return (
      <div>
        <NewsTable
          news={this.props.news}
          showDeleteNewsDialog={this.showDeleteNewsDialog}
          showEditNewsDialog={this.showEditNewsDialog}
        />

        <ConfirmDialog
          open={this.state.isDeleteNewsDialogOpened}
          onClose={this.handleCloseDialogs}
          title={'Delete Order'}
          content={'Do you want to delete this news: {{news}} ?'}
          confirmCallback={event => {
            if (event) {
              this.props.fetchNewsById('DELETE', this.state.selectedNews._id);
            }
          }}
          confirmDialogContent={'confirmDialogContent'}
          cancelDialogActionId={'cancelDeleteOrdersDialog'}
          confirmDialogActionId={'confirmDeleteOrdersDialog'}
        />

        <SwipeableDrawer
          anchor="right"
          classes={{ paper: this.props.classes.paperDrawer }}
          open={this.state.right}
          onClose={this.toggleNewsDrawer('right', false)}
          onOpen={this.toggleNewsDrawer('right', true)}
        >
          <div
            tabIndex={0}
            role='button'
            // onClick={this.toggleNewsDrawer('right', false)}
            // onKeyDown={this.toggleNewsDrawer('right', false)}
          >
            <EditNewsComponent
              selectedNews={this.state.selectedNews}
              servicesCategories={this.props.servicesCategories}
              serviceTypes={this.props.serviceTypes}
              fetchEditNews={this.props.fetchEditNewsPromise}
            />
          </div>
        </SwipeableDrawer>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    content: state.auth.content,
    news: getNews(state),
    servicesCategories: getServicesCategories(state),
    serviceTypes: getServicesTypes(state),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...NewsAction, fetchNews, fetchNewsById, fetchServiceCategory, fetchEditNewsPromise, fetchServicesTypes }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, { withTheme: true })(translate(['core'], { wait: true })(NewsDashboard)));
