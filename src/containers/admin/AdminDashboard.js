import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withStyles } from 'material-ui/styles/index';
import { Accessibility, ContentCopy, DateRange, InfoOutline, LocalOffer, Store, Update, Warning } from "@material-ui/icons/index";
import Grid from 'material-ui/Grid';
import { translate } from 'react-i18next';
import { actions as AppAdminActions,
  fetchAllUsers,
  fetchUserByAdmin,
  fetchInviteUser
} from '../../actions/admin';
import ItemGrid from '../../components/Grid/ItemGrid';
import StatsCard from '../../components/Cards/StatsCard';
import UserTable from '../../components/admin/UsersTable';
import ConfirmDialog from '../../components/dialogs/ConfirmDialog';
import InviteUserDialog from '../../components/dialogs/InviteUserDialog';
import AddUserDialog from '../../components/dialogs/AddUserDialog';
import EditUserDialog from '../../components/dialogs/EditUserDialog';

type Props = {};

type State = {
  isDeleteUserDialogOpened: string,
  isAddUserDialogOpened: string,
  isEditUserDialogOpened: string,
  isInviteUserEmailDialogOpened: string
};

class AdminDashboard extends Component<Props, State> {
  constructor(props) {
    super(props);
  }

  state = {
    isDeleteUserDialogOpened: false,
    isAddUserDialogOpened: false,
    isEditUserDialogOpened: false,
    isInviteUserEmailDialogOpened: false,
  };

  componentDidMount (nextProps, nextState) {
    // Fetch users data prior to component mounting
    this.props.fetchAllUsers();
  }

  showDeleteUserDialog = () => {
    this.setState({ isDeleteUserDialogOpened: true });
  };

  showInviteUserEmailDialog = () => {
    this.setState({ isInviteUserEmailDialogOpened: true });
  };

  showAddUserDialog = () => {
    this.setState({ isAddUserDialogOpened: true });
  };

  showEditUserDialog = () => {
    this.setState({ isEditUserDialogOpened: true });
  };

  handleCloseDialogs = () => {
    this.setState({
      isDeleteUserDialogOpened: false,
      isAddUserDialogOpened: false,
      isEditUserDialogOpened: false,
      isInviteUserEmailDialogOpened: false,
    });
  };

  render() {
    // const { classes } = this.props;

    return (
      <div>
        <Grid container>
          <ItemGrid xs={12} sm={6} md={3}>
            <StatsCard
              icon={ContentCopy}
              iconColor="orange"
              title="Used Space"
              description="49/50"
              small="GB"
              statIcon={Warning}
              statIconColor="danger"
              statLink={{ text: "Get More Space...", href: "#pablo" }}
            />
          </ItemGrid>
          <ItemGrid xs={12} sm={6} md={3}>
            <StatsCard
              icon={Store}
              iconColor="green"
              title="Revenue"
              description="$34,245"
              statIcon={DateRange}
              statText="Last 24 Hours"
            />
          </ItemGrid>
          <ItemGrid xs={12} sm={6} md={3}>
            <StatsCard
              icon={InfoOutline}
              iconColor="red"
              title="Fixed Issues"
              description="75"
              statIcon={LocalOffer}
              statText="Tracked from Github"
            />
          </ItemGrid>
          <ItemGrid xs={12} sm={6} md={3}>
            <StatsCard
              icon={Accessibility}
              iconColor="blue"
              title="Followers"
              description="+245"
              statIcon={Update}
              statText="Just Updated"
            />
          </ItemGrid>
        </Grid>

        <UserTable
          users={this.props.users}
          showDeleteUserDialog={this.showDeleteUserDialog}
          showInviteUserEmailDialog={this.showInviteUserEmailDialog}
          showAddUserDialog={this.showAddUserDialog}
          showEditUserDialog={this.showEditUserDialog}
          fetchUserByAdmin={this.props.fetchUserByAdmin}
        />

        <ConfirmDialog
          open={this.state.isDeleteUserDialogOpened}
          onClose={this.handleCloseDialogs}
          title={'Delete User'}
          content={'Do you want to delete this user/s: {{user}} ?'}
          confirmCallback={event => {
            if (event) {
              // this.props.fetchUserByAdmin('DELETE', this.state.selectedUser);
            }
          }}
          confirmDialogContent={'confirmDialogContent'}
          cancelDialogActionId={'cancelDeleteUserDialog'}
          confirmDialogActionId={'confirmDeleteUserDialog'}
        />

        <InviteUserDialog
          inviteUser={this.props.inviteUser}
          open={this.state.isInviteUserEmailDialogOpened}
          onClose={this.handleCloseDialogs}
        />

        <AddUserDialog
          open={this.state.isAddUserDialogOpened}
          onClose={this.handleCloseDialogs}
        />

        <EditUserDialog
          open={this.state.isEditUserDialogOpened}
          onClose={this.handleCloseDialogs}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    content: state.auth.content,
    users: state.user.users
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchAllUsers, fetchUserByAdmin, inviteUser: fetchInviteUser }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles({ withTheme: true })(translate(['core'], { wait: true })(AdminDashboard)));
