import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withStyles } from 'material-ui/styles/index';
// import { translate } from 'react-i18next';
import SwipeableDrawer from 'material-ui/SwipeableDrawer';
import ServiceSetting from '../../components/admin/ServiceSetting';
import ServiceTable from '../../components/admin/ServiceTable';
import ConfirmDialog from '../../components/dialogs/ConfirmDialog';
import EditServiceComponent from '../../components/EditServiceComponent';
import {
  fetchAllServices,
  fetchServicesTypes,
  fetchServiceCategory,
  fetchGetServiceType,
  fetchService,
  fetchServiceLanguage,
  fetchCategoryTranslation,
  fetchTypeTranslation
} from '../../actions/services';
import { actions as ServicesAction,
  getAdminServices,
  getService,
  getServicesTypes,
  getServicesCategories
} from '../../reducers/user';
import { getServicesLanguages } from '../../reducers/app';

const styles = theme => ({
  paperDrawer: {
    width: '80%'
  }
});

type Props = {};

type State = {
  rightDrawer: string,
  isEditServiceOpened: string,
  isDeleteEventDialogOpened: string,
  isAddEventDialogOpened: string,
  isEditEventDialogOpened: string
};

class ServiceDashboard extends Component<Props, State> {
  constructor(props) {
    super(props);
  }

  state = {
    isEditServiceOpened: false,
    isDeleteEventDialogOpened: false,
    isAddEventDialogOpened: false,
    isEditEventDialogOpened: false,
    selectedService: {},
    right: false,
    top: false,
  };

  componentDidMount () {
    // Fetch all services
    this.props.fetchAllServices();
  }

  showDeleteServiceDialog = (service) => {
    this.setState({
      isDeleteServiceDialogOpened: true,
      selectedService: service
    });
  };

  showAddServiceDialog = () => {
    this.setState({
      isAddServiceDialogOpened: true
    });
  };

  showEditServiceDialog = (service) => {
    this.setState({
      isEditServiceDialogOpened: true,
      selectedService: service
    });
  };

  handleCloseDialogs = () => {
    this.setState({
      isDeleteServiceDialogOpened: false,
      isAddServiceDialogOpened: false,
      isEditServiceDialogOpened: false,
      isEditServiceOpened: false,
    });
  };

  toggleServiceDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  render() {
    console.log('All Services: ', this.props);

    return (
      <div>
        <ServiceSetting
          fetchServiceLanguage={this.props.fetchServiceLanguage}
          fetchServiceCategory={this.props.fetchServiceCategory}
          fetchCategoryTranslation={this.props.fetchCategoryTranslation}
          fetchTypeTranslation={this.props.fetchTypeTranslation}
          fetchServicesTypes={this.props.fetchServicesTypes}
          serviceCategories={this.props.serviceCategories}
          serviceTypes={this.props.serviceTypes}
          supportedServiceLanguages={this.props.supportedServiceLanguages}
        />

        <ServiceTable
          services={this.props.services}
          showDeleteServiceDialog={this.showDeleteServiceDialog}
          showAddServiceDialog={this.showAddServiceDialog}
          showEditServiceDialog={this.showEditServiceDialog}
        />

        <ConfirmDialog
          open={this.state.isDeleteServiceDialogOpened}
          onClose={this.handleCloseDialogs}
          title={'Delete Service'}
          content={'Do you want to delete this service: {{service}} ?'}
          confirmCallback={event => {
            if (event) {
              this.props.fetchService('DELETE', this.state.selectedService._id);
            }
          }}
          confirmDialogContent={'confirmDialogContent'}
          cancelDialogActionId={'cancelDeleteServiceDialog'}
          confirmDialogActionId={'confirmDeleteServiceDialog'}
        />

        <SwipeableDrawer
          anchor="right"
          classes={{ paper: this.props.classes.paperDrawer }}
          open={this.state.right}
          onClose={this.toggleServiceDrawer('right', false)}
          onOpen={this.toggleServiceDrawer('right', true)}
        >
          <div
            tabIndex={0}
            role='button'
            // onClick={this.toggleNewsDrawer('right', false)}
            // onKeyDown={this.toggleNewsDrawer('right', false)}
          >
            <EditServiceComponent
              selectedService={this.state.selectedService}
              servicesCategories={this.props.servicesCategories}
              serviceTypes={this.props.serviceTypes}
              fetchService={this.props.fetchService}
            />
          </div>
        </SwipeableDrawer>
      </div>
    );
  }
}

{/*<AddEventDialog
  open={this.state.isAddEventDialogOpened}
  onClose={this.handleCloseDialogs}
/>
<EditEventDialog
open={this.state.isEditEventDialogOpened}
onClose={this.handleCloseDialogs}
/>*/}

function mapStateToProps(state) {
  return {
    content: state.auth.content,
    service: getService(state),
    services: getAdminServices(state),
    serviceTypes: getServicesTypes(state),
    serviceCategories: getServicesCategories(state),
    types: fetchGetServiceType(state),
    supportedServiceLanguages: getServicesLanguages(state),
    fetchCategoryTranslation: fetchCategoryTranslation(state),
    fetchTypeTranslation: fetchTypeTranslation(state)
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...ServicesAction,
    fetchAllServices,
    fetchService,
    fetchServiceCategory,
    fetchServicesTypes,
    fetchGetServiceType,
    fetchServiceLanguage,
    fetchCategoryTranslation,
    fetchTypeTranslation
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, { withTheme: true })(ServiceDashboard));
