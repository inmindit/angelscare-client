import React, { Component } from 'react';
import Snackbar from 'material-ui/Snackbar';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import HeaderTemplate from '../components/template/Header';
import FooterTemplate from '../components/template/Footer';
import {
  actions as AppActions,
  getNotificationStatus
} from '../reducers/app';
import { getCurrentService, getServicesCategories } from '../reducers/user';
import { fetchServiceCategory, fetchServicesTypes} from '../actions/services';

type Props = {
  children: Object,
  notificationStatus: Object,
  hideNotifications: () => void,
}

class App extends Component<Props> {
  state = {
    open: false,
    vertical: null,
    horizontal: null,
    isMobileWidth: window.innerWidth,
  };

  componentWillMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    this.setState({ isMobileWidth: window.innerWidth });
  };

  /**
   * Calculate & Update state of new dimensions
   */
  updateDimensions() {
    // if(window.innerWidth < 500) {
    //   this.setState({ width: 450, height: 102 });
    // } else {
    //   let updateWidth  = window.innerWidth - 100;
    //   let updateHeight = Math.round(updateWidth / 4.4);
    //   this.setState({ width: updateWidth, height: updateHeight });
    // }
  }

  /**
   * Add event listener
   */
  componentDidMount() {
    this.props.fetchServiceCategory('GET');
    this.updateDimensions();
    window.addEventListener('resize', this.updateDimensions.bind(this));
    this.setState(({
      open: this.props.notificationStatus.visible
    }));
  }

  /**
   * Remove event listener
   */
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions.bind(this));
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  handleHideNotifications = () => {
    this.setState((prevState) => ({
      open: !prevState.open
    }));
  };

  render() {
    console.log('App:', this.props);
    const { isMobileWidth } = this.state;
    const isMobile = isMobileWidth <= 768;

    return (
      <div>
        <HeaderTemplate
          logo="AgeWeb.care"
          isMobile={isMobile}
          currentServices={this.props.currentServices}
          servicesCategories={this.props.servicesCategories}
        />

        <div>
          {this.props.children}
        </div>

        <FooterTemplate isMobile={isMobile} />

        <Snackbar
          anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
          open={this.state.open}
          onClose={this.handleHideNotifications}
          autoHideDuration={4000}
          SnackbarContentProps={{
            'aria-describedby': 'message-id'
          }}
          message={
            <span id="message-id">{this.props.notificationStatus.text}</span>
          }
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentServices: getCurrentService(state),
    notificationStatus: getNotificationStatus(state),
    servicesCategories: getServicesCategories(state)
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...AppActions, fetchServiceCategory }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps )(App);

