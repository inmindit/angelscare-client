import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { bindActionCreators } from 'redux';
// import { Link } from 'react-router';
import { actions as NewsAction, getNews, getNewsById } from '../reducers/news';
import { fetchNews, fetchNewsById } from '../actions/news';
import SocialIconComponent from '../components/SocialIconsComponent';
import BlogCardComponent from '../components/Cards/BlogCardComponent';
import i18n from '../services/i18n';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: '3%',
    marginBottom: "5%",
    marginLeft: "3%",
    justify: 'center',
    width: "91%"
  }),
  contentHeader: {
    margin: '-40px 15px 30px',
    padding: '15px',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    fontWeight: 300,
    lineHeight: '1.5em',
    borderRadius: '3px',
    background: 'linear-gradient(60deg, #ff9800, #8e24aa)',
    boxShadow: '0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2)',
    color: '#fff'
  },
  typographyTitle: {
    color: '#fff'
  },
  typographyOffset: {
    marginTop: 5,
    fontSize: 15,
    color: '#fff'
  },
  title: {
    color: '#fc3752',
    fontFamily: 'Avalon,sans-serif',
    fontWeight: 700,
    fontSize: '28px',
    letterSpacing: '-.03em',
    lineHeight: 1,
    padding: '15px'
  },
  description: {
    width: '90%',
    marginLeft: '5%',
    lineHeight: '1.8',
    marginBottom: '1rem',
    fontSize: '1.0rem',
    fontFamily: '"Montserrat", "Helvetica Neue", Arial, sans-serif'
  },
  category: {
    fontSize: '15px',
    padding: '15px'
  },
  currentDate: {
    marginLeft: '80%',
    marginTop: '-12%',
    fontWeight: 450,
    textTransform: 'uppercase',
    textDecoration: 'none',
    padding: '1%',
    background: '#fc3752',
    color: '#fefefe',
    width: '4em',
    height: '4em',
    borderRadius: '3em',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    opacity: '0.9',
    margin: '1em',
  }
});

type Props = {};

type State = {};

class NewsPreviewPage extends Component<Props, State> {
  state = {};

  componentDidMount() {
    this.props.fetchNewsById('GET', this.props.params.newsId);
    this.props.fetchNews();
  }

  handleNews = (news) => {
    this.props.router.replace('/news/' + news._id);
    this.props.router.go('/news/' + news._id);
    // window.location.replace('/news/' + news._id);
  };

  render() {
    const { classes, newsId } = this.props;
    console.log('News Preview Page - Props:', this.props);

    if (newsId) {
      const date = new Date(newsId.created).getDate();

      return (
        <div data-tid="NewsPreviewPage">
          <Paper className={classes.root} elevation={4}>
            <div className={classes.contentHeader}>
              <Typography variant="headline" component="h3" className={classes.typographyTitle}>
                {i18n.t('core:newsPreview')}
              </Typography>
              <Typography variant="headline" component="p" className={classes.typographyOffset}>
                {newsId.category ? newsId.category.name + ' > ' + newsId.type.name : 'Other'}
              </Typography>
            </div>

            <Grid container spacing={40}>
              <Grid item xs={12} sm={6}>
                <Typography variant="headline" component="h3" className={classes.title}>
                  {newsId.title}
                </Typography>
              {/* <Typography variant="headline" component="span" className={classes.category}>
                  {newsId.category ? newsId.category.name + ' > ' + newsId.type.name : 'Other'}
                </Typography>
                <Typography variant="headline" component="span" className={classes.currentDate}>
                  {date}
                </Typography>*/}

                {newsId.youtube.length > 0 ? (
                  <iframe
                    width="420"
                    height="315"
                    src={newsId.youtube}
                  >
                  </iframe>
                  ) : (
                  <div>
                    <img
                      className="imgPreview"
                      src={newsId.image ? newsId.image : '/src/public/assets/img/image_placeholder.jpg'}
                      height="300"
                    />
                  </div>
                  )
                }

                {newsId.description && (
                  <p className={classes.description}>
                    {newsId.description}
                  </p>
                )}

                <div dangerouslySetInnerHTML={{ __html: newsId.content }}/>
              </Grid>

              <Grid item xs={12} sm={6}>
                <Typography variant="headline" component="h3" className={classes.title}>
                  {i18n.t('core:latestNews')}
                </Typography>

                {this.props.news && this.props.news.map((news) => (
                  <Typography
                    component="div"
                    key={news._id}
                    onClick={() => this.handleNews(news)}
                  >
                    <BlogCardComponent
                      key={news._id}
                      news={news}
                    />
                  </Typography>
                ))}
              </Grid>
            </Grid>

            <SocialIconComponent entry={this.props.newsId} />
          </Paper>
        </div>
      );
    } else {
      return (
        <div data-tid="NewsPreviewPage">
          <Paper className={classes.root} elevation={4}>
            <div className={classes.contentHeader}>
              <Typography variant="headline" component="h3" className={classes.typographyTitle}>
                {i18n.t('core:newsPreview')}
              </Typography>
            </div>
          </Paper>
        </div>
      )
    }
  }
}

function mapStateToProps(state) {
  return {
    newsId: getNewsById(state),
    news: getNews(state),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...NewsAction, fetchNewsById, fetchNews }, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(translate(['core'])(NewsPreviewPage))));
