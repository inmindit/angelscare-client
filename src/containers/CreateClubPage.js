import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import FolderIcon from '@material-ui/icons/Folder';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Input, { InputAdornment } from 'material-ui/Input';
import MenuItem from 'material-ui/Menu/MenuItem';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { translate } from 'react-i18next';
import { fetchCreateClubs } from '../actions/clubs';
import { fetchServiceCategory } from '../actions/services';
import { getClubs } from '../reducers/clubs';
import { getServicesCategories } from '../reducers/user';
import RichTextEditorComponent from '../components/RichTextEditorComponent';
import i18n from '../services/i18n';
import EventsCalendarComponent from "../components/EventsCalendarComponent";

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: '3%',
    marginBottom: "5%",
    marginLeft: "3%",
    justify: 'center',
    width: "91%"
  }),
  textField: {
    marginBottom: 15,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '80%'
  },
  input: {
    display: 'none',
  },
  contentHeader: {
    margin: '-40px 15px 30px',
    padding: '15px',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    fontWeight: 300,
    lineHeight: '1.5em',
    borderRadius: '3px',
    background: 'linear-gradient(60deg, #ff9800, #8e24aa)',
    boxShadow: '0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2)',
    color: '#fff'
  },
  typographyTitle: {
    color: '#fff'
  },
  typographyOffset: {
    marginTop: 5,
    fontSize: 15,
    color: '#fff'
  },
  imgPreviewCS: {
    boxShadow: '0 10px 25px 0 rgba(0, 0, 0, 0.3)'
  },
  uploadImageField: {
    marginTop: '20%'
  },
  Paper: {
    marginBottom: 10
  }
});

type Props = {
  fetchServiceCategory: () => void
};

type State = {
  title: string,
  description: string,
};

class CreateClubPage extends Component<Props, State> {
  state = {
    title: '',
    youtube: '',
    description: '',
    image: {},
    imagePreviewURL: '',
    content: '',
    phone: '',
    contact: '',
    attendant: '',
    clubCategory: '',
    clubType: '',
    serviceTypes: '',
    selectedClubCategory: '',
    selectedClubType: '',
    events: [],
    errorText: false,
  };

  componentDidMount(nextProps, nextState) {
    this.props.fetchServiceCategory('GET');
  }

  handleCreateClub = (e) => {
    this.props.createClubs({
      category: this.state.selectedClubCategory._id,
      type: this.state.selectedClubType._id || '',
      title: this.state.title,
      description: this.state.description,
      content: this.state.content,
      youtube: this.state.youtube,
      phone: this.state.phone,
      contact: this.state.contact,
      attendant: this.state.attendant,
      events: this.state.events,
      location: this.state.location,
      image: this.state.image,
    });
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    }, this.handleValidation());
  };

  handleInputImageChange = (event) => {
    event.preventDefault();
    let reader = new FileReader();
    let file = event.target.files[0];

    reader.onloadend = () => {
      this.setState({
        image: file,
        imagePreviewURL: reader.result
      });
    };

    reader.readAsDataURL(file);
  };

  handleServiceCategoryChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.props.servicesCategories.map(category => {
      if (category.slug === value) {
        this.setState({
          [name]: value,
          selectedClubCategory: category,
          serviceTypes: category.types
        });
      }
    });
  };

  handleServiceTypeChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.state.serviceTypes.map(type => {
      if (type.slug === value) {
        this.setState({
          [name]: value,
          selectedClubType: type
        });
      }
    });
  };

  handleValidation() {
    // const pathRegex = '^((\.\./|[a-zA-Z0-9_/\-\\])*\.[a-zA-Z0-9]+)$';
    // const nameRegex = '^[A-Z][-a-zA-Z]+$';
    if (this.state.title.length > 0) {
      this.setState({ inputError: false, disableConfirmButton: false });
    } else {
      this.setState({ inputError: true, disableConfirmButton: true });
    }
  }

  setContent = (content) => {
    this.setState({ content });
  };

  setEvents = (events) => {
    this.setState({ events });
  };

  renderClub = (classes, services) => {

    return (
      <Grid container spacing={40} className={classes.mainSubHeaderContainer}>

        <Grid item xs={12} sm={6}>
          <Paper className={classes.Paper}>
            <FormControl fullWidth={true}>
              <TextField
                id="createClubCategory"
                select
                label={i18n.t('core:selectClubCategory')}
                className={classes.textField}
                value={this.state.clubCategory}
                name="clubCategory"
                data-tid="createClubCategory"
                onChange={this.handleServiceCategoryChange}
                SelectProps={{
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
                helperText={i18n.t('core:helperTextSelectServiceCategory')}
                margin="normal"
              >
                {services[0] && services.map(service => (
                  <MenuItem key={service.slug} value={service.slug}>
                    {service.name}
                  </MenuItem>
                ))}
              </TextField>
            </FormControl>

            {this.state.serviceTypes && (
              <FormControl fullWidth={true}>
                <TextField
                  id="createClubType"
                  select
                  label={i18n.t('core:selectClubType')}
                  className={classes.textField}
                  value={this.state.clubType}
                  name="clubType"
                  data-tid="createClubType"
                  onChange={this.handleServiceTypeChange}
                  SelectProps={{
                    MenuProps: {
                      className: classes.menu,
                    },
                  }}
                  helperText={i18n.t('core:helperTextSelectType')}
                  margin="normal"
                >
                  {this.state.serviceTypes && this.state.serviceTypes.map(type =>
                    <MenuItem key={type.slug} value={type.slug}>
                      {type.name}
                    </MenuItem>
                  )}
                </TextField>
              </FormControl>
            )}

            <FormControl fullWidth={true}>
              <TextField
                className={classes.textField}
                helperText={i18n.t('core:youtubeEmbedded')}
                label={i18n.t('core:youtubeEmbedded')}
                name="youtube"
                onChange={this.handleInputChange}
                fullWidth
              />
            </FormControl>

            <FormControl fullWidth={true}>
              <TextField
                className={classes.textField}
                helperText={i18n.t('core:clubCreateTitle')}
                label={i18n.t('core:clubTitle')}
                name="title"
                onChange={this.handleInputChange}
                fullWidth
              />
            </FormControl>

            <FormControl fullWidth={true}>
              <TextField
                className={classes.textField}
                helperText={i18n.t('core:clubCreateAttendant')}
                label={i18n.t('core:clubCreateAttendant')}
                name="attendant"
                type="text"
                onChange={this.handleInputChange}
                fullWidth
              />
            </FormControl>

            <FormControl fullWidth={true}>
              <TextField
                className={classes.textField}
                helperText={i18n.t('core:clubCreateContactPhone')}
                label={i18n.t('core:clubCreateContactPhone')}
                name="phone"
                type="tel"
                onChange={this.handleInputChange}
                fullWidth
              />
            </FormControl>

            <FormControl fullWidth={true}>
              <TextField
                className={classes.textField}
                helperText={i18n.t('core:clubCreateContactEmail')}
                label={i18n.t('core:clubCreateContactEmail')}
                name="contact"
                type="email"
                onChange={this.handleInputChange}
                fullWidth
              />
            </FormControl>

            <FormControl fullWidth={true}>
              <TextField
                id="createClubDescription"
                placeholder={i18n.t('core:clubCreateDescription')}
                rows="4"
                rowsMax="6"
                multiline
                name="description"
                className={classes.textField}
                value={this.state.description}
                onChange={e => this.handleInputChange(e)}
                margin="normal"
                fullWidth={true}
              />
            </FormControl>
          </Paper>

          <Grid item xs={12} sm={12}>
            <EventsCalendarComponent
              setEvents={this.setEvents}
            />
          </Grid>

        </Grid>

        <Grid item xs={12} sm={6}>

          <div className={classes.textField}>
            <img
              className={classes.imgPreviewCS}
              src={this.state.imagePreviewURL ? this.state.imagePreviewURL : '/src/public/assets/img/image_placeholder.jpg'} height="150"
            />
          </div>

          <FormControl
            className={classes.uploadImageField}
            encType="multipart/form-data"
            fullWidth={true}
            error={this.state.errorText}
          >
            <Input
              required
              margin="dense"
              disabled={true}
              placeholder={i18n.t('core:choosePhoto')}
              fullWidth={true}
              data-tid="createClubUploadImage"
              value={this.state.image.name}
              className={classes.textField}
              endAdornment={
                <InputAdornment position="end" style={{ height: 32 }}>
                  <IconButton>
                    <label htmlFor="createClubUploadImage">
                      <FolderIcon/>
                    </label>
                  </IconButton>
                </InputAdornment>
              }
            />
            {this.state.errorText && <FormHelperText>{i18n.t('core:InvalidUrlAddressMessage')}</FormHelperText>}
          </FormControl>

          <FormControl fullWidth={true}>
            <Paper className={classes.Paper}>
              <RichTextEditorComponent
                setContent={this.setContent}
              />
            </Paper>
          </FormControl>


        </Grid>
      </Grid>
    );
  };

  render() {
    const { classes } = this.props;
    console.log('Create Club Page - Props:', this.props);
    console.log('Create Club Page - State:', this.state);

    return (
      <div data-tid="createClubPage">
        <Paper className={classes.root} elevation={4}>
          <div className={classes.contentHeader}>
            <Typography variant="headline" component="h3" className={classes.typographyTitle}>
              {i18n.t('core:createClub')}
            </Typography>
            <Typography variant="headline" component="p" className={classes.typographyOffset}>
              {i18n.t('core:createClubDescription')}
            </Typography>
          </div>

          <div>
            {this.renderClub(classes, this.props.servicesCategories)}

            <Button
              variant="raised"
              style={{ marginTop: '15px' }}
              color="primary"
              onClick={(event) => this.handleCreateClub(event)}
            >
              {i18n.t('core:create')}
            </Button>
          </div>

          <form encType="multipart/form-data" className={classes.input}>
            <input
              accept="image/*"
              onChange={this.handleInputImageChange}
              id="createClubUploadImage"
              name="club"
              type="file"
            />
          </form>
          {/*{this.renderAlert()}*/}
        </Paper>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    club: getClubs(state),
    servicesCategories: getServicesCategories(state)
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ createClubs: fetchCreateClubs, fetchServiceCategory }, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(translate(['core'], { wait: true })(CreateClubPage))));
