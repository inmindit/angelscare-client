import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchUser } from '../../../actions/index';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Switch from 'material-ui/Switch';
import { FormControl, FormControlLabel, FormHelperText } from 'material-ui/Form';
import TextField from 'material-ui/TextField';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import ExpansionPanel from 'material-ui/ExpansionPanel/ExpansionPanel';
import ExpansionPanelSummary from 'material-ui/ExpansionPanel/ExpansionPanelSummary';
import ExpansionPanelDetails from 'material-ui/ExpansionPanel/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import LocationAutosuggestion from '../../LocationAutosuggestion';
import { countrySuggestions } from '../../Config';
import i18n from '../../../services/i18n';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: '1%', // theme.spacing.unit * 3,
    marginBottom: '3%', // theme.spacing.unit * 3,
    width: "96.5%"
  }),
  pageTitle: {
    margin: 15,
    paddingTop: 15
  },
  buttonAlign: {
    marginTop: 15,
    width: '100%'
  },
  updateButton: {
    marginTop: 15
  },
  textField: {
    marginBottom: 15,
    marginLeft: 20,
    width: '80%'
  },
  contentHeader: {
    margin: '-40px 15px 30px',
    padding: '15px',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    fontWeight: 300,
    lineHeight: '1.5em',
    borderRadius: '3px',
    background: 'linear-gradient(60deg, #ff9800, #8e24aa)',
    boxShadow: '0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2)',
    color: '#fff'
  },
  typographyTitle: {
    color: '#fff'
  },
  typographyOffset: {
    marginTop: 5,
    fontSize: 15,
    color: '#fff'
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  profileSettingPanel: {
    marginLeft: 18,
    marginRight: 18
  },
  autoSuggestField: {
    marginLeft: '3%',
    width: '80%'
  }
});

type Props = { };

type State = {
  userImage: string,
  firstName: string,
  lastName: string,
  email: string,
  username: string,
  password: string,
  phone: string,
  city: string,
  zipCode: number,
  location: string,
  created: string,
  errorFirstName: boolean,
  errorLastName: boolean,
  errorUsername: boolean,
  errorPassword: boolean,
  errorConfirmPassword: boolean,
  errorEmailAddress: boolean,
};

class UserProfile extends Component<Props, State> {
  constructor(props) {
    super(props);
  }

  state = {
    userImage: '',
    firstName: '',
    lastName: '',
    emailAddress: '',
    username: '',
    password: '',
    phone: '',
    city: '',
    zipCode: '',
    country: '',
    location: '',
    address: '',
    created: '',
    userRoles: [],
    countrySuggestions: [],
    confirmPassword: '',
    errorFirstName: false,
    errorLastName: false,
    errorUsername: false,
    errorPassword: false,
    errorConfirmPassword: false,
    errorEmailAddress: false,
    errorPhoneNumber: false,
    DailyReports: false,
    isWeeklyReports: false,
    isReceiveEmailMessage: false,
    isNewDevice: false,
    expanded: null,
  };

  componentDidMount (nextProps, nextState) {
    // Fetch user data prior to component mounting
    this.props.fetchUser('GET');
  }

  componentWillReceiveProps  (nextProps, nextState) {
    this.setState({
      uuid: nextProps.profile.id,
      userImage: '',
      firstName: nextProps.profile.firstName,
      lastName: nextProps.profile.lastName,
      emailAddress: nextProps.profile.email,
      username: nextProps.profile.username,
      password: nextProps.profile.password || '',
      phone: nextProps.profile.phone || '',
      city: nextProps.profile.fullLocation && nextProps.profile.fullLocation.city || '',
      location: nextProps.profile.fullLocation &&nextProps.profile.fullLocation.country || '',
      address: nextProps.profile.fullLocation && nextProps.profile.fullLocation.address || '',
      zipCode: nextProps.profile.fullLocation && nextProps.profile.fullLocation.zip || '',
      created: nextProps.profile.created,
      userRoles: nextProps.profile.roles,
    })
  }

  handleChangeExpandedPanel = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    }, this.handleValidation());
  };

  handleValidation() {
    // const pathRegex = '^((\.\./|[a-zA-Z0-9_/\-\\])*\.[a-zA-Z0-9]+)$';
    // const nameRegex = '^[A-Z][-a-zA-Z]+$';
    // if (this.state.username.length > 0) {
    //   this.setState({ errorUsername: false, disableConfirmButton: false });
    // } else {
    //   this.setState({ errorUsername: true, disableConfirmButton: true });
    // }
    //
    // if (this.state.password.length > 6) {
    //   this.setState({ errorPassword: false, disableConfirmButton: false });
    // } else {
    //   this.setState({ errorPassword: true, disableConfirmButton: true });
    // }
  }

  onConfirm = () => {
    this.props.fetchUser('PUT', {
      id: this.state.uuid,
      // userImage: this.state.userImage,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      // emailAddress: this.state.emailAddress,
      // username: this.state.username,
      password: this.state.password,
      confirmPassword: this.state.confirmPassword,
      fullLocation: {
        city: this.state.city,
        address: this.state.address,
        country: this.state.location,
        zip: this.state.zipCode,
      },
      phone: this.state.phone,
    });
  };

  setLocation = (location) => {
    this.setState({ location });
  };

  render() {
    const { classes } = this.props;
    const { expanded } = this.state;
    console.log('Profile:', this.props);

    return (
      <div className={classes.root}>
        <Paper elevation={4} className={classes.root}>

          <div className={classes.contentHeader}>
            <Typography variant="headline" component="h3" className={classes.typographyTitle}>
              Profile: Your personal info & privacy
            </Typography>
            <Typography variant="headline" component="p" className={classes.typographyOffset}>
              Manage this basic information — your name, email, phone number, and make it easier to get in touch.
            </Typography>
          </div>

          <div className={classes.profileSettingPanel}>

            <ExpansionPanel expanded={expanded === 'panel1'} onChange={this.handleChangeExpandedPanel('panel1')}>

              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <Typography className={classes.heading} variant="headline" component="h3">General Information</Typography>
                <Typography className={classes.secondaryHeading}>My personal information</Typography>
              </ExpansionPanelSummary>

              <ExpansionPanelDetails>
                <Grid container spacing={24}>
                  <Grid item xs={12} sm={6}>

                    <FormControl fullWidth={true} error={this.state.errorFirstName}>
                      <TextField
                        className={classes.textField}
                        helperText="Enter your First name"
                        label="First Name"
                        value={this.state.firstName}
                        name="firstName"
                        onChange = {this.handleInputChange}
                        fullWidth
                      />
                      {this.state.errorFirstName && <FormHelperText>Invalid first name.</FormHelperText>}
                    </FormControl>

                    <FormControl fullWidth={true} error={this.state.errorLastName}>
                      <TextField
                        className={classes.textField}
                        helperText="Enter your Last Name"
                        label="Last Name"
                        value={this.state.lastName}
                        name="lastName"
                        onChange = {this.handleInputChange}
                        fullWidth
                      />
                      {this.state.errorLastName && <FormHelperText>Invalid last name.</FormHelperText>}
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} sm={6}>
                    <FormControl fullWidth={true} error={this.state.errorUsername}>
                      <TextField
                        disabled={true}
                        className={classes.textField}
                        helperText="Enter your username"
                        label="Username"
                        value={this.state.username}
                        name="username"
                        onChange = {this.handleInputChange}
                        fullWidth={true}
                      />
                    </FormControl>

                    <FormControl fullWidth={true} error={this.state.errorUsername}>
                      <TextField
                        disabled={true}
                        className={classes.textField}
                        label="Role"
                        helperText="Role"
                        name="userRoles"
                        onChange = {this.handleInputChange}
                        value={this.state.userRoles}
                        fullWidth={true}
                      />
                    </FormControl>

                  </Grid>
                </Grid>
              </ExpansionPanelDetails>
            </ExpansionPanel>

            <ExpansionPanel expanded={expanded === 'panel2'} onChange={this.handleChangeExpandedPanel('panel2')}>

              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <Typography className={classes.heading} variant="headline" component="h3">Contact Information</Typography>
                <Typography className={classes.secondaryHeading}>Organize your contacts information and make it easier to connect with you.</Typography>
              </ExpansionPanelSummary>

              <ExpansionPanelDetails>
                <Grid container spacing={24}>
                  <Grid item xs={12} sm={6}>

                    <FormControl fullWidth={true} error={this.state.errorEmailAddress}>
                      <TextField
                        disabled={true}
                        className={classes.textField}
                        helperText="Enter your Email"
                        label="Email Address"
                        value={this.state.emailAddress}
                        name="emailAddress"
                        onChange = {this.handleInputChange}
                        fullWidth
                      />
                      {this.state.errorEmailAddress && <FormHelperText>Invalid email address.</FormHelperText>}
                    </FormControl>

                    <FormControl fullWidth={true} error={this.state.errorPhoneNumber}>
                      <TextField
                        className={classes.textField}
                        helperText="Enter your phone number"
                        label="Phone Number"
                        name="phone"
                        value={this.state.phone}
                        type="number"
                        onChange = {this.handleInputChange}
                        fullWidth
                      />
                      {this.state.errorPhoneNumber && <FormHelperText>Invalid phone number.</FormHelperText>}
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} sm={6}>
                    <FormControl fullWidth={true} className={classes.autoSuggestField}>
                      <LocationAutosuggestion
                        setLocation={this.setLocation}
                        location={this.state.location}
                        placeholder={'Location'}
                        suggestions={countrySuggestions}
                      />
                    </FormControl>

                    <FormControl fullWidth={true}>
                      <TextField
                        className={classes.textField}
                        helperText="Enter your city"
                        label="City"
                        value={this.state.city}
                        name="city"
                        onChange = {this.handleInputChange}
                        fullWidth
                      />
                    </FormControl>

                    <FormControl fullWidth={true}>
                      <TextField
                        className={classes.textField}
                        helperText="Enter your address"
                        label="Address"
                        value={this.state.address}
                        name="address"
                        onChange = {this.handleInputChange}
                        fullWidth
                      />
                    </FormControl>

                    <FormControl fullWidth={true}>
                      <TextField
                        className={classes.textField}
                        helperText="Enter your ZIP code"
                        label="ZIP  Code"
                        value={this.state.zipCode}
                        name="zipCode"
                        onChange = {this.handleInputChange}
                        fullWidth
                      />
                    </FormControl>
                  </Grid>
                </Grid>
              </ExpansionPanelDetails>
            </ExpansionPanel>

            <ExpansionPanel expanded={expanded === 'panel3'} onChange={this.handleChangeExpandedPanel('panel3')}>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <Typography className={classes.heading} variant="headline" component="h3">Change Password: Your password protects your account.</Typography>
                <Typography className={classes.secondaryHeading}>Note: To change these settings, you will need to confirm your password.</Typography>
              </ExpansionPanelSummary>

              <ExpansionPanelDetails>
                <Grid container spacing={24}>
                  <Grid item xs={12} sm={6}>
                    <FormControl fullWidth={true} error={this.state.errorPassword}>
                      <TextField
                        className={classes.textField}
                        helperText="Enter new password"
                        label="Password"
                        value={this.state.password}
                        name="password"
                        type="password"
                        onChange = {this.handleInputChange}
                        fullWidth={true}
                      />
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} sm={6}>
                    <FormControl fullWidth={true} error={this.state.errorConfirmPassword}>
                      <TextField
                        className={classes.textField}
                        helperText="Enter the password again"
                        label="Confirm Password"
                        value={this.state.passwordConfirm}
                        name="passwordConfirm"
                        onChange = {this.handleInputChange}
                        type="password"
                        fullWidth={true}
                      />
                    </FormControl>
                  </Grid>
                </Grid>
              </ExpansionPanelDetails>
            </ExpansionPanel>

            <ExpansionPanel expanded={expanded === 'panel4'} onChange={this.handleChangeExpandedPanel('panel4')}>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <Typography className={classes.heading}>{i18n.t('core:profileSendEmailNotificationsHeader')}</Typography>
                <Typography className={classes.secondaryHeading}>
                  {i18n.t('core:profileSendEmailNotificationsDescription')}
                </Typography>
              </ExpansionPanelSummary>

              <ExpansionPanelDetails>
                <Grid container spacing={24}>
                  <Grid item xs={12} sm={12}>

                    <FormControl fullWidth={true}>
                      <FormControlLabel
                        control={
                          <Switch
                            data-tid="enableReceiveEmailMessage"
                            name="isReceiveEmailMessage"
                            checked={this.state.isReceiveEmailMessage}
                            onChange={this.handleInputChange}
                          />
                        }
                        // label={i18n.t('core:startupLocation')}
                        label="Receive a email message"
                      />
                    </FormControl>

                    <FormControl fullWidth={true}>
                      <FormControlLabel
                        control={
                          <Switch
                            data-tid="enableNewDevice"
                            name="isNewDevice"
                            checked={this.state.isNewDevice}
                            onChange={this.handleInputChange}
                          />
                        }
                        label="When anyone logs into your account from new device or browser"
                      />
                    </FormControl>

                    <FormControl fullWidth={true}>
                      <FormControlLabel
                        control={
                          <Switch
                            data-tid="enableWeeklyReports"
                            name="isWeeklyReports"
                            checked={this.state.isWeeklyReports}
                            onChange={this.handleInputChange}
                          />
                        }
                        label="Weekly reports"
                      />
                    </FormControl>

                    <FormControl fullWidth={true}>
                      <FormControlLabel
                        control={
                          <Switch
                            data-tid="enableDailyReports"
                            name="DailyReports"
                            checked={this.state.DailyReports}
                            onChange={this.handleInputChange}
                          />
                        }
                        label="Daily reports"
                      />
                    </FormControl>

                  </Grid>
                </Grid>
              </ExpansionPanelDetails>
            </ExpansionPanel>

            <Button
              className={classes.updateButton}
              id="profileConfirm"
              variant="raised"
              color="primary"
              onClick={() => {this.onConfirm()}}
            >
              {i18n.t('core:profileUpdateProfile')}
            </Button>
          </div>
        </Paper>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    profile: state.user.profile,
  };
}

export default connect(mapStateToProps, { fetchUser }) (withStyles(styles)(UserProfile));
