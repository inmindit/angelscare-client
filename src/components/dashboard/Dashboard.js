import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { Link } from 'react-router';
// import cookie from 'react-cookie';

class Dashboard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    console.log('Dashboard: ', this.props);
    return (
      <div>
        <div>{this.props.content}</div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { content: state.auth.content };
}

export default connect(mapStateToProps)(Dashboard);
