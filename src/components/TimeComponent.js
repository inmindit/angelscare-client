import React from 'react';
import TimePicker from 'react-times';
import 'react-times/css/material/default.css';
import 'react-times/css/classic/default.css';

class TimeComponent extends React.Component {

  state = {
    currentTime: ''
  };

  onHourChange(hour) {

  }

  onMinuteChange(minute) {

  }

  onTimeChange(time) {
    this.setState({
      currentTime: time
    });
    this.props.setCurrentTime(time)
  }

  onFocusChange(focusStatue) {

  }

  render() {
    return (
      <TimePicker
        colorPalette="dark" // main color, default "light"
        theme="classic"
        time={this.state.currentTime}
        onFocusChange={this.onFocusChange.bind(this)}
        onHourChange={this.onHourChange.bind(this)}
        onMinuteChange={this.onMinuteChange.bind(this)}
        onTimeChange={this.onTimeChange.bind(this)}
      />
    )
  }
}

export default TimeComponent;
