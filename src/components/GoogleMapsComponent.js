import React, { Component } from 'react';
import { compose, withProps } from 'recompose';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from 'react-google-maps';

const GoogleMaps = compose(
  withProps({
    /**
     * Note: create and replace your own key in the Google console.
     * https://console.developers.google.com/apis/dashboard
     * The key "AIzaSyBkNaAGLEVq0YLQMi-PYEMabFeREadYe1Q" can be ONLY used in this sandbox (no forked).
     */
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyD7dS3buTHIxvseyVDmAHkFAEIGzctj0Q4&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />
  }),
  withScriptjs,
  withGoogleMap
)(props => (
  <GoogleMap defaultZoom={8} defaultCenter={{ lat: props.latitude, lng: props.longitude }}>
    <Marker position={{ lat: props.latitude, lng: props.longitude }} />
  </GoogleMap>
));

class GoogleMapsComponent extends Component {
  state = {
    isMarkerShown: false,
  };

  // componentDidMount() {
  //   this.delayedShowMarker()
  // }
  //
  // delayedShowMarker = () => {
  //   setTimeout(() => {
  //     this.setState({ isMarkerShown: true })
  //   }, 3000)
  // };
  //
  // handleMarkerClick = () => {
  //   this.setState({ isMarkerShown: false })
  //   this.delayedShowMarker()
  // };

  render() {
    return (
      <GoogleMaps
        latitude={this.props.latitude}
        longitude={this.props.longitude}
        // isMarkerShown={this.state.isMarkerShown}
        // onMarkerClick={this.handleMarkerClick}
      />
    )
  }
}

export default GoogleMapsComponent;
