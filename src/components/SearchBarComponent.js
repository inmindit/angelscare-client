import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
// import {FormControl} from 'material-ui/Form';
// import {MenuItem} from 'material-ui/Menu';
// import {Button, TextField} from 'material-ui';
// import SearchIcon from '@material-ui/icons/Search';
// import i18n from '../services/i18n';

const styles = theme => ({
  textField: {
    marginBottom: 15,
    marginLeft: 20,
    width: '80%'
  },
  suggestionsContainerOpen: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
  },
  suggestion: {
    display: 'block',
  },
  suggestionsList: {
    margin: 0,
    padding: 0,
    listStyleType: 'none',
  },
  autoSuggestField: {
    marginLeft: '3%',
    width: '80%'
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
});


type Props = {
  searchCategories: Array,
};

type State = {
  searchCategories: Array,
  searchTypes: Array,
  selectedCategory: Object,
  selectedType: Object,
  searchTag: string
};

class SearchBarComponent extends Component<Props, State> {
  /*constructor(props) {
    super(props);
  }

  state = {
    searchCategories: [],
    searchTypes: [],
    selectedCategory: {},
    selectedType: {},
    searchTag: ''
  };

  componentDidMount(nextProps, nextState) {

  }

  handleCategoryChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.props.searchCategories.map(category => {
      if (category.slug === value) {
        this.setState({
          [name]: value,
          selectedCategory: category,
          searchTypes: category.types
        });
      }
    });
  };

  handleTypeChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.state.searchTypes.map(type => {
      if (type.slug === value) {
        this.setState({
          [name]: value,
          selectedType: type
        });
      }
    });
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  render() {
    const {classes} = this.props;
    return (
      <div>
        {this.props.searchCategories.length > 0 &&
        <FormControl fullWidth={true}>
          <TextField
            id="searchCategory"
            select
            label={i18n.t('core:selectServiceCategory')}
            className={classes.textField}
            value={this.props.searchCategories.length > 1 ? this.props.searchCategories : this.props.searchCategories[0].name || ''}
            name="selectedCategory"
            data-tid="selectedCategory"
            onChange={this.handleCategoryChange}
            SelectProps={{
              MenuProps: {
                className: classes.menu,
              },
            }}
            helperText={i18n.t('core:helperTextSelectServiceCategory')}
            margin="normal"
          >
            {this.props.searchCategories[0] && this.props.searchCategories.map(cat => (
              <MenuItem key={cat.slug} value={cat.slug}>
                {cat.name}
              </MenuItem>
            ))}
          </TextField>
        </FormControl>
        }

        {this.state.searchTypes.length > 0 && (
          <FormControl fullWidth={true}>
            <TextField
              id="searchType"
              select
              label={i18n.t('core:selectServiceCategory')}
              className={classes.textField}
              value={this.state.searchTypes.length > 1 ? this.state.searchTypes : this.props.searchTypes[0].name}
              name="selectedType"
              data-tid="selectedType"
              onChange={this.handleTypeChange}
              SelectProps={{
                MenuProps: {
                  className: classes.menu,
                },
              }}
              helperText={i18n.t('core:helperTextSelectServiceType')}
              margin="normal"
            >
              {this.state.searchTypes && this.state.searchTypes.map(type =>
                <MenuItem key={type.slug} value={type.slug}>
                  {type.name}
                </MenuItem>
              )}
            </TextField>
          </FormControl>
        )}
        {<FormControl fullWidth={true}>
          <TextField
            className={classes.textField}
            helperText={i18n.t('core:searchServiceTagHelper')}
            label={i18n.t('core:searchServiceTagLabel')}
            name="searchTag"
            onChange={this.handleInputChange}
            fullWidth
          />
        </FormControl>}
        {
          <FormControl fullWidth={true}>
            <Button fullWidth={true} variant="raised" color="primary">
              <SearchIcon className={classes.rightIcon}>send</SearchIcon>
            </Button>
          </FormControl>
        }
      </div>
    );
  }*/
}

export default withStyles(styles)(SearchBarComponent);
