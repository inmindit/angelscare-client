import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from 'material-ui/styles';
import BottomNavigation, { BottomNavigationAction } from 'material-ui/BottomNavigation';
import RestoreIcon from '@material-ui/icons/Restore';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import Grid from 'material-ui/Grid';
import i18n from '../../services/i18n';
import {translate} from "react-i18next";
// import { Link } from 'react-router';

const styles = {
  root: {
    flexGrow: 1,
    width: "100%",
  },
  footer: {
    width: "100%",
    position: 'fixed',
    left: '0px',
    bottom:'0px'
  },
  mainPageFooter: {
    fontStyle: 'italic',
    color: '#fc375b',
    textAlign: 'center'
  },
  footerBs: {
    backgroundColor: '#3c3d41',
    padding: '60px 40px',
    color: 'rgba(255,255,255,1.00)',
    marginBottom: '20px',
    borderBottomRightRadius: '6px',
    borderTopLeftRadius: '0px',
    borderBottomLeftRadius: '6px'
  },
  designedBy: {
    textAlign: 'center',
    margin: '10px auto',
    color: '#3c3d41',
  },
  designedByText: {
    color: '#ffba4e',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    textDecoration: 'none'
  }
};

type Props = {
  isAuthenticated: boolean,
};

type State = {
  classes: Object,
  value: number
};

class Footer extends Component<Props, State> {
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;
    const { isAuthenticated } = this.props;
    console.log('Main Page(Footer): ', this.props);

    if (isAuthenticated) {
      return (
        <div>
          <BottomNavigation
            value={value}
            onChange={this.handleChange}
            showLabels
            className={classes.footer}
          >
            <BottomNavigationAction label={i18n.t('core:recents')} icon={<RestoreIcon />} />
            <BottomNavigationAction label={i18n.t('core:favorites')} icon={<FavoriteIcon />} />
            <BottomNavigationAction label={i18n.t('core:nearby')} icon={<LocationOnIcon />} />
          </BottomNavigation>
        </div>
      );
    } else {
      return (
        <div className={classes.root}>
          <div className={classes.mainPageFooter}>
            <h2><q>{i18n.t('core:ageWebFooterMotto')}</q></h2>
          </div>
          <footer className="footer-bs">
            <div>
              <Grid container spacing={40}>
                <Grid item xs={12} sm={3} className="footer-brand animated fadeInLeft">
                  <h2>
                    <img src="/src/public/assets/img/logo_400.png" height="50" />
                  </h2>
                  <p>
                    <address>

                    </address>
                  </p>
                  <p>© 2018 AgeWeb, All rights reserved</p>
                </Grid>
                <Grid item xs={12} sm={3} className="footer-nav animated fadeInUp">
                  <h4>Menu —</h4>
                  <ul className="pages">
                    <li>
                      <a
                        href="#"
                        onClick={() => this.refs.activeAging.scrollIntoView({ behavior: "smooth" })}
                      >{i18n.t('core:activeAging')}</a>
                    </li>
                    <li>
                      <a href="#">{i18n.t('core:dailyLiving')}</a>
                    </li>
                    <li>
                      <a
                        href="#"
                        onClick={() => this.refs.careGiving.scrollIntoView({ behavior: "smooth" })}
                      >{i18n.t('core:careGiving')}</a>
                    </li>
                  </ul>
                </Grid>
                <Grid item xs={12} sm={2} className="footer-nav animated fadeInUp">
                  <ul className="list">
                    <li><a href="http://ageweb.care/about">{i18n.t('core:aboutUs')}</a></li>
                    <li><a href="http://ageweb.care/contacts">{i18n.t('core:contactUs')}</a></li>
                    <li><a href="http://ageweb.care/terms">{i18n.t('core:termsOfService')}</a></li>
                    <li><a href="http://ageweb.care/privacy">{i18n.t('core:privacyPolicy')}</a></li>
                    <li><a href="http://ageweb.care/cookie">{i18n.t('core:cookiePolicy')}</a></li>
                  </ul>
                </Grid>
                <Grid item xs={12} sm={2} className="footer-social animated fadeInDown">
                  <h4>Follow Us</h4>
                  <ul className="list">
                    <li><a href="#">Facebook</a></li>
                    <li><a href="#">Twitter</a></li>
                  </ul>
                </Grid>
                <Grid item xs={12} sm={2} className="footer-ns animated fadeInRight">
                  <h4>{i18n.t('core:newsletter')}</h4>
                  {/*<p>A rover wearing a fuzzy suit doesn’t alarm the real penguins</p>*/}
                </Grid>
              </Grid>
            </div>
          </footer>
          <section className={classes.designedBy}>
            {/*<p>Designed by <a className={classes.designedByText} href="https://jordas.net/">JD Software Solution</a></p>*/}
          </section>
        </div>
      );
    }
  }
}

function mapStateToProps(state) {
  return {
    isAuthenticated: state.auth.isAuthenticated,
  };
}

export default connect(mapStateToProps, null)(withStyles(styles)(translate(['core'])(Footer)));

