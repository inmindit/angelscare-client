import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import SwipeableDrawer from 'material-ui/SwipeableDrawer';
import List from 'material-ui/List';
import Divider from 'material-ui/Divider';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import TranslateIcon from '@material-ui/icons/Translate';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Menu, { MenuItem } from 'material-ui/Menu';
import { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
// import ListSubheader from '@material-ui/core/ListSubheader';
import Collapse from '@material-ui/core/Collapse';
import {
  AccountBox as AccountBoxIcon,
  ShoppingCart as CartIcon,
  // FeaturedPlayList as OrdersIcon,
  Event as EventIcon,
  EventNote as MyEventsIcon,
  BusinessCenter as ServicesIcon,
  PlaylistAdd as AddEventIcon,
  People as PeopleIcon,
  ArrowDropDown as ArrowDropDown,
  ArrowDropUp as ArrowDropUp,
  // PersonAdd as InviteIcon,
  // InboxIcon  as InboxIcon ,
  StarBorder as StarBorder,
  // ExpandLess as ExpandLess,
  // ExpandMore as ExpandMore,
  Public as GlobeIcon
} from '@material-ui/icons/';
import cookie from 'react-cookie';
import { translate } from 'react-i18next';
import { bindActionCreators } from 'redux';
import {
  action as AppSettingsActions,
  getAppSettings
} from '../../reducers/app';
// import { getProfile } from '../../reducers/user';
import i18n from '../../services/i18n';

const styles = {
  root: {
    flexGrow: 1,
  },
  flex: {
    flex: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  appBar: {
    background: '#3c3d41',
    color: '#fff'
  },
  list: {
    color: '#fff',
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
  sideBar: {
    backgroundColor: 'rgba(75, 86, 160, 0.7)',
  },
  nested: {
    paddingLeft: 15,
  },
  countryList: {
    maxHeight: 320,
  }
};

type Props = {
  isAuthenticated: boolean,
};

type State = {};

class Header extends Component<Props, State> {
  constructor(context) {
    super(context);
    this.user = cookie.load('user');
    this.state = {
      isAuthenticated: this.props.isAuthenticated,
      user: this.props.user,
      anchorEl: null,
      anchorLanguageEl: null,
      anchorCountryEl: null,
      userRoles: [],
      left: false,
      top: false,
      collapseAdminMenu: true
    };
  }

  componentWillMount() {
    if (this.user && this.user.settings.country) {
      this.props.setCountry(this.user.settings.country);
    }
  }

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleSearchMenu = event => {

  };

  handleLanguageMenu = event => {
    this.setState({ anchorLanguageEl: event.currentTarget });
  };

  handleCountryMenu = event => {
    this.setState({ anchorCountryEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({
      anchorEl: null,
      anchorLanguageEl: null,
      anchorCountryEl: null
    });
  };

  handleClick = (link, handle = false) => {
    this.props.router.push(link);
    if (handle) {
      this.handleClose();
    }
  };

  handleServiceClick = (link, categoryId, typeId, handle = false) => {
    if (typeId) {
      this.props.router.push(link + categoryId + '/' + typeId);
    } else {
      this.props.router.push(link + categoryId);
    }

    if (handle) {
      this.handleClose();
    }
  };

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };

  handleCollapseAdminMenu = (e) => {
    e.stopPropagation();
    this.setState({ collapseAdminMenu: !this.state.collapseAdminMenu });
  };

  userListItems = (classes, isAuth) => (
    <div>
      <ListItem button onClick={() => {this.handleClick('/')}}>
        <ListItemIcon>
          <IconButton
            color='inherit'
            aria-label='Menu'
            onClick={this.toggleDrawer('left', true)}
          >
            <MenuIcon/>
          </IconButton>
        </ListItemIcon>

        <Typography
          variant='title'
          color='inherit'
          className={classes.flex}
          onClick={() => {this.handleClick('/')}}
        >
          <img
            src="/src/public/assets/img/logo_400.png"
            height="50"
            style={{ marginBottom: '-10px', marginTop: '-5px' }}
          />
        </Typography>
      </ListItem>

      <Divider />
      <List
        component="nav"
        // subheader={<ListSubheader component="div">{i18n.t('core:home')}</ListSubheader>}
      >
        {!isAuth && (
          <ListItem button onClick={() => {this.handleClick('/login')}}>
            <ListItemIcon>
              <AccountBoxIcon />
            </ListItemIcon>
            <ListItemText primary={i18n.t('core:signIn')} />
          </ListItem>
        )}

        {!isAuth && (
          <ListItem button onClick={() => {this.handleClick('/register')}}>
            <ListItemIcon>
              <AccountBoxIcon />
            </ListItemIcon>
            <ListItemText primary={i18n.t('core:signUp')} />
          </ListItem>
        )}

        <ListItem button onClick={() => {this.handleClick('/')}}>
          <ListItemIcon>
            <AccountBoxIcon />
          </ListItemIcon>
          <ListItemText primary={i18n.t('core:homePage')} />
        </ListItem>

        {isAuth && (
          <ListItem button onClick={() => {this.handleClick('/dashboard')}}>
            <ListItemIcon>
              <CartIcon />
            </ListItemIcon>
            <ListItemText primary={i18n.t('core:dashboard') }/>
          </ListItem>
        )}
      </List>
      <Divider />

      {this.props.servicesCategories && this.props.servicesCategories.map((category) => {
        // const collapseService = 'collapseMenu' + category.name;
        // const isCollapsedService = true;
        return (
            <div>
              <List
                key={category._id}
                component="nav"
                // subheader={<ListSubheader component="div">{category.name}</ListSubheader>}
              >
                <ListItem button onClick={() => {this.handleServiceClick('/entry/', category._id)}} key={category._id}>
                  <ListItemIcon>
                    {/*{this.state + '.' + collapseService ? (<ArrowDropUp />) : (<ArrowDropDown />)}*/}
                    <StarBorder />
                  </ListItemIcon>
                  <ListItemText primary={category.name} />
                </ListItem>
              </List>

              {/*<Collapse in={this.state + '.' + collapseService} timeout="auto" unmountOnExit>*/}
                <List component="div" disablePadding>
                  {category.types.map((type) => (
                    <ListItem
                      key={type._id}
                      button
                      onClick={() => {this.handleServiceClick('/entry/', category._id, type._id)}}
                    >
                      {/*<ListItemIcon>*/}
                        {/*<EventIcon />*/}
                      {/*</ListItemIcon>*/}
                      <ListItemText style={{ marginLeft: '60px' }} primary={type.name} />
                    </ListItem>
                  ))}
                  <Divider />
                </List>
              {/*</Collapse>*/}
            </div>
          )
        }
      )}
    </div>
  );

  providerListItems = (role) => (
    <div key={role._id}>
      <Divider />
      <List
        component="nav"
        // subheader={<ListSubheader component="div">{i18n.t('core:providerOperations')}</ListSubheader>}
      >
        <ListItem button onClick={() => {this.handleClick('/admin/service/create')}}>
          <ListItemIcon>
            <AddEventIcon />
          </ListItemIcon>
          <ListItemText primary={i18n.t('core:createService')} />
        </ListItem>
        <ListItem button onClick={() => {this.handleClick('/dashboard/services')}}>
          <ListItemIcon>
            <ServicesIcon />
          </ListItemIcon>
          <ListItemText primary={i18n.t('core:myServices')} />
        </ListItem>
        <ListItem button onClick={() => {this.handleClick('/dashboard/service/orders/')}}>
          <ListItemIcon>
            <MyEventsIcon />
          </ListItemIcon>
          <ListItemText primary={i18n.t('core:myOrders')} />
        </ListItem>
      </List>
    </div>
  );

  adminListItems = (role) => (
    <div key={role._id}>
      <Divider />

      <List
        component="nav"
        // subheader={<ListSubheader component="div">{i18n.t('core:adminDashboard')}</ListSubheader>}
      >
        <ListItem button onClick={(e) => {this.handleCollapseAdminMenu(e)}}>
          <ListItemIcon>
            {this.state.collapseAdminMenu ? (<ArrowDropUp />) : (<ArrowDropDown />)}
          </ListItemIcon>
          <ListItemText primary={i18n.t('core:dashboard')} />
        </ListItem>
      </List>

      <Collapse in={this.state.collapseAdminMenu} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ListItem button onClick={() => {this.handleClick('/admin')}}>
            <ListItemIcon>
              <EventIcon />
            </ListItemIcon>
            <ListItemText primary={i18n.t('core:adminDashboard')} />
          </ListItem>
          <ListItem button onClick={() => {this.handleClick('/admin/services')}}>
            <ListItemIcon>
              <EventIcon />
            </ListItemIcon>
            <ListItemText primary={i18n.t('core:serviceDashboard')} />
          </ListItem>
          <ListItem button onClick={() => {this.handleClick('/admin/all-orders')}}>
            <ListItemIcon>
              <EventIcon />
            </ListItemIcon>
            <ListItemText primary={i18n.t('core:allOrders')} />
          </ListItem>
          <ListItem button onClick={() => {this.handleClick('/admin/retirement-clubs')}}>
            <ListItemIcon>
              <EventIcon />
            </ListItemIcon>
            <ListItemText primary={i18n.t('core:retirementClubsDashboard')} />
          </ListItem> .
          <ListItem button onClick={() => {this.handleClick('/admin/news')}}>
            <ListItemIcon>
              <EventIcon />
            </ListItemIcon>
            <ListItemText primary={i18n.t('core:newsDashboard')} />
          </ListItem>
        </List>

        <List
          component="nav"
          // subheader={<ListSubheader component="div">{i18n.t('core:adminOperations')}</ListSubheader>}
        >
          <ListItem button onClick={() => {this.handleClick('/admin/service/create')}}>
            <ListItemIcon>
              <AddEventIcon />
            </ListItemIcon>
            <ListItemText primary={i18n.t('core:createService')} />
          </ListItem>
          <ListItem button onClick={() => {this.handleClick('/admin/club/create')}}>
            <ListItemIcon>
              <AddEventIcon />
            </ListItemIcon>
            <ListItemText primary={i18n.t('core:createClub')} />
          </ListItem>
          <ListItem button onClick={() => {this.handleClick('/admin/news/create')}}>
            <ListItemIcon>
              <AddEventIcon />
            </ListItemIcon>
            <ListItemText primary={i18n.t('core:createNews')} />
          </ListItem>
          {/* <ListItem button onClick={() => {this.handleClick('/admin/news/create')}}>
          <ListItemIcon>
            <PeopleIcon/>
          </ListItemIcon>
          <ListItemText primary={i18n.t('core:viewUsers')} />
        </ListItem>*/}
          {/* <ListItem button onClick={() => {this.handleClick('/admin/invite')}}>
          <ListItemIcon>
            <InviteIcon/>
          </ListItemIcon>
          <ListItemText primary={i18n.t('core:inviteAgent')} />
        </ListItem>*/}
        </List>
      </Collapse>
    </div>
  );

  serviceList = (classes, isAuth) => {
    return (
      <div className={classes.list}>
        {this.userListItems(classes, isAuth)}
      </div>
    )
  };

  sideList = (classes, isAuth) => (
    <div className={classes.list}>
      {this.userListItems(classes, isAuth)}
      {this.user && this.user.roles.map(role => {
          if (role === 'provider') {
            return this.providerListItems(role)
          }
        })
      }
      {this.user && this.user.roles.map(role => {
          if (role === 'admin') {
            return this.adminListItems(role)
          }
        })
      }
    </div>
  );

  renderMobileMenu = (classes) => (
    <div className={classes.fullList}>
      <List>
        <Divider />
        <ListItem button>
          <ListItemIcon>
            <EventIcon />
          </ListItemIcon>
          <ListItemText primary={i18n.t('core:healthCare')} />
        </ListItem>
        <ListItem button>
          <ListItemIcon>
            <EventIcon />
          </ListItemIcon>
          <ListItemText primary={i18n.t('core:leadership')} />
        </ListItem>
        <ListItem button>
          <ListItemIcon>
            <AddEventIcon />
          </ListItemIcon>
          <ListItemText primary={i18n.t('core:outcomes')} />
        </ListItem>
        <ListItem button>
          <ListItemIcon>
            <AddEventIcon />
          </ListItemIcon>
          <ListItemText primary={i18n.t('core:approach')} />
        </ListItem>
        <ListItem button>
          <ListItemIcon>
            <PeopleIcon />
          </ListItemIcon>
          <ListItemText primary={i18n.t('core:about')} />
        </ListItem>
      </List>
    </div>
  );

  renderToggleDrawer = (classes, drawerSide) => {
    return (
      <IconButton
        className={classes.menuButton}
        color='inherit'
        aria-label='Menu'
        onClick={this.toggleDrawer(drawerSide, true)}
      >
        <MenuIcon/>
      </IconButton>
    )
  };

  render() {
    const { classes, isMobile } = this.props;
    const { isAuthenticated, anchorEl, anchorLanguageEl, anchorCountryEl } = this.state;
    const open = Boolean(anchorEl);
    const openLng = Boolean(anchorLanguageEl);
    const openCountryList = Boolean(anchorCountryEl);
    console.log('Main Page (Header): ', this.props);

    return (
      <div className={classes.root}>
        <AppBar position='static' className={classes.appBar}>
          <Toolbar>

            {this.renderToggleDrawer(classes, 'left')}
            {/*{this.renderToggleDrawer(classes, 'left')}*/}
            {!isAuthenticated && isMobile && this.renderToggleDrawer(classes, 'top')}

            <Typography
              variant='title'
              color='inherit'
              className={classes.flex}
              onClick={() => {this.handleClick('/')}}
            >
              <img src="/src/public/assets/img/logo_400.png" height="50" />
            </Typography>

            <Typography
              variant='title'
              color='inherit'
            >
              <IconButton
                aria-owns={openLng ? 'headerLanguage' : null}
                aria-haspopup='true'
                onClick={this.handleSearchMenu}
                color='inherit'
              >
                <SearchIcon />
              </IconButton>
            </Typography>

            <div>
              <IconButton
                aria-owns={openCountryList ? 'headerCountry' : null}
                aria-haspopup='true'
                onClick={this.handleCountryMenu}
                color='inherit'
              >
                <GlobeIcon />
              </IconButton>
              <Menu
                id='headerCountry'
                anchorEl={this.state.anchorCountryEl}
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                open={openCountryList}
                PaperProps={{
                  style: {
                    maxHeight: 320,
                  },
                }}
                onClose={this.handleClose}
              >
                {this.props.app.supportedCountries.map(country => (
                  <MenuItem
                    key={country.label}
                    value={country.value}
                    onClick={() => {
                      this.handleClose();
                      this.props.setUserCountry(country.value);

                    }}
                  >
                    {country.label}
                  </MenuItem>
                ))}
              </Menu>
            </div>

            <div>
              {this.props.app.userCountry && (
                <Typography
                  variant='title'
                  color='inherit'
                >
                  {this.props.app.userCountry}
                </Typography>
              )}
            </div>

            <div>
              <IconButton
                aria-owns={openLng ? 'headerLanguage' : null}
                aria-haspopup='true'
                onClick={this.handleLanguageMenu}
                color='inherit'
              >
                <TranslateIcon />
              </IconButton>
              <Menu
                id='headerLanguage'
                anchorEl={this.state.anchorLanguageEl}
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                open={openLng}
                onClose={this.handleClose}
              >
                {this.props.app.supportedLanguages.map(language => (
                  <MenuItem
                    key={language.iso}
                    value={language.iso}
                    onClick={() => {
                      this.handleClose();
                      this.props.setLandLanguage(language.iso);

                    }}
                  >
                    {language.title}
                  </MenuItem>
                ))}
              </Menu>
            </div>

            {isAuthenticated && (
              <div>
                <IconButton
                  aria-owns={open ? 'headerMenuAppBar' : null}
                  aria-haspopup='true'
                  onClick={this.handleMenu}
                  color='inherit'
                >
                  <AccountCircle />
                </IconButton>
                <Menu
                  id='headerMenuAppBar'
                  anchorEl={anchorEl}
                  anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                  transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                  open={open}
                  onClose={this.handleClose}
                >
                  <MenuItem
                    onClick={() => {this.handleClick('/profile', true)}}
                  >
                    <AccountCircle style={{ marginRight: 10 }} /> {i18n.t('core:myProfile')}
                  </MenuItem>
                {/*<MenuItem
                    onClick={() => {this.handleClick('/profile/cart', true)}}
                  >
                    {i18n.t('core:myCart')}
                  </MenuItem>*/}
                  <MenuItem
                    onClick={() => {this.handleClick('/profile/orders', true)}}
                  >
                    {i18n.t('core:myOrders')}
                  </MenuItem>
                  <MenuItem
                    onClick={() => {this.handleClick('/logout', true)}}
                  >
                    {i18n.t('core:logout')}
                  </MenuItem>
                </Menu>
              </div>
            )}

            {!isAuthenticated && (
              <div>
                <Button
                  color='inherit'
                  onClick={() => {this.handleClick('/')}}
                >
                  {i18n.t('core:home')}
                </Button>
                <Button
                  color='inherit'
                  onClick={() => {this.handleClick('/login')}}
                >
                  {i18n.t('core:signIn')}
                </Button>
                <Button
                  color='inherit'
                  onClick={() => {this.handleClick('/register')}}
                >
                  {i18n.t('core:signUp')}
                </Button>
              </div>
            )}
          </Toolbar>
        </AppBar>

        {isAuthenticated && (
          <SwipeableDrawer
            open={this.state.left}
            onClose={this.toggleDrawer('left', false)}
            onOpen={this.toggleDrawer('left', true)}
          >
            <div
              tabIndex={0}
              role='button'
              onClick={this.toggleDrawer('left', false)}
              onKeyDown={this.toggleDrawer('left', false)}
            >
              {this.sideList(classes, isAuthenticated)}
            </div>
          </SwipeableDrawer>
        )}

        {!isAuthenticated && isMobile && (
          <SwipeableDrawer
            anchor="top"
            open={this.state.top}
            onClose={this.toggleDrawer('top', false)}
            onOpen={this.toggleDrawer('top', true)}
          >
            <div
              tabIndex={0}
              role='button'
              onClick={this.toggleDrawer('top', false)}
              onKeyDown={this.toggleDrawer('top', false)}
            >
              {this.renderMobileMenu(classes)}
            </div>
          </SwipeableDrawer>
        )}

        {!isAuthenticated && (
          <SwipeableDrawer
            open={this.state.left}
            onClose={this.toggleDrawer('left', false)}
            onOpen={this.toggleDrawer('left', true)}
          >
            <div
              tabIndex={0}
              role='button'
              onClick={this.toggleDrawer('left', false)}
              onKeyDown={this.toggleDrawer('left', false)}
            >
              {this.serviceList(classes, isAuthenticated)}
            </div>
          </SwipeableDrawer>
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isAuthenticated: state.auth.isAuthenticated,
    user: state.auth.user,
    app: getAppSettings(state),
  };
}

function mapActionCreatorsToProps(dispatch) {
  return bindActionCreators(AppSettingsActions, dispatch);
}

export default withRouter(connect(mapStateToProps, mapActionCreatorsToProps)(translate(['core'], { wait: true })(withStyles(styles)(Header))));
