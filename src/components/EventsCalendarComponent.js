import React from 'react';
import InfiniteCalendar from 'react-infinite-calendar';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import List, { ListItem, ListItemText, ListItemAvatar, ListItemSecondaryAction } from 'material-ui/List';
import IconButton from 'material-ui/IconButton';
import Typography from 'material-ui/Typography';
import { FormControl } from 'material-ui/Form';
import DeleteIcon from '@material-ui/icons/Delete';
import { withStyles } from "material-ui/styles/index";
// import i18n from "../services/i18n";

const styles = theme => ({
  paper: {
    padding: 16,
    color: theme.palette.text.secondary,
  },
  textField: {
    marginLeft: 16,
    marginRight: 16,
    width: 200,
  },
  rightButton: {
    float: 'right',
    marginTop: 10,
    marginBottom: 10,
    marginRight: 10,
  },
  leftButton: {
    float: 'left',
    marginTop: 10,
    marginBottom: 10,
    marginRight: 10,
  },
  infiniteCalendar: {
    margin: 10,
    textAlign: 'center'
  }
});

class EventsCalendarComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    events: [],
    date: '',
    previewDate: '',
    description: '',
    showCalendar: false,
    error: '',
  };

  componentDidMount(nextProps, nextState) {
    if (nextProps && nextProps.hasOwnProperty('events')){
      this.setState({
        events: nextProps.events
      });
    }
  }

  handleDateSelect = (newDate) => {
    const date = new Date(newDate).getDate();
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    const month = new Date(newDate).getMonth();
    const previewDate = date + " " + months[month];
    this.setState({ date: new Date(newDate).getTime(), showCalendar: false, previewDate, error: '' })
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value,
      error: '',
    });
  };

  removeEvent = (event) => {
    let events = this.state.events;
    events.splice(events.indexOf(event), 1);
    this.setState({ events });
  };

  openCalendar = () => {
    this.setState({
      showCalendar: !this.state.showCalendar,
      error: '',
    })
  };

  handleAdd = () => {
    if (this.state.description.length < 1 || this.state.date.length < 1){
      return this.setState({
        error: "Description or Date must not be empty !"
      });
    }
    let event = {
      date: this.state.date,
      description: this.state.description,
    };
    let events = this.state.events;
    events.push(event);
    this.props.setEvents(events);
    this.setState({
      events: events,
      date: '',
      previewDate: '',
      description: '',
      error: '',
    });
  };

  render() {
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    const eventDate = (date) => {
      const newDate = new Date(date).getDate();
      const month = new Date(date).getMonth();
      return newDate + " " + months[month];
    };
    const { classes } = this.props;

    return (
      <Paper className={classes.paper}>
        <Grid container spacing={24}>
          <Grid item xs={12} sm={12}>
            <FormControl className={classes.margin} fullWidth={true}>
              <TextField
                id="description"
                label="Event Description"
                name="description"
                value={this.state.description}
                onChange={this.handleInputChange}
              />
              <TextField
                id="date-string"
                label="Select Event Date"
                value={this.state.previewDate || this.state.date }
                disabled={true}
                placeholder="Select a date"
              />
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={12}>
            {this.state.showCalendar &&
            <InfiniteCalendar
              width={320}
              height={200}
              selected={this.state.date}
              placeholder='Event Date'
              onSelect={this.handleDateSelect}
              className={classes.infiniteCalendar}
            />
            }
            <Button variant="raised" size="large" color="primary" className={classes.leftButton} onClick={() => {
              this.handleAdd()
            }}>
              Add Event
            </Button>
            <Button variant="raised" className={classes.rightButton} size="large" color="primary" onClick={() => {
              this.openCalendar()
            }}>
              Select Date
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={40}>
          <Grid item xs={12} sm={6}>
            {this.state.error.length > 0 &&
            <Typography variant="body1" color="red" align="center">
              {this.state.error}
            </Typography>
            }
            <List dense={true}>
              {this.state.events && this.state.events.length > 0 && this.state.events.map((event) => {
                return (
                  <ListItem>
                    <ListItemAvatar>
                      <Typography variant="caption" gutterBottom align="center">
                        {eventDate(event.date)}
                      </Typography>
                    </ListItemAvatar>
                    <ListItemText
                      primary={event.description}
                    />
                    <ListItemSecondaryAction>
                      <IconButton aria-label="Delete" onClick={() => {
                        this.removeEvent(event)
                      }}>
                        <DeleteIcon/>
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>);
              })}
            </List>
          </Grid>
        </Grid>
      </Paper>

    );
  }
}

export default withStyles(styles)(EventsCalendarComponent);
