import React from 'react';
// import InfiniteCalendar from 'react-infinite-calendar';
// import Grid from 'material-ui/Grid';
// import TextField from 'material-ui/TextField';
// import Button from 'material-ui/Button';
// import List, { ListItem, ListItemText, ListItemAvatar, ListItemIcon, ListItemSecondaryAction } from 'material-ui/List';
// import IconButton from 'material-ui/IconButton';
// import Typography from 'material-ui/Typography';
// import Paper from 'material-ui/Paper';
// import i18n from "../services/i18n";

class DateCalendarComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    events: [],
    date: '',
    description: ''
  };
/*
  componentDidMount(nextProps, nextState) {

  }

  handleDateSelect = (date) => {
    this.setState({ date: Date.parse(date) })
  };

  handleDescription = (description) => {
    this.setState({ description })
  };

  removeEvent = (event) => {
    let events = this.state.events;
    events.splice(events.indexOf(event),1);
    this.setState({ events });
  };

  handleSelect = () => {
    let event = {
      date: this.state.date,
      description: this.state.description
    };

    let events = this.state.events;
    events.push(event);

    this.props.setEvents(events);
    this.setState({
      events: events,
      date: '',
      description: '',
    });
  };

  render() {
    const today = new Date();
    const lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1);
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    const eventDate = (date) => {
      const newDate = new Date(date).getDate();
      const month = new Date(date).getMonth();
      return newDate + " " + months[month];
    };

    return (
      <div>
        <Grid container spacing={40}>
          <Grid item xs={12} sm={6}>
            <TextField
              id="multiline-description"
              label="Event Description"
              multiline
              rowsMax="4"
              value={this.state.description}
              onChange={this.handleDescription}
              margin="normal"
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <InfiniteCalendar
              width={180}
              height={200}
              selected={this.state.date}
              // disabledDays={[0,2]}
              minDate={lastWeek}
              placeholder='Event Date'
              onSelect={this.handleDateSelect}
            />
          </Grid>
        </Grid>
        <Grid container spacing={40}>
          <List dense={true}>
            {this.state.events && this.state.events.length > 0 && this.state.events.map((event) => {
              return (
              <ListItem>
                <ListItemAvatar>
                  <Typography variant="caption" gutterBottom align="center">
                    {eventDate(event.date)}
                  </Typography>
                </ListItemAvatar>
                <ListItemText
                  primary={event.description}
                  secondary={event.description}
                />
                <ListItemSecondaryAction>
                  <IconButton aria-label="Delete" onClick={() => {this.removeEvent(event)}}>
                    <DeleteIcon />
                  </IconButton>
                </ListItemSecondaryAction>
              </ListItem> );
            })}
          </List>
        </Grid>
        <Button variant="raised" size="large" color="primary" onClick={() => {this.handleSelect()}}>
          Add Event
        </Button>
      </div>
    );
  }*/
}

export default DateCalendarComponent;
