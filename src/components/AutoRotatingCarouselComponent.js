import React, { Component } from 'react';
import { AutoRotatingCarousel, Slide } from 'material-auto-rotating-carousel';

class AutoRotatingCarouselComponent extends Component {
  state = {
    currentTime: ''
  };

  // componentWillReceiveProps(nextProps) {
  //   this.setState({
  //     dataSlide: nextProps.dataSlide,
  //   })
  // }

  onHourChange(hour) {

  }

  onMinuteChange(minute) {

  }

  onTimeChange(time) {
    this.setState({
      currentTime: time
    });
    this.props.setCurrentTime(time)
  }

  onFocusChange(focusStatue) {

  }

  render() {
    console.log('dataSlide:::::', this.props.dataSlide);
    return (
      <div style={{ position: 'relative', width: '100%', height: 500 }}>
        <AutoRotatingCarousel
          label='Get started'
          open={this.props.open}
          onClose={this.props.onClose}
          style={{ position: 'absolute' }}
        >
          {this.props.dataSlide.map((data) => {
            return (
              <Slide
                key={data.key}
                media={<img src={data.media} />}
                mediaBackgroundStyle={data.mediaBackgroundStyle}
                style={data.style}
                title={data.title}
                subtitle={data.subtitle}
              />
            )
          })}
        </AutoRotatingCarousel>
      </div>
    )
  }
}

export default AutoRotatingCarouselComponent;
