import React, { Component } from 'react';
import InfiniteCalendar from 'react-infinite-calendar';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import Input, { InputLabel, InputAdornment } from 'material-ui/Input';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Chip from 'material-ui/Chip';
import IconButton from 'material-ui/IconButton';
import Typography from 'material-ui/Typography';
// import { withStyles } from "material-ui/styles/index";
import RichTextEditorComponent from '../components/RichTextEditorComponent';
import LocationSearchInputComponent from '../components/LocationSearchInputComponent';
import TimeComponent from '../components/TimeComponent';
import 'react-infinite-calendar/styles.css'; // Make sure to import the default stylesheet
import i18n from "../services/i18n";
import FolderIcon from '@material-ui/icons/Folder';
import AddIcon from '@material-ui/icons/Add';

class CreateServiceComponent extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    title: '',
    image: {},
    imagePreviewURL: '',
    price: '',
    date: '',
    tag: '',
    tags: [],
    description: '',
    content: '',
    location: '',
    locationLatitude: '',
    locationLongitude: '',
    address: '',
    startDate: '',
    endDate: '',
    startHours: '',
    endHours: '',
    place: {},
    lat: '',
    lng: '',
    activeStep: 0,
    orderQuantity: 0,
    //
    errorText: false,
    serviceSupportedLanguages: [],
    service: {},
    settings: []
  };

  componentDidMount(nextProps, nextState) {
    let newState = {};
    if (nextProps && nextProps.hasOwnProperty('service')) {
      newState.service = nextProps.service;
      const keys = Object.keys(nextProps.service);
      keys.forEach((key) => {
        newState[key] = nextProps.service[key];
      })
    }
    if (nextProps && nextProps.hasOwnProperty('settings')) {
      newState.settings = nextProps.settings;
    }
    this.setState(newState);
  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    }, this.handleValidation());

    this.updateService();
  };

  updateService = () => {
    let service = {
      title: this.state.title,
      price: this.state.price,
      content: this.state.content,
      description: this.state.description,
      location: this.state.location,
      place: this.state.place,
      image: this.state.image,
      imagePreviewURL: this.state.imagePreviewURL,
      orderQuantity: this.state.orderQuantity,
      date: this.state.date,
      startDate: this.state.startDate,
      endDate: this.state.endDate,
      startHours: this.state.startHours,
      endHours: this.state.endHours,
      tags: this.state.tags,
    };
    this.setState({ service });
    this.props.handleUpdateService(service);
  };

  handleValidation() {
    // const pathRegex = '^((\.\./|[a-zA-Z0-9_/\-\\])*\.[a-zA-Z0-9]+)$';
    // const nameRegex = '^[A-Z][-a-zA-Z]+$';
    if (this.state.title.length > 0) {
      this.setState({ inputError: false, disableConfirmButton: false });
    } else {
      this.setState({ inputError: true, disableConfirmButton: true });
    }
  }

  handleDateChange = (date) => {
    this.setState({ date: Date.parse(date) });
    this.updateService();
  };

  handleStartDateChange = (date) => {
    this.setState({ startDate: Date.parse(date) });
    this.updateService();
  };

  handleEndDateChange = (date) => {
    this.setState({ endDate: Date.parse(date) });
    this.updateService();
  };

  setLocation = (location) => {
    this.setState({ location });
    this.updateService();
  };

  setContent = (content) => {
    this.setState({ content });
    this.updateService();
  };

  setPlaceFromInput = (place) => {
    this.setState({
      location: place.location,
      place: place,
      lat: place.latitude,
      lng: place.longitude,
    });
    this.updateService();
  };

  setStartTime = (time) => {
    this.setState({ startTime: time });
    this.updateService();
  };

  setEndTime = (time) => {
    this.setState({ endTime: time });
    this.updateService();
  };

  handleAddTag = () => {
    if (this.state.tag.length > 0) {
      let { tags, tag } = this.state;

      tags.push({
        name: tag,
        slug: tag.toLowerCase()
      });
      this.setState({ tag: '', tags: tags });
    }
    this.updateService();
  };

  handleDeleteTag = data => {
    const tags = [...this.state.tags];
    const deleteServiceTag = tags.indexOf(data);
    tags.splice(deleteServiceTag, 1);
    this.setState({ tags });
    this.updateService();
  };

  handleInputImageChange = (event) => {
    event.preventDefault();
    let reader = new FileReader();
    let file = event.target.files[0];

    reader.onloadend = () => {
      this.setState({
        image: file,
        imagePreviewURL: reader.result
      });
      this.updateService();
    };

    reader.readAsDataURL(file);
  };

  handleMouseDown = event => {
    event.preventDefault();
  };

  renderTags = (classes) => {
    return (
      this.state.tags.map(data => (
          <Chip
            key={data.name}
            avatar={null}
            label={data.name}
            onDelete={() => this.handleDeleteTag(data)}
            className={classes.tag}
          />
        )
      )
    )
  };

  render() {
    const today = new Date();
    // const lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
    const lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1);
    const classes = this.props.classes;

    return (
      <Grid container spacing={40} className={classes.mainSubHeaderContainer}>
        <Grid item xs={12} sm={6}>

          <div className={classes.textField}>
            <img
              className={classes.imgPreviewCS}
              src={this.state.imagePreviewURL ? this.state.imagePreviewURL : '/src/public/assets/img/image_placeholder.jpg'} height="150"
            />
          </div>

          <FormControl
            encType="multipart/form-data"
            fullWidth={true}
            error={this.state.errorText}
          >
            <Input
              required
              margin="dense"
              disabled={true}
              placeholder={i18n.t('core:choosePhoto')}
              fullWidth={true}
              data-tid="createServiceImage"
              className={classes.textField}
              value={this.state.image.name}
              endAdornment={
                <InputAdornment position="end" style={{ height: 32 }}>
                  <IconButton>
                    <label htmlFor="createServiceUploadImage">
                      <FolderIcon />
                    </label>
                  </IconButton>
                </InputAdornment>
              }
            />
            {this.state.errorText && <FormHelperText>{i18n.t('core:InvalidUrlAddressMessage')}</FormHelperText>}
          </FormControl>

          <FormControl fullWidth={true}>
            <TextField
              className={classes.textField}
              helperText={i18n.t('core:serviceTitle')}
              label={i18n.t('core:serviceTitle')}
              name="title"
              value={this.state.title}
              onChange={this.handleInputChange}
              fullWidth
            />
          </FormControl>

          <FormControl fullWidth={true}>
            <TextField
              id="createServiceDescription"
              placeholder={i18n.t('core:serviceDescription')}
              rows="4"
              rowsMax="6"
              multiline
              name="description"
              className={classes.textField}
              value={this.state.description}
              onChange={e => this.handleInputChange(e)}
              fullWidth={true}
            />
          </FormControl>

          {this.props.settings.isPayment && (
            <FormControl fullWidth={true}>
              <TextField
                className={classes.textField}
                helperText={i18n.t('core:helperTextServiceTitle')}
                label={i18n.t('core:price')}
                name="price"
                type="number"
                value={this.state.price}
                onChange={this.handleInputChange}
                fullWidth
              />
            </FormControl>
          )}

          <FormControl className={classes.margin} fullWidth={true}>
            <InputLabel className={classes.inputLabel} htmlFor="service-tags">{i18n.t('core:addTagToService')}</InputLabel>
            <Input
              id="service-tags"
              type="text"
              className={classes.textField}
              value={this.state.tag}
              name="tag"
              fullWidth={true}
              onChange={this.handleInputChange}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label={i18n.t('core:addTagToService')}
                    onClick={this.handleAddTag}
                    onMouseDown={this.handleMouseDown}
                  >
                    <AddIcon/>
                  </IconButton>
                </InputAdornment>
              }
            />

            {this.state.tags.length > 0 && (
              <Paper className={classes.paper}>
                {this.renderTags(classes)}
              </Paper>
            )}
          </FormControl>
        </Grid>

        <Grid item xs={12} sm={6}>
          <FormControl fullWidth={true} className={classes.textField}>
            <LocationSearchInputComponent setPlaceFromInput={this.setPlaceFromInput} />
          </FormControl>

          {this.props.settings.isLimitOrderQuantity && (
            <FormControl fullWidth={true} className={classes.textField}>
              <TextField
                helperText={i18n.t('core:helperTextOrderQuantity')}
                label={i18n.t('core:orderQuantity')}
                name="orderQuantity"
                value={this.state.orderQuantity}
                type="number"
                onChange={this.handleInputChange}
              />
            </FormControl>
          )}

          {this.props.settings.isStartDate && (
            <FormControl fullWidth={true} className={classes.textField}>
              <Typography variant="headline" component="h4">
                {i18n.t('core:helperTextStartDate')}
              </Typography>

              <InfiniteCalendar
                className={classes.infiniteCalendar}
                width={320}
                height={250}
                selected={this.state.startDate}
                // disabledDays={[0,2]}
                minDate={lastWeek}
                onSelect={this.handleStartDateChange}
              />
            </FormControl>
          )}

          {this.props.settings.isEndDate && (
            <FormControl fullWidth={true} className={classes.textField}>
              <Typography variant="headline" component="h4">
                {i18n.t('core:helperTextEndDate')}
              </Typography>

              <InfiniteCalendar
                className={classes.infiniteCalendar}
                width={320}
                height={250}
                selected={this.state.endDate}
                // disabledDays={[0,2]}
                minDate={lastWeek}
                onSelect={this.handleEndDateChange}
              />
            </FormControl>
          )}

          {this.props.settings.isStartHour && (
            <FormControl fullWidth={true} className={classes.textField}>
              <Typography variant="headline" component="h4">
                {i18n.t('core:helperTextStartHour')}
              </Typography>
              <TimeComponent setCurrentTime={this.setStartTime} />
            </FormControl>
          )}

          {this.props.settings.isEndHour && (
            <FormControl fullWidth={true} className={classes.textField}>
              <Typography variant="headline" component="h4">
                {i18n.t('core:helperTextEndHour')}
              </Typography>
              <TimeComponent setCurrentTime={this.setEndTime} />
            </FormControl>
          )}

          {this.props.settings.isDateCalendar && (
            <FormControl fullWidth={true} className={classes.textField}>
              <Typography variant="headline" component="h4">
                {i18n.t('core:helperTextDateCalendar')}
              </Typography>

              <InfiniteCalendar
                className={classes.infiniteCalendar}
                width={320}
                height={250}
                selected={this.state.date}
                // disabledDays={[0,2]}
                minDate={lastWeek}
                onSelect={this.handleDateChange}
              />
            </FormControl>
          )}
        </Grid>

        <Grid item xs={12} sm={12}>
          <FormControl fullWidth={true} className={classes.textField}>
            <RichTextEditorComponent setContent={this.setContent} />
          </FormControl>
        </Grid>

        <form encType="multipart/form-data" className={classes.input}>
          <input
            accept="image/*"
            onChange={this.handleInputImageChange}
            id="createServiceUploadImage"
            name="service"
            type="file"
          />
        </form>
      </Grid>
    );
  }
}

export default CreateServiceComponent;
