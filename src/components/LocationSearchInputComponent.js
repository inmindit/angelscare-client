import React from 'react';
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/Menu/MenuItem';
import i18n from '../services/i18n';

class LocationSearchInputComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    address: '',
    gmapsLoaded: false,
  };

  componentDidMount(nextProps, nextState) {
    window.initMap = this.initMap;
    const gmapScriptEl = document.createElement('script');
    gmapScriptEl.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD7dS3buTHIxvseyVDmAHkFAEIGzctj0Q4&libraries=places&callback=initMap';
    document.querySelector('body').insertAdjacentElement('beforeend', gmapScriptEl);
  }

  initMap = () => {
    this.setState({
      gmapsLoaded: true,
    })
  };

  handleChange = (address) => {
    this.setState({ address })
  };

  handleSelect = (address) => {
    let result = {};
    geocodeByAddress(address)
      .then(results => {
        result = results[0];
        return getLatLng(results[0]);
      })
      .then((latLng) => {
        let params = {};
        result.address_components.map((address) => {
          if (address.types[0].indexOf('_') > -1) {
            address.types[0] = address.types[0].substr(0, address.types[0].indexOf('_'));
          }
          params[address.types[0]] = address.long_name;
          return address;
        });
        params.location = result.formatted_address;
        params.latitude = latLng.lat;
        params.longitude = latLng.lng;
        this.props.setPlaceFromInput(params);
      })
      .catch(error => console.error('Error', error))
  };

  selectLocation = (location) => {
    console.log('selectLocation::', location);
    this.setState({
      address: location
    });
  };

  render() {
    return (
      <div>
        {this.state.gmapsLoaded && (
          <PlacesAutocomplete
            value={this.state.address}
            onChange={this.handleChange}
            onSelect={this.handleSelect}
          >
            {({ getInputProps, suggestions, getSuggestionItemProps }) => (
              <div>
                <TextField
                  id="createServiceSelectLocation"
                  label={i18n.t('core:createServiceLocationHeader')}
                  fullWidth={true}
                  {...getInputProps({
                    placeholder: this.props.placeholder || 'Where is the service located at ?',
                    className: 'location-search-input'
                  })}
                  helperText={i18n.t('core:createServiceLocationHelper')}
                  margin="normal"
                />

                <div className="autocomplete-dropdown-container">
                  {suggestions.map(suggestion => {
                    console.log(suggestion);
                    const className = suggestion.active ? 'suggestion-item--active' : 'suggestion-item';
                    const style = suggestion.active
                      ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                      : { backgroundColor: '#ffffff', cursor: 'pointer' };

                    return (
                      <MenuItem
                        onClick={() => this.selectLocation(suggestion.description)}
                        {...getSuggestionItemProps(suggestion, { className, style })}
                        key={suggestion.id}
                        value={suggestion.description}
                      >
                        {suggestion.description}
                      </MenuItem>
                    )}
                  )}
                </div>
              </div>
            )}
          </PlacesAutocomplete>
        )}
      </div>
    );
  }
}

export default LocationSearchInputComponent;
