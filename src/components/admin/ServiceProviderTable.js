import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import classNames from 'classnames';
import Table, {
  TableBody,
  TableCell,
  TablePagination,
  TableRow,
} from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import Button from 'material-ui/Button';
import Checkbox from 'material-ui/Checkbox';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import FilterListIcon from '@material-ui/icons/FilterList';
import TableHeadTable, { toolbarStyles } from './TableHeadTable';
import DeleteIcon from '@material-ui/icons/Delete';
import { tableStyles } from '../../assets/jss/configTableStyles';
import i18n from '../../services/i18n';

let EnhancedTableToolbar = props => {
  const { numSelected, classes } = props;

  return (
    <Toolbar
      className={classNames(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      <div className={classes.title}>
        {numSelected > 0 ? (
          <Typography color="inherit" variant="subheading">
            {numSelected} selected
          </Typography>
        ) : (
          <Typography variant="title">{i18n.t('core:services')}</Typography>
        )}
      </div>
      <div className={classes.spacer} />
      <div className={classes.actions}>
        {numSelected > 0 ? (
          <Tooltip title="Delete">
            <IconButton aria-label="Delete">
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        ) : (
          <Tooltip title="Filter list">
            <IconButton aria-label="Filter list">
              <FilterListIcon />
            </IconButton>
          </Tooltip>
        )}
      </div>
    </Toolbar>
  );
};
EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

const orderProviderColumnData = [
  { id: 'id', numeric: false, label: 'User ID' },
  { id: 'name', numeric: false, label: 'Name' },
  { id: 'email', numeric: false, label: 'Email' },
  { id: 'username', numeric: false, label: 'Username' },
  { id: 'roles', numeric: false, label: 'Roles' },
  { id: 'setRole', numeric: false, label: 'Set Role' },
  { id: 'created', numeric: false, label: 'Created' },
];

type Props = {};

type State = {
  order: string,
  orderBy: string,
  selected: Array,
  services: Array<Object>,
  page: number,
  rowsPerPage: number
};

class ServiceProviderTable extends Component<Props, State> {
  constructor(props, context) {
    super(props, context);

    this.state = {
      order: 'asc',
      orderBy: 'calories',
      selected: [],
      services: [],
      page: 0,
      rowsPerPage: 5,
      selectedRole: [],
      userRoles: [
        'user',
        'agent',
        'provider',
        'admin'
      ]
    };
  }

  componentWillReceiveProps(nextProps: any) {
    this.setState({
      services: nextProps.services
    });
  };

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    const services =
      order === 'desc'
        ? this.state.services.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
        : this.state.services.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

    this.setState({ services: services, order, orderBy });
  };

  handleSelectAllClick = (event, checked) => {
    if (checked) {
      this.setState({ selected: this.state.services.map(n => n._id) });
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  handleEditService = () => {
    this.props.showEditUserDialog();
  };

  handleDeleteService = () => {
    this.props.showDeleteUserDialog();
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    const { classes } = this.props;
    const { services, order, orderBy, selected, rowsPerPage, page } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, services.length - page * rowsPerPage);

    return (
      <Paper className={classes.root} elevation={4}>
        <div className={classes.contentHeader}>
          <Typography variant="headline" component="h3" className={classes.typographyTitle}>
            {i18n.t('core:services')}
          </Typography>
          <Typography variant="headline" component="p" className={classes.typographyOffset}>
            {i18n.t('core:servicesAdminRoleDescription')}
          </Typography>
        </div>
        <div className={classes.tableActionHeader}>
          <Button
            aria-label={i18n.t('core:servicesAdd')}
            color="primary"
            onClick={this.handleEditService}
          >
            {i18n.t('core:servicesAdd')}
          </Button>

          <Button
            aria-label={i18n.t('core:servicesDelete')}
            color="primary"
            onClick={this.handleDeleteService}
          >
            {i18n.t('core:servicesDelete')}
          </Button>
        </div>
        <EnhancedTableToolbar numSelected={selected.length} />

        <div className={classes.tableWrapper}>
          <Table className={classes.table}>
            <TableHeadTable
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={services.length}
              columnData={orderProviderColumnData}
            />
            <TableBody>
              {services.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(service => {
                const isSelected = this.isSelected(service._id);
                return (
                  <TableRow
                    hover
                    onClick={event => this.handleClick(event, service._id)}
                    role="checkbox"
                    aria-checked={isSelected}
                    tabIndex={-1}
                    key={service._id}
                    selected={isSelected}
                  >
                    <TableCell padding="checkbox">
                      <Checkbox checked={isSelected} />
                    </TableCell>
                    <TableCell>{service._id}</TableCell>
                    <TableCell>{service.displayName}</TableCell>
                    <TableCell>{service.email}</TableCell>
                    <TableCell>{service.username}</TableCell>
                    <TableCell>{service.created}</TableCell>
                  </TableRow>
                );
              })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          component="div"
          count={services.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    );
  }
}

export default withStyles(tableStyles)(ServiceProviderTable);
