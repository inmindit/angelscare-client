import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import classNames from 'classnames';
import Table, {
  TableBody,
  TableCell,
  TablePagination,
  TableRow,
} from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import Checkbox from 'material-ui/Checkbox';
import Toolbar from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import FilterListIcon from '@material-ui/icons/FilterList';
import TableHeadTable, { toolbarStyles } from './TableHeadTable';
import DeleteIcon from '@material-ui/icons/Delete';
import { tableStyles } from '../../assets/jss/configTableStyles';
import i18n from '../../services/i18n';

export const newsColumnData = [
  { id: 'newsId', numeric: false, label: 'News ID' },
  { id: 'title', numeric: false, label: 'News Title' },
  { id: 'category', numeric: false, label: 'Category' },
  { id: 'Types', numeric: false, label: 'Types'},
  { id: 'created', numeric: false, label: 'Created' },
  { id: 'updated', numeric: false, label: 'Updated' },
];

let EnhancedTableToolbar = props => {
  const { numSelected, classes } = props;

  return (
    <Toolbar
      className={classNames(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      <div className={classes.title}>
        {numSelected > 0 ? (
          <Typography color="inherit" variant="subheading">
            {numSelected} selected
          </Typography>
        ) : (
          <Typography variant="title">{i18n.t('core:news')}</Typography>
        )}
      </div>
      <div className={classes.spacer} />
      <div className={classes.actions}>
        {numSelected > 0 ? (
          <Tooltip title="Delete">
            <IconButton aria-label="Delete"
                        onClick={() => this.handleDeleteService()}
            >
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        ) : (
          <Tooltip title="Filter list">
            <IconButton aria-label="Filter list">
              <FilterListIcon />
            </IconButton>
          </Tooltip>
        )}
      </div>
    </Toolbar>
  );
};
EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

type Props = {};

type State = {
  order: string,
  orderBy: string,
  selected: Array,
  news: Array<Object>,
  page: number,
  rowsPerPage: number
};

class NewsTable extends Component<Props, State> {
  constructor(props, context) {
    super(props, context);

    this.state = {
      order: 'asc',
      orderBy: 'calories',
      selected: [],
      selectedNews: {},
      news: [
        // { _id: 1, event: 'Event 1', type: 'tour', location: 'Prague', created: '11/12/18', createdBy: 'Jordan' },
        // { _id: 2, event: 'Event 2', type: 'caretaker', location: 'Berlin', created: '11/12/18', createdBy: 'Kristiyan' },
        // { _id: 3, event: 'Event 3', type: 'event', location: 'Frankfurt', created: '11/12/18', createdBy: 'Jordan' },
        // { _id: 4, event: 'Event 4', type: 'tour', location: 'Sofia', created: '11/12/18', createdBy: 'Kristiyan' },
        // { _id: 5, event: 'Event 5', type: 'event', location: 'Prague', created: '11/12/18', createdBy: 'Jordan' },
      ],
      page: 0,
      rowsPerPage: 5,
    };
  }

  componentWillReceiveProps(nextProps: any) {
    this.setState({
      news: nextProps.news || this.state.news
    });
  };

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    const news =
      order === 'desc'
        ? this.state.news.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
        : this.state.news.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

    this.setState({ news: news, order, orderBy });
  };

  handleSelectAllClick = (event, checked) => {
    if (checked) {
      this.setState({ selected: this.state.news.map(n => n._id) });
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (news) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(news._id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, news._id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }
    this.setState({
      selected: newSelected,
      selectedNews: news
    });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  handleAddNews = () => {
    // this.props.showAddNewsDialog();
  };

  handleUpdateNews = () => {
    if (this.state.selectedNews) {
      this.props.showEditNewsDialog(this.state.selectedNews);
      // this.props.toggleNewsDrawer(this.state.selectedNews)
    }
  };

  handleDeleteNews = () => {
    if (this.state.selectedNews) {
      this.props.showDeleteNewsDialog(this.state.selectedNews)
    }
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    const { classes } = this.props;
    const { news, order, orderBy, selected, rowsPerPage, page } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, news.length - page * rowsPerPage);

    return (
      <Paper className={classes.root}>
        <div className={classes.contentHeader}>
          <Typography variant="headline" component="h3" className={classes.typographyTitle}>
            {i18n.t('core:news')}
          </Typography>
          <Typography variant="headline" component="p" className={classes.typographyOffset}>
            {i18n.t('core:newsAdminRoleDescription')}
          </Typography>
        </div>
        <div className={classes.tableActionHeader}>
          <Button
            aria-label={i18n.t('core:newsAdd')}
            color="primary"
            onClick={() => this.handleAddNews()}
          >
            {i18n.t('core:newsAdd')}
          </Button>
          <Button
            aria-label={i18n.t('core:newsUpdate')}
            color="primary"
            onClick={() => this.handleUpdateNews()}
          >
            Update Service
          </Button>
          <Button
            aria-label={i18n.t('core:newsDelete')}
            color="primary"
            onClick={() => this.handleDeleteNews()}
          >
            {i18n.t('core:newsDelete')}
          </Button>
        </div>
        <EnhancedTableToolbar numSelected={selected.length} />

        <div className={classes.tableWrapper}>
          <Table className={classes.table}>
            <TableHeadTable
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={news.length}
              columnData={newsColumnData}
            />
            <TableBody>
              {news.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(news => {
                const isSelected = this.isSelected(news._id);
                return (
                  <TableRow
                    hover
                    onClick={() => this.handleClick(news)}
                    role="checkbox"
                    aria-checked={isSelected}
                    tabIndex={-1}
                    key={news._id}
                    selected={isSelected}
                  >
                    <TableCell padding="checkbox">
                      <Checkbox checked={isSelected} />
                    </TableCell>
                    <TableCell>{news._id}</TableCell>
                    <TableCell>{news.title}</TableCell>
                    <TableCell>{news.category ? news.category.name : ''}</TableCell>
                    <TableCell>{news.type ? news.type.name : ''}</TableCell>
                    <TableCell>{news.created}</TableCell>
                    <TableCell>{news.updated}</TableCell>
                  </TableRow>
                );
              })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          component="div"
          count={news.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    );
  }
}

export default withStyles(tableStyles)(NewsTable);
