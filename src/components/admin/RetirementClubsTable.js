import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import classNames from 'classnames';
import Table, {
  TableBody,
  TableCell,
  TablePagination,
  TableRow,
} from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import Checkbox from 'material-ui/Checkbox';
import Toolbar from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import FilterListIcon from '@material-ui/icons/FilterList';
import TableHeadTable, { toolbarStyles } from './TableHeadTable';
import DeleteIcon from '@material-ui/icons/Delete';
import { tableStyles } from '../../assets/jss/configTableStyles';
import i18n from '../../services/i18n';

export const clubsColumnData = [
  { id: 'serviceId', numeric: false, label: 'Service ID' },
  { id: 'service', numeric: false, label: 'Service Title' },
  { id: 'category', numeric: false, label: 'Category' },
  { id: 'status', numeric: false, label: 'Status' },
  { id: 'location', numeric: false, label: 'Location' },
  { id: 'date', numeric: false, label: 'Date' },
];

let EnhancedTableToolbar = props => {
  const { numSelected, classes } = props;

  return (
    <Toolbar
      className={classNames(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      <div className={classes.title}>
        {numSelected > 0 ? (
          <Typography color="inherit" variant="subheading">
            {numSelected} selected
          </Typography>
        ) : (
          <Typography variant="title">{i18n.t('core:clubs')}</Typography>
        )}
      </div>
      <div className={classes.spacer} />
      <div className={classes.actions}>
        {numSelected > 0 ? (
          <Tooltip title="Delete">
            <IconButton aria-label="Delete"
                        onClick={() => this.handleDeleteClub()}
            >
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        ) : (
          <Tooltip title="Filter list">
            <IconButton aria-label="Filter list">
              <FilterListIcon />
            </IconButton>
          </Tooltip>
        )}
      </div>
    </Toolbar>
  );
};
EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

type Props = {};

type State = {
  order: string,
  orderBy: string,
  selected: Array,
  clubs: Array<Object>,
  page: number,
  rowsPerPage: number
};

class RetirementClubsTable extends Component<Props, State> {
  constructor(props, context) {
    super(props, context);

    this.state = {
      order: 'asc',
      orderBy: 'calories',
      selected: [],
      selectedClub: {},
      clubs: [
        // { _id: 1, event: 'Event 1', type: 'tour', location: 'Prague', created: '11/12/18', createdBy: 'Jordan' },
        // { _id: 2, event: 'Event 2', type: 'caretaker', location: 'Berlin', created: '11/12/18', createdBy: 'Kristiyan' },
        // { _id: 3, event: 'Event 3', type: 'event', location: 'Frankfurt', created: '11/12/18', createdBy: 'Jordan' },
        // { _id: 4, event: 'Event 4', type: 'tour', location: 'Sofia', created: '11/12/18', createdBy: 'Kristiyan' },
        // { _id: 5, event: 'Event 5', type: 'event', location: 'Prague', created: '11/12/18', createdBy: 'Jordan' },
      ],
      page: 0,
      rowsPerPage: 5,
    };
  }

  componentWillReceiveProps(nextProps: any) {
    this.setState({
      clubs: nextProps.clubs || this.state.clubs
    });
  };

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    const clubs =
      order === 'desc'
        ? this.state.clubs.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
        : this.state.clubs.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

    this.setState({ clubs: clubs, order, orderBy });
  };

  handleSelectAllClick = (event, checked) => {
    if (checked) {
      this.setState({ selected: this.state.clubs.map(n => n._id) });
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (service) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(service._id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, service._id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }
    this.setState({
      selected: newSelected,
      selectedClub: service
    });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  handleAddClub = () => {
    // this.props.showAddClubDialog();
  };

  handleUpdateClub = () => {
    if (this.state.selectedClub) {
      this.props.showEditClubDialog(this.state.selectedClub)
    }
  };

  handleDeleteClub = () => {
    if (this.state.selectedClub) {
      this.props.showDeleteClubDialog(this.state.selectedClub)
    }
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    const { classes } = this.props;
    const { clubs, order, orderBy, selected, rowsPerPage, page } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, clubs.length - page * rowsPerPage);

    return (
      <Paper className={classes.root}>
        <div className={classes.contentHeader}>
          <Typography variant="headline" component="h3" className={classes.typographyTitle}>
            {i18n.t('core:clubs')}
          </Typography>
          <Typography variant="headline" component="p" className={classes.typographyOffset}>
            {i18n.t('core:clubsAdminRoleDescription')}
          </Typography>
        </div>
        <div className={classes.tableActionHeader}>
          <Button
            aria-label={i18n.t('core:clubsAdd')}
            color="primary"
            onClick={() => this.handleAddClub()}
          >
            {i18n.t('core:clubsAdd')}
          </Button>
          <Button
            aria-label={i18n.t('core:clubsUpdate')}
            color="primary"
            onClick={() => this.handleUpdateClub()}
          >
            Update Service
          </Button>
          <Button
            aria-label={i18n.t('core:clubsDelete')}
            color="primary"
            onClick={() => this.handleDeleteClub()}
          >
            {i18n.t('core:clubsDelete')}
          </Button>
        </div>
        <EnhancedTableToolbar numSelected={selected.length} />

        <div className={classes.tableWrapper}>
          <Table className={classes.table}>
            <TableHeadTable
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={clubs.length}
              columnData={clubsColumnData}
            />
            <TableBody>
              {clubs.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(service => {
                const isSelected = this.isSelected(service._id);
                return (
                  <TableRow
                    hover
                    onClick={() => this.handleClick(service)}
                    role="checkbox"
                    aria-checked={isSelected}
                    tabIndex={-1}
                    key={service._id}
                    selected={isSelected}
                  >
                    <TableCell padding="checkbox">
                      <Checkbox checked={isSelected} />
                    </TableCell>
                    <TableCell>{service._id}</TableCell>
                    <TableCell>{service.title}</TableCell>
                    <TableCell>{service.category ? service.category.name : ''}</TableCell>
                    <TableCell>{service.status}</TableCell>
                    <TableCell>{service.location}</TableCell>
                    <TableCell>{service.created}</TableCell>
                  </TableRow>
                );
              })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          component="div"
          count={clubs.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    );
  }
}

export default withStyles(tableStyles)(RetirementClubsTable);
