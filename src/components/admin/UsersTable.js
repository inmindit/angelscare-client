import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import classNames from 'classnames';
import Table, {
  TableBody,
  TableCell,
  TablePagination,
  TableRow,
} from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/Menu/MenuItem';
import Paper from 'material-ui/Paper';
import Button from 'material-ui/Button';
import Checkbox from 'material-ui/Checkbox';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import FilterListIcon from '@material-ui/icons/FilterList';
import TableHeadTable, { toolbarStyles } from './TableHeadTable';
import DeleteIcon from '@material-ui/icons/Delete';
import { tableStyles } from '../../assets/jss/configTableStyles';
import i18n from '../../services/i18n';

let EnhancedTableToolbar = props => {
  const { numSelected, classes } = props;

  return (
    <Toolbar
      className={classNames(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      <div className={classes.title}>
        {numSelected > 0 ? (
          <Typography color="inherit" variant="subheading">
            {numSelected} selected
          </Typography>
        ) : (
          <Typography variant="title">All Users</Typography>
        )}
      </div>
      <div className={classes.spacer} />
      <div className={classes.actions}>
        {numSelected > 0 ? (
          <Tooltip title="Delete">
            <IconButton aria-label="Delete">
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        ) : (
          <Tooltip title="Filter list">
            <IconButton aria-label="Filter list">
              <FilterListIcon />
            </IconButton>
          </Tooltip>
        )}
      </div>
    </Toolbar>
  );
};
EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

const userColumnData = [
  { id: 'id', numeric: false, label: 'User ID' },
  { id: 'name', numeric: false, label: 'Name' },
  { id: 'email', numeric: false, label: 'Email' },
  { id: 'username', numeric: false, label: 'Username' },
  { id: 'roles', numeric: false, label: 'Roles' },
  { id: 'setRole', numeric: false, label: 'Set Role' },
  { id: 'created', numeric: false, label: 'Created' },
];

type Props = {};

type State = {
  order: string,
  orderBy: string,
  selected: Array,
  users: Array<Object>,
  page: number,
  rowsPerPage: number
};

class UsersTable extends Component<Props, State> {
  constructor(props, context) {
    super(props, context);

    this.state = {
      order: 'asc',
      orderBy: 'calories',
      selected: [],
      users: [
        // { _id: 1, user: 'User 1', email: 'user1@email.com', location: 'Prague', roles: 'Agent', status: 'Registered' },
        // { _id: 2, user: 'User 2', email: 'user2@email.com', location: 'Berlin', roles: 'Agent', status: 'Registered' },
        // { _id: 3, user: 'User 3', email: 'user3@email.com', location: 'Frankfurt', roles: 'Agent', status: 'Registered' },
        // { _id: 4, user: 'User 4', email: 'user4@email.com', location: 'Sofia', roles: 'Agent', status: 'Invited' },
        // { _id: 5, user: 'User 5', email: 'user5@email.com', location: 'Prague', roles: 'Agent', status: 'Registered' },
      ],
      page: 0,
      rowsPerPage: 5,
      selectedRole: [],
      userRoles: [
        'user',
        'agent',
        'provider',
        'admin'
      ]
    };
  }

  componentWillReceiveProps(nextProps: any) {
    console.log('componentWillReceiveProps: ', nextProps);
    this.setState({
      users: nextProps.users
    });
  };

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    const users =
      order === 'desc'
        ? this.state.users.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
        : this.state.users.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

    this.setState({ users: users, order, orderBy });
  };

  handleSelectAllClick = (event, checked) => {
    if (checked) {
      this.setState({ selected: this.state.services.map(n => n._id) });
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  handleSetRoleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    // [name]: value
    this.setState({ selectedRole: value });
  };

  handleAddUser = () => {
    this.props.showAddUserDialog();
  };

  handleEditUser = () => {
    this.props.showEditUserDialog();
  };

  handleDeleteUsers = () => {
    this.props.showDeleteUserDialog();
  };

  handleInviteUser = () => {
    this.props.showInviteUserEmailDialog();
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  render() {
    const { classes } = this.props;
    const { users, order, orderBy, selected, rowsPerPage, page } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, users.length - page * rowsPerPage);

    return (
      <Paper className={classes.root} elevation={4}>
        <div className={classes.contentHeader}>
          <Typography variant="headline" component="h3" className={classes.typographyTitle}>
            {i18n.t('core:adminDashboard')}
          </Typography>
          <Typography variant="headline" component="p" className={classes.typographyOffset}>
            {i18n.t('core:adminDashboardDescription')}
          </Typography>
          <Typography variant="headline" component="p" className={classes.typographyOffset}>
            {i18n.t('core:adminRoleDescription')}
          </Typography>
          <Typography variant="headline" component="p" className={classes.typographyOffset}>
            {i18n.t('core:adminReportingDescription')}
          </Typography>
        </div>
        <div className={classes.tableActionHeader}>
          <Button
            aria-label={i18n.t('core:addUser')}
            color="primary"
            onClick={this.handleAddUser}
          >
            {i18n.t('core:addUser')}
          </Button>

          <Button
            aria-label={i18n.t('core:invite')}
            color="primary"
            onClick={this.handleInviteUser}
          >
            {i18n.t('core:invite')}
          </Button>

          <Button
            aria-label={i18n.t('core:updateUser')}
            color="primary"
            onClick={this.handleEditUser}
          >
            {i18n.t('core:updateUser')}
          </Button>

          <Button
            aria-label={i18n.t('core:deleteUsers')}
            color="primary"
            onClick={this.handleDeleteUsers}
          >
            {i18n.t('core:deleteUsers')}
          </Button>
        </div>
        <EnhancedTableToolbar numSelected={selected.length} />

        <div className={classes.tableWrapper}>
          <Table className={classes.table}>
            <TableHeadTable
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={users.length}
              columnData={userColumnData}
            />
            <TableBody>
              {users && users.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(user => {
                const isSelected = this.isSelected(user._id);
                return (
                  <TableRow
                    hover
                    onClick={event => this.handleClick(event, user._id)}
                    role="checkbox"
                    aria-checked={isSelected}
                    tabIndex={-1}
                    key={user._id}
                    selected={isSelected}
                  >
                    <TableCell padding="checkbox">
                      <Checkbox checked={isSelected} />
                    </TableCell>
                    <TableCell>{user._id}</TableCell>
                    <TableCell>{user.displayName}</TableCell>
                    <TableCell>{user.email}</TableCell>
                    <TableCell>{user.username}</TableCell>
                    <TableCell>
                      <TextField
                        fullWidth={true}
                        select
                        label="Select"
                        value={user.roles}
                        name="selectedRole"
                        data-tid="userTableSetUserRole"
                        onChange={this.handleSetRoleInputChange}
                      >
                        {this.state.userRoles.map(role => (
                          <MenuItem key={role} value={role}>
                            {role}
                           </MenuItem>
                        ))}
                       </TextField>
                    </TableCell>
                    <TableCell>{user.created}</TableCell>
                  </TableRow>
                );
              })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          component="div"
          count={users.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    );
  }
}

export default withStyles(tableStyles)(UsersTable);
