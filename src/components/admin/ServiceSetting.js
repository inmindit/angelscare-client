import React, { Component } from 'react';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import Chip from 'material-ui/Chip';
import AddIcon from '@material-ui/icons/Add';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/Menu/MenuItem';
import IconButton from 'material-ui/IconButton';
import Input, { InputLabel, InputAdornment } from 'material-ui/Input';
import { FormControl } from 'material-ui/Form';
import { withStyles } from 'material-ui/styles/index';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: '3%',
    marginBottom: "5%",
    marginLeft: "3%",
    justify: 'center',
    width: "91%",
    flexGrow: 1,
  }),
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  chip: {
    margin: theme.spacing.unit / 2,
  },
  contentHeader: {
    margin: '-40px 15px 30px',
    padding: '15px',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    fontWeight: 300,
    lineHeight: '1.5em',
    borderRadius: '3px',
    background: 'linear-gradient(60deg, #ff9800, #8e24aa)',
    boxShadow: '0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2)',
    color: '#fff'
  },
  typographyTitle: {
    color: '#fff'
  },
  typographyOffset: {
    marginTop: 5,
    fontSize: 15,
    color: '#fff'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
});

type Props = {
  fetchServiceLanguage: () => void,
  fetchServiceCategory: () => void,
  fetchServiceTypesByCategoryId: () => void,
  fetchServicesTypes: () => void,
  supportedServiceLanguages: Array<Object>,
};

type State = {
  isDeleteServiceDialogOpened: boolean,
  isAddServiceDialogOpened: boolean,
  isEditEventDialogOpened: boolean,
  serviceCategory: string,
  serviceType: string,
  serviceCategories: Array<Object>,
  serviceTypes: Array<Object>,
  selectedCategory: Object,
  selectedCategoryLanguage: Object,
  selectedCategoryLanguageTranslation: string,
  selectedType: Object,
  selectedTypeLanguage: Object,
  selectedTypeLanguageTranslation: string,
  serviceCategoryLanguage: string,
  serviceCategoryLanguages: Array<Object>,
  serviceTypeLanguage: string,
  serviceTypeLanguages: Array<Object>,
};

class ServiceSetting extends Component<Props, State> {
  constructor(props) {
    super(props);
  }

  state = {
    isDeleteServiceDialogOpened: false,
    isAddServiceDialogOpened: false,
    isEditServiceDialogOpened: false,
    serviceCategoryLanguage: '',
    serviceCategoryLanguages: [],
    serviceTypeLanguage: '',
    serviceTypeLanguages: [],
    serviceCategory: '',
    serviceCategories: [],
    serviceType: '',
    serviceTypes: [],
    selectedCategory: null || {},
    selectedCategoryLanguage: null || {},
    selectedCategoryLanguageTranslation: '',
    selectedType: null || {},
    selectedTypeLanguage: null || {},
    selectedTypeLanguageTranslation: ''
  };

  componentDidMount(nextProps) {
    // Fetch all services
    this.props.fetchServiceCategory('GET');
    this.props.fetchServiceLanguage('GET');
  }

  componentWillReceiveProps(nextProps: any) {
    if (nextProps.serviceCategories) {
      console.log("ADMIN SERVICES:", nextProps);
      this.setState({
        serviceCategories: nextProps.serviceCategories,
        supportedServiceLanguages: nextProps.supportedServiceLanguages
      });
      this.setServiceTypes(nextProps.serviceTypes);
    }
  };

  handleDeleteServiceCategory = (data) => {
    const serviceCategories = [...this.state.serviceCategories];
    const deleteServiceCategory = serviceCategories.indexOf(data);
    serviceCategories.splice(deleteServiceCategory, 1);
    this.props.fetchServiceCategory('DELETE', this.state.serviceCategories[deleteServiceCategory]);
    this.setState({ serviceCategories: serviceCategories });
  };

  handleDeleteServiceType = (data) => {
    const servicesTypes = [...this.state.serviceTypes];
    const deleteServiceTypes = servicesTypes.indexOf(data);
    servicesTypes.splice(deleteServiceTypes, 1);
    this.props.fetchServicesTypes('DELETE', this.state.serviceTypes[deleteServiceTypes]);
    this.setState({ serviceTypes: servicesTypes });
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  handleInputChangeSelectCategoryLanguage = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    const selectedCategory = this.state.selectedCategory;

    this.setState({
      selectedCategoryLanguage: {},
      selectedCategoryLanguageTranslation: '',
    });

    this.state.supportedServiceLanguages.map(lang => {
      if (lang.name === value) {
        console.log("LANGUAGE: ", lang);
        if (selectedCategory.translations.length > 0) {
          this.setState({
            [name]: lang.name,
            selectedCategoryLanguage: lang,
            selectedCategoryLanguageTranslation: '',
          });
          selectedCategory.translations.map((translation) => {
            if (translation.language._id === lang._id) {
              this.setState({
                [name]: translation.language.name,
                selectedCategoryLanguage: translation.language,
                selectedCategoryLanguageTranslation: translation.translation,
              });
            }
          });
        } else {
          this.setState({
            [name]: lang.name,
            selectedCategoryLanguage: lang,
            selectedCategoryLanguageTranslation: '',
          });
        }
      }
    });
  };

  handleInputChangeSelectTypeLanguage = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    const selectedType = this.state.selectedType;

    this.setState({
      selectedTypeLanguage: {},
      selectedTypeLanguageTranslation: '',
    });

    this.state.supportedServiceLanguages.map(lang => {
      if (lang.name === value) {
        console.log("LANGUAGE: ", lang);
        if (selectedType.translations.length > 0) {
          this.setState({
            [name]: lang.name,
            selectedTypeLanguage: lang,
            selectedTypeLanguageTranslation: '',
          });
          selectedType.translations.map((translation) => {
            if (translation.language._id === lang._id) {
              this.setState({
                [name]: translation.language.name,
                selectedTypeLanguage: translation.language,
                selectedTypeLanguageTranslation: translation.translation,
              });
            }
          });
        } else {
          this.setState({
            [name]: lang.name,
            selectedTypeLanguage: lang,
            selectedTypeLanguageTranslation: '',
          });
        }
      }
    });
  };

  handleInputChangeSelectCategory = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.state.serviceCategories.map(category => {
      if (category.name === value) {
        this.setState({
          [name]: value,
          selectedCategory: category,
          serviceTypes: category.types,
          serviceType: category.types.length > 0 ? category.types[0].name : '',
          selectedType: category.types.length > 0  ? category.types[0] : {},
        });
        if (category.translations.length > 0) {
          this.setState({
            serviceCategoryLanguage: category.translations[0].language.name,
            selectedCategoryLanguage: category.translations[0].language,
            selectedCategoryLanguageTranslation: category.translations[0].translation,
          });
        } else {
          this.setState({
            serviceCategoryLanguage: '',
            selectedCategoryLanguage: {},
            selectedCategoryLanguageTranslation: {},
          });
        }
      }
    });
  };

  handleInputChangeSelectType = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.state.serviceTypes.map(type => {
      if (type.name === value) {
        this.setState({
          [name]: value,
          selectedType: type,
          serviceType: type.name,
        });
        if (type.translations.length > 0) {
          this.setState({
            serviceTypeLanguage: type.translations[0].language.name || '',
            selectedTypeLanguage: type.translations[0].language,
            selectedTypeLanguageTranslation: type.translations[0].translation,
          });
        } else {
          this.setState({
            serviceTypeLanguage: '',
            selectedTypeLanguage: {},
            selectedTypeLanguageTranslation: {},
          });
        }
      }
    });
  };

  handleMouseDown = event => {
    event.preventDefault();
  };

  // handleSaveServicesSettings = (event) => {
  //   // event.preventDefault();
  //   this.props.fetchServicesTypes('POST', this.state.serviceTypes);
  // };

  onClickSetCategoryTranslation = () => {
    if (this.state.serviceCategoryLanguage.length > 0) {
      const selectedCategoryLanguageTranslation = this.state.selectedCategoryLanguageTranslation;
      const selectedCategory = this.state.selectedCategory;
      const selectedCategoryLanguage = this.state.selectedCategoryLanguage;
      const newTranslation = {
        language: selectedCategoryLanguage._id,
        translation: selectedCategoryLanguageTranslation
      };
      selectedCategory.translations.push(newTranslation);
      this.setState({
        // serviceCategory: serviceCategory,
        selectedCategory: selectedCategory
      });
      this.props.fetchCategoryTranslation('PUT', newTranslation, selectedCategory._id);
    }
  };

  onClickSetTypeTranslation = () => {
    if (this.state.serviceTypeLanguage.length > 0) {
      const selectedTypeLanguageTranslation = this.state.selectedTypeLanguageTranslation;
      const selectedType = this.state.selectedType;
      const selectedTypeLanguage = this.state.selectedTypeLanguage;
      const newTranslation = {
        language: selectedTypeLanguage._id,
        translation: selectedTypeLanguageTranslation
      };
      selectedType.translations.push(newTranslation);
      this.setState({
        // serviceType: serviceType,
        selectedType: selectedType
      });
      this.props.fetchTypeTranslation('PUT', newTranslation, selectedType._id);
    }
  };

  onClickAddServiceCategory = () => {
    if (this.state.serviceCategory.length > 0) {
      const serviceCategory = this.state.serviceCategory;
      const serviceCategories = this.state.serviceCategories;
      serviceCategories.push({
        name: serviceCategory,
        slug: serviceCategory.toLowerCase()
      });
      this.setState({
        // serviceCategory: serviceCategory,
        serviceCategories: serviceCategories
      });
      this.props.fetchServiceCategory('POST', {
        name: serviceCategory,
        slug: serviceCategory.toLowerCase()
      });
    }
  };

  onClickAddServiceType = () => {
    if (this.state.serviceType.length > 0) {
      const serviceType = this.state.serviceType;
      this.props.fetchServicesTypes('POST', [{
        category: this.state.selectedCategory._id,
        name: serviceType,
        slug: serviceType.toLowerCase()
      }]);
    }
  };

  setServiceTypes = (service) => {
    if (this.state.selectedCategory && service[0]) {
      service.map(category => {
        if (category._id === this.state.selectedCategory._id) {
          this.setState({
            serviceTypes: category.types
          });
        }
      });
    }
  };

  render() {
    console.log('ServiceSetting: ', this.state);
    const { classes } = this.props;

    return (
      <div>
        <Paper className={classes.root}>
          <div className={classes.contentHeader}>
            <Typography variant="headline" component="h3" className={classes.typographyTitle}>
              Service Settings
            </Typography>
            <Typography variant="headline" component="p" className={classes.typographyOffset}>
              Admin Role: Has access to all functionality in the UI
              and the ability to to configure the types of services in UI (Main Page and Create Service Page Settings to select a type of service).
            </Typography>
            <Typography variant="headline" component="p" className={classes.typographyOffset}>
              Up to 10 types of services can be added.
            </Typography>
          </div>

          <div>
            <Grid container spacing={24}>
              <Grid item xs={6}>
                <Paper className={classes.paper}>
                  <FormControl className={classes.margin} fullWidth={true}>
                    <InputLabel htmlFor="service-category">Add service category</InputLabel>
                    <Input
                      id="service-category"
                      type="text"
                      name="serviceCategory"
                      onChange={this.handleInputChange}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="Add service category"
                            onClick={this.onClickAddServiceCategory}
                            onMouseDown={this.handleMouseDown}
                          >
                            <AddIcon/>
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                  </FormControl>

                  {this.state.serviceCategories[0] && this.state.serviceCategories.map(data => {
                    let avatar = null;
                    return (
                      <Chip
                        key={data.name}
                        avatar={avatar}
                        label={data.name}
                        onDelete={() => this.handleDeleteServiceCategory(data) }
                        className={classes.chip}
                      />
                    );
                  })}
                </Paper>

                <Paper className={classes.paper}>
                  <FormControl fullWidth={true}>
                    <TextField
                      disabled={!this.state.serviceCategories}
                      id="selectServiceCategory"
                      select
                      label="Select an service category"
                      value={this.state.serviceCategory}
                      name="serviceCategory"
                      data-tid="selectServiceCategory"
                      onChange={this.handleInputChangeSelectCategory}
                      SelectProps={{
                        MenuProps: {
                          className: classes.menu,
                        },
                      }}
                      helperText="Please select your service category"
                      margin="normal"
                    >
                      {this.state.serviceCategories[0] && this.state.serviceCategories.map(option => (
                        <MenuItem key={option._id} value={option.name}>
                          {option.name}
                        </MenuItem>
                      ))}
                    </TextField>
                  </FormControl>
                </Paper>

                {this.state.serviceCategory.length > 0 && (
                  <Paper className={classes.paper}>
                    <FormControl className={classes.margin} fullWidth={true}>
                      <TextField
                        disabled={!this.state.serviceCategories}
                        id="selectCategoryLanguage"
                        select
                        label="Select supported language for Category Translation"
                        value={this.state.serviceCategoryLanguage}
                        name="serviceCategoryLanguage"
                        data-tid="serviceCategoryLanguage"
                        onChange={this.handleInputChangeSelectCategoryLanguage}
                        SelectProps={{
                          MenuProps: {
                            className: classes.menu,
                          },
                        }}
                        helperText="Please select your category Language"
                        margin="normal"
                      >
                        {this.state.supportedServiceLanguages && this.state.supportedServiceLanguages.map(option => (
                          <MenuItem key={option.tag} value={option.name}>
                            {option.name}
                          </MenuItem>
                        ))}
                      </TextField>
                    </FormControl>
                    {this.state.serviceCategoryLanguage.length > 0 && (
                      <FormControl className={classes.margin} fullWidth={true}>
                        <InputLabel htmlFor="service-category">Input translated name for the language</InputLabel>
                        <Input
                          id="service-category"
                          type="text"
                          name="selectedCategoryLanguageTranslation"
                          value={this.state.selectedCategoryLanguageTranslation}
                          onChange={this.handleInputChange}
                          endAdornment={
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="Add Category Translation"
                                onClick={this.onClickSetCategoryTranslation}
                                onMouseDown={this.handleMouseDown}
                              >
                                <AddIcon/>
                              </IconButton>
                            </InputAdornment>
                          }
                        />
                      </FormControl>
                    )}
                  </Paper>
                )}
              </Grid>

              {this.state.serviceCategory.length > 0 && (
                <Grid item xs={6}>
                  <Paper className={classes.paper}>
                    <FormControl className={classes.margin} fullWidth={true}>
                      <InputLabel htmlFor="service-type">Add service type</InputLabel>
                      <Input
                        id="service-type"
                        type="text"
                        className={classes.textField}
                        value={this.state.serviceType}
                        name="serviceType"
                        fullWidth={true}
                        onChange={this.handleInputChange}
                        endAdornment={
                          <InputAdornment position="end">
                            <IconButton
                              aria-label="Add service type"
                              onClick={this.onClickAddServiceType}
                              onMouseDown={this.handleMouseDown}
                            >
                              <AddIcon/>
                            </IconButton>
                          </InputAdornment>
                        }
                      />
                    </FormControl>

                    {this.state.serviceTypes && this.state.serviceTypes.length > 0 && this.state.serviceTypes.map(data => {
                      let avatar = null;
                      return (
                        <Chip
                          key={data.name}
                          avatar={avatar}
                          label={data.name}
                          onDelete={() => this.handleDeleteServiceType(data) }
                          className={classes.chip}
                        />
                      );
                    })}
                  </Paper>

                  {this.state.serviceTypes.length > 0 && (
                    <Paper className={classes.paper}>
                      <FormControl fullWidth={true}>
                        <TextField
                          disabled={!this.state.serviceTypes}
                          id="selectServiceType"
                          select
                          label="Select an service category"
                          value={this.state.serviceType}
                          name="serviceType"
                          data-tid="selectServiceType"
                          onChange={this.handleInputChangeSelectType}
                          SelectProps={{
                            MenuProps: {
                              className: classes.menu,
                            },
                          }}
                          helperText="Please select your service type"
                          margin="normal"
                        >
                          {this.state.serviceTypes[0] && this.state.serviceTypes.map(option => (
                            <MenuItem key={option._id} value={option.name}>
                              {option.name}
                            </MenuItem>
                          ))}
                        </TextField>
                      </FormControl>
                    </Paper>
                  )}

                  {this.state.serviceType.length > 0 && (
                    <Paper className={classes.paper}>
                      <FormControl className={classes.margin} fullWidth={true}>
                        <TextField
                          disabled={!this.state.serviceTypes}
                          id="selectTypeLanguage"
                          select
                          label="Select supported language for Type Translation"
                          value={this.state.serviceTypeLanguage}
                          name="serviceTypeLanguage"
                          data-tid="serviceTypeLanguage"
                          onChange={this.handleInputChangeSelectTypeLanguage}
                          SelectProps={{
                            MenuProps: {
                              className: classes.menu,
                            },
                          }}
                          helperText="Please select your Type Language"
                          margin="normal"
                        >
                          {this.state.supportedServiceLanguages && this.state.supportedServiceLanguages.map(option => (
                            <MenuItem key={option.tag} value={option.name}>
                              {option.name}
                            </MenuItem>
                          ))}
                        </TextField>
                      </FormControl>

                      {this.state.serviceTypeLanguage.length > 0 && (
                        <FormControl className={classes.margin} fullWidth={true}>
                          <InputLabel htmlFor="service-type-translation">Input translated name for the language</InputLabel>
                          <Input
                            id="service-type-translation"
                            type="text"
                            name="selectedTypeLanguageTranslation"
                            value={this.state.selectedTypeLanguageTranslation}
                            onChange={this.handleInputChange}
                            endAdornment={
                              <InputAdornment position="end">
                                <IconButton
                                  aria-label="Add Type Translation"
                                  onClick={this.onClickSetTypeTranslation}
                                  onMouseDown={this.handleMouseDown}
                                >
                                  <AddIcon/>
                                </IconButton>
                              </InputAdornment>
                            }
                          />
                        </FormControl>
                      )}
                    </Paper>
                  )}
                </Grid>
              )}
            </Grid>
          </div>
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(ServiceSetting);
