import React, { Component } from 'react';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import { convertFromRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
// import htmlToDraft from 'html-to-draftjs';

type Props = {
  content: string
};

type State = {};

class RichTextEditorComponent extends Component<Props, State> {
  state = {
    editorState: EditorState.createEmpty(),
    contentState: {}
  };

  componentWillReceiveProps(nextProps: any) {
    if (nextProps.content) {
      const content = {
        entityMap:{},
        blocks:[{
          key: 637,
          text: nextProps.content,
          depth:0,
          inlineStyleRanges:[],
          entityRanges:[],
          data:{}
        }]
      };
      const contentState = convertFromRaw(content);
      this.setState({
        contentState: contentState,
      });
    }
  };

  onEditorStateChange = (editorState) => {
    // this.setState({
    //   editorState,
    // });
    console.log('EditorTextEditorComponent:', editorState);
    this.setState({
      editorState,
    });
    this.props.setContent(draftToHtml(convertToRaw(editorState.getCurrentContent())));
  };


  onContentStateChange: Function = (contentState) => {
    this.setState({
      contentState,
    });
  };

  render() {
    const { editorState } = this.state;
    return (
      <div>
        <Editor
          editorState={editorState}
          wrapperClassName="demo-wrapper"
          editorClassName="demo-editor"
          onEditorStateChange={this.onEditorStateChange}
          onContentStateChange={this.onContentStateChange}
        />
      {/*<textarea
          disabled
          value={draftToHtml(convertToRaw(editorState.getCurrentContent()))}
        />*/}
      </div>
    );
  }
}

 export default RichTextEditorComponent;
