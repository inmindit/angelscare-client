import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import { FormControl, FormHelperText } from 'material-ui/Form';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import { translate } from 'react-i18next';
import { fetchSignIn } from '../../actions/auth';
import i18n from "../../services/i18n";

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3,
    marginLeft: "23%",
    justify: 'center',
    width: "50%"
  }),
  buttonAlign: {
    marginTop: 15,
    width: '100%'
  }
});

type Props = {
  isAuthenticated: boolean
};

type State = {
  username: string,
  password: string,
  errorUsername: boolean,
  errorPassword: boolean,
  disableConfirmButton: boolean,
};

class SignIn extends Component<Props, State> {
  constructor(props){
    super(props);
  }

  state = {
    errorUsername: false,
    errorPassword: false,
    inputError: false,
    disableConfirmButton: true,
    username:'',
    password:''
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    }, this.handleValidation());
  };

  handleValidation() {
    // const pathRegex = '^((\.\./|[a-zA-Z0-9_/\-\\])*\.[a-zA-Z0-9]+)$';
    // const nameRegex = '^[A-Z][-a-zA-Z]+$';
    if (this.state.username.length > 0) {
      this.setState({ errorUsername: false, disableConfirmButton: false });
    } else {
      this.setState({ errorUsername: true, disableConfirmButton: true });
    }

    if (this.state.password.length > 6) {
      this.setState({ errorPassword: false, disableConfirmButton: false });
    } else {
      this.setState({ errorPassword: true, disableConfirmButton: true });
    }
  }

  onConfirm = () => {
    this.props.loginUser({
      username: this.state.username,
      password: this.state.password
    });
  };

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div>
          <h4>{this.props.errorMessage}</h4>
        </div>
      );
    }
  }

  handleClick = (link, handle = false) => {
    this.props.router.push(link);
    if (handle) {
      this.handleClose();
    }
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Paper className={classes.root} elevation={4}>
          <Typography variant="headline" component="h3">
            {i18n.t('core:signUp')}
          </Typography>
          <FormControl
            fullWidth={true}
            error={this.state.errorUsername}
          >
            <TextField
              id="signInUsername"
              helperText="Enter your Username"
              name="username"
              label="Username"
              onChange = {this.handleInputChange}
              fullWidth
            />
            {this.state.errorUsername && <FormHelperText>Invalid username</FormHelperText>}
          </FormControl>
          <FormControl
            fullWidth={true}
            error={this.state.errorPassword}
          >
            <TextField
              id="signInPassword"
              type="password"
              helperText="Enter your Password"
              name="password"
              label="Password"
              onChange = {this.handleInputChange}
              fullWidth
            />
            {this.state.errorPassword && <FormHelperText>Your password is invalid. Requires special characters and 6 lengths.</FormHelperText>}
          </FormControl>
          <Typography component="p">
            <Button
              disabled={this.state.disableConfirmButton}
              id="signInConfirm"
              variant="raised"
              color="primary"
              style={{ marginTop: 15, width: "100%" }}
              onClick={() => {this.onConfirm()}}
            >
              {i18n.t('core:login')}
            </Button>
            <Button
              id="signInForgotPassword"
              variant="raised"
              color="primary"
              style={{marginTop: 15, width: "100%"}}
              onClick={() => {this.handleClick('/forgot-password')}}
            >
              {i18n.t('core:forgottenPassword')}
            </Button>
            <Button
              id="signInRegister"
              variant="raised"
              color="primary"
              style={{ marginTop: 15, width: "100%" }}
              onClick={() => {this.handleClick('/register')}}
            >
              {i18n.t('core:register')}
            </Button>
            {this.renderAlert()}
            </Typography>
        </Paper>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error,
    message: state.auth.message,
    isAuthenticated: state.auth.isAuthenticated,
  };
}

export default withRouter(connect(mapStateToProps, { loginUser: fetchSignIn })(withStyles(styles)(translate(['core'], { wait: true })(SignIn))));
