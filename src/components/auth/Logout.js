import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import * as actions from '../../actions/auth';
import i18n from "../../services/i18n";

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3,
    marginLeft: "25%",
    width: "50%"
  }),
});

class Logout extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.fetchLogout();
    }, 500);
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Paper className={classes.root} elevation={4}>
          <Typography variant="headline" component="h3">
            {i18n.t('core:logoutHeader')}
          </Typography>
          <Typography component="p">
            {i18n.t('core:logoutDescription')}
          </Typography>
        </Paper>
      </div>
    );
  }
}

export default connect(null, actions)(withStyles(styles)(Logout));
