import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from 'material-ui/styles';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import { translate } from 'react-i18next';
import { fetchGetForgotPasswordToken } from '../../actions/auth';
import i18n from "../../services/i18n";

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3,
    marginLeft: "23%",
    width: "50%"
  }),
});

type Props = {
  isAuthenticated: boolean
};

type State = {
  emailAddress: string
};

class ForgotPassword extends Component<Props, State> {
  constructor(props) {
    super(props);
  }

  state = {
    errorEmailAddress: false,
    emailAddress: ''
  };

  componentDidMount() {
    if (this.props.isAuthenticated) {
      this.context.router.push('/dashboard');
    }
  }

  componentDidUpdate(nextProps) {
    if (nextProps.isAuthenticated) {
      this.context.router.push('/dashboard');
    }
  }

  onConfirm() {
    this.props.getForgotPasswordToken(this.state);
  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div>
          <h4><strong>{i18n.t('core:error')}!</strong> {this.props.errorMessage}</h4>
        </div>
      );
    }
  }

  render() {
    const {classes} = this.props;

    return (
      <div className={classes.root}>
        <Paper className={classes.root} elevation={4}>
          <Typography variant="headline" component="h3">
            {i18n.t('core:resetPassword')}
          </Typography>
          <Typography component="p">
            <FormControl
              fullWidth={true}
              error={this.state.errorEmailAddress}
            >
              <TextField
                helperText={i18n.t('core:profileEmailAddressDescription')}
                label={i18n.t('core:email')}
                name="emailAddress"
                onChange = {this.handleInputChange}
                fullWidth
              />
              {this.state.errorEmailAddress && <FormHelperText>Invalid email address</FormHelperText>}
            </FormControl>
            <Button
              variant="raised"
              color="primary"
              style={{marginTop: 15, width: "100%"}}
              onClick={() => this.onConfirm()}
            >
              {i18n.t('core:resetPassword')}
            </Button>
            {this.renderAlert()}
          </Typography>
        </Paper>
      </div>
    );
  };
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error,
    message: state.auth.message,
    isAuthenticated: state.auth.isAuthenticated,
  };
}

export default connect(mapStateToProps, { getForgotPasswordToken: fetchGetForgotPasswordToken })(withStyles(styles)(translate(['core'], { wait: true })(ForgotPassword)));
