import React, { Component } from 'react';
import { connect } from 'react-redux';

type Props = {
  router: Object,
  isAuthenticated: boolean
};

type State = {};

export default function (ComposedComponent) {
  class Authentication extends Component<Props, State> {
    componentDidMount() {
      if (!this.props.isAuthenticated) {
        this.props.router.push('/login');
      }
    }

    componentDidUpdate(nextProps) {
      if (!nextProps.isAuthenticated) {
        this.props.router.push('/login');
      }
    }

    render() {
      return <ComposedComponent {...this.props} />;
    }
  }

  function mapStateToProps(state) {
    return { isAuthenticated: state.auth.isAuthenticated };
  }

  return connect(mapStateToProps)(Authentication);
}
