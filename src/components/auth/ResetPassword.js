import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import { FormControl } from 'material-ui/Form';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import { translate } from 'react-i18next';
import { fetchResetPassword } from '../../actions/auth';
import i18n from '../../services/i18n';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3,
    marginLeft: "25%",
    width: "50%"
  }),
});

// function validate(formProps) {
//   const errors = {};
//
//   if (!formProps.password) {
//     errors.password = 'Please enter a new password';
//   }
//
//   if (!formProps.passwordConfirm) {
//     errors.passwordConfirm = 'Please confirm new password';
//   }
//
//   if (formProps.password !== formProps.passwordConfirm) {
//     errors.password = 'Passwords must match';
//   }
//
//   return errors;
// }

// const renderField = field => (
//   <div>
//     <input className="form-control" {...field.input} />
//     {field.touched && field.error && <div className="error">{field.error}</div>}
//   </div>
// );

type Props = {
  isAuthenticated: boolean
};

type State = {
  password: string,
  passwordConfirm: string,
  errorPassword: boolean,
  errorConfirmPassword: boolean
};

class ResetPassword extends Component<Props, State> {
  constructor(props){
    super(props);
  }

  state = {
    errorPassword: false,
    errorConfirmPassword: false,
    password: '',
    passwordConfirm: ''
  };

  componentDidMount() {
    if (this.props.isAuthenticated) {
      this.context.router.push('/dashboard');
    }
  }

  componentDidUpdate(nextProps) {
    if (nextProps.isAuthenticated) {
      this.context.router.push('/dashboard');
    }
  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    }); // , this.handleValidation());
  };

  handleValidation() {
    // const pathRegex = '^((\.\./|[a-zA-Z0-9_/\-\\])*\.[a-zA-Z0-9]+)$';
    // const nameRegex = '^[A-Z][-a-zA-Z]+$';
    // if (this.state.password.length > 0) {
    //   this.setState({ errorPassword: false, disableConfirmButton: false });
    // } else {
    //   this.setState({ errorPassword: true, disableConfirmButton: true });
    // }
    //
    // if (this.state.passwordConfirm === this.state.password) {
    //   this.setState({ errorConfirmPassword: false, disableConfirmButton: false });
    // } else {
    //   this.setState({ errorConfirmPassword: true, disableConfirmButton: true });
    // }
  }

  onConfirm({ password }) {
    const resetToken = this.props.params.resetToken;
    this.props.resetPassword(resetToken, { password });
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.errorMessage}
        </div>
      );
    } else if (this.props.message) {
      return (
        <div className="alert alert-success">
          <strong>{i18n.t('core:success')}!</strong> {this.props.message}
        </div>
      );
    }
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Paper className={classes.root} elevation={4}>
          <Typography variant="headline" component="h3">
            {i18n.t('core:resetPasswordHeader')}
          </Typography>
          <Typography component="p">
            <FormControl
              fullWidth={true}
              error={this.state.errorPassword}
            >
              <TextField
                helperText={i18n.t('core:passwordHelperText')}
                label={i18n.t('core:password')}
                name="password"
                onChange = {this.handleInputChange}
                fullWidth
              />
            </FormControl>
            <FormControl
              fullWidth={true}
              error={this.state.errorConfirmPassword}
            >
              <TextField
                helperText={i18n.t('core:profileConfirmPasswordDescription')}
                label={i18n.t('core:profileConfirmPasswordLabel')}
                name="passwordConfirm"
                onChange = {this.handleInputChange}
                type="password"
                fullWidth
              />
            </FormControl>
            <Button
              variant="raised"
              color="primary"
              style={{marginTop: 15, width: "100%"}}
              onClick={(e) => this.onConfirm(e)}
            >
              {i18n.t('core:changePassword')}
            </Button>
            {this.renderAlert()}
          </Typography>
        </Paper>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { errorMessage: state.auth.error, message: state.auth.resetMessage };
}

export default connect(mapStateToProps, { resetPassword: fetchResetPassword })(translate(['core'], { wait: true })(withStyles(styles)(ResetPassword)));
