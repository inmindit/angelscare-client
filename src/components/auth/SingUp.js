import React, {Component} from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import { FormControl, FormHelperText } from 'material-ui/Form';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import { fetchSignUp } from '../../actions/auth';
import i18n from '../../services/i18n';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3,
    marginLeft: "23%",
    width: "50%"
  }),
  buttonAlign: {
    marginTop: 15,
    width: '100%'
  }
});

type Props = {
  errorMessage: string,
  isAuthenticated: boolean
};

type State = {
  firstName: string,
  lastName: string,
  email: string,
  username: string,
  password: string,
  errorFirstName: boolean,
  errorLastName: boolean,
  errorUsername: boolean,
  errorPassword: boolean,
  errorEmailAddress: boolean,
};

class SignUp extends Component<Props, State> {
  constructor(props) {
    super(props);
  }

  state = {
    firstName: '',
    lastName: '',
    emailAddress: '',
    username: '',
    password: '',
    errorFirstName: false,
    errorLastName: false,
    errorUsername: false,
    errorPassword: false,
    errorEmailAddress: false,
  };

  handleRegister = () => {
    this.props.registerUser({
      email: this.state.emailAddress,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      username: this.state.username,
      password: this.state.password,
    });
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    }, this.handleValidation());
  };

  handleValidation() {
    // const pathRegex = '^((\.\./|[a-zA-Z0-9_/\-\\])*\.[a-zA-Z0-9]+)$';
    // const nameRegex = '^[A-Z][-a-zA-Z]+$';
    if (this.state.username.length > 0) {
      this.setState({ errorUsername: false, disableConfirmButton: false });
    } else {
      this.setState({ errorUsername: true, disableConfirmButton: true });
    }

    if (this.state.password.length > 6) {
      this.setState({ errorPassword: false, disableConfirmButton: false });
    } else {
      this.setState({ errorPassword: true, disableConfirmButton: true });
    }
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div>
          <span>{this.props.errorMessage}</span>
        </div>
      );
    }
  }

  handleClick = (link, handle = false) => {
    this.props.router.push(link);
    if (handle) {
      this.handleClose();
    }
  };

  render() {
    const {classes} = this.props;

    return (
      <div className={classes.root}>
        <Paper className={classes.root} elevation={4}>
          <Typography variant="headline" component="h3">
            {i18n.t('core:signUp')}
          </Typography>
          <FormControl
            fullWidth={true}
            error={this.state.errorFirstName}
          >
            <TextField
              helperText={i18n.t('core:helperTextFirstName')}
              label={i18n.t('core:firstName')}
              name="firstName"
              onChange = {this.handleInputChange}
              fullWidth
            />
            {this.state.errorFirstName && <FormHelperText>Invalid first name.</FormHelperText>}
          </FormControl>
          <FormControl
            fullWidth={true}
            error={this.state.errorLastName}
          >
            <TextField
              helperText={i18n.t('core:helperTextLastName')}
              label={i18n.t('core:lastName')}
              name="lastName"
              onChange = {this.handleInputChange}
              fullWidth
            />
            {this.state.errorLastName && <FormHelperText>Invalid last name.</FormHelperText>}
          </FormControl>
          <FormControl
            fullWidth={true}
            error={this.state.errorUsername}
          >
            <TextField
              helperText={i18n.t('core:createUserDescription')}
              label={i18n.t('core:username')}
              name="username"
              onChange = {this.handleInputChange}
              fullWidth
            />
            {this.state.errorUsername && <FormHelperText>Invalid username</FormHelperText>}
          </FormControl>
          <FormControl
            fullWidth={true}
            error={this.state.errorEmailAddress}
          >
            <TextField
              helperText={i18n.t('core:helperTextEmail')}
              label={i18n.t('core:profileEmailAddressLabel')}
              name="emailAddress"
              onChange = {this.handleInputChange}
              fullWidth
            />
            {this.state.errorEmailAddress && <FormHelperText>Invalid email address.</FormHelperText>}
          </FormControl>
          <FormControl
            fullWidth={true}
            error={this.state.errorPassword}
          >
            <TextField
              type="password"
              helperText={i18n.t('core:profilePasswordDescription')}
              label={i18n.t('core:profilePasswordLabel')}
              name="password"
              onChange = {this.handleInputChange}
            />
            {this.state.errorPassword && <FormHelperText>Your password is invalid. Requires special characters and 6 lengths.</FormHelperText>}
          </FormControl>
          <Typography component="p">
            <Button
              variant="raised"
              color="primary"
              style={{marginTop: 15, width: "100%"}}
              onClick={this.handleRegister}
            >
              {i18n.t('core:signUp')}
            </Button>
            <Button
              variant="raised"
              color="primary"
              style={{marginTop: 15, width: "100%"}}
              onClick={() => {this.handleClick('/login')}}
            >
              {i18n.t('core:signIn')}
            </Button>
            <Button
              variant="raised"
              color="primary"
              style={{marginTop: 15, width: "100%"}}
              onClick={() => {this.handleClick('/forgot-password')}}
            >
              {i18n.t('core:forgottenPassword')}
            </Button>
            {this.renderAlert()}
          </Typography>
        </Paper>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error,
    message: state.auth.message,
    isAuthenticated: state.auth.isAuthenticated,
  };
}

export default withRouter(connect(mapStateToProps, { registerUser: fetchSignUp })(withStyles(styles)(SignUp)));
