import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import {
  FacebookShareButton,
  GooglePlusShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  // TelegramShareButton,
  // WhatsappShareButton,
  // PinterestShareButton,
  // VKShareButton,
  // OKShareButton,
  // RedditShareButton,
  // TumblrShareButton,
  // LivejournalShareButton,
  // ViberShareButton,
  EmailShareButton,
} from 'react-share';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    display: 'inline-block'
  },
  socialButton: {
    display: 'inline-block'
  }
});

class SocialIconsComponent extends Component<Props, State> {
  state = {};

  render() {
    const { classes } = this.props;

    return (
      <div>
        <FacebookShareButton
          className={classes.socialButton}
          url={'http://dev.ageweb.care'}
          image={'http://dev.ageweb.care/api/static/uploads/news/news-1529670007188.jpg'}
        >
          <IconButton color="primary" className={classes.button} aria-label="google+" >
            <img src="/src/public/assets/icons/facebook-box.png" height="24" />
          </IconButton>
        </FacebookShareButton>

        <GooglePlusShareButton
          className={classes.socialButton}
          url={'http://localhost:8080/api/static/uploads/news/news-1529607317658.png'}
        >
          <IconButton color="primary" className={classes.button} aria-label="google+" >
            <img src="/src/public/assets/icons/google-plus.png" height="24" />
          </IconButton>
        </GooglePlusShareButton>

        <LinkedinShareButton
          className={classes.socialButton}
          url={'http://localhost:8080/api/static/uploads/news/news-1529607317658.png'}
        >
          <IconButton color="primary" className={classes.button} aria-label="linkedin">
            <img src="/src/public/assets/icons/linkedin-box.png" height="24" />
          </IconButton>
        </LinkedinShareButton>

        <TwitterShareButton
          className={classes.socialButton}
          url={'http://localhost:8080/api/static/uploads/news/news-1529607317658.png'}
        >
          <IconButton color="primary" className={classes.button} aria-label="twitter">
            <img src="/src/public/assets/icons/twitter-box.png" height="24" />
          </IconButton>
        </TwitterShareButton>

        <EmailShareButton
          className={classes.socialButton}
          url={'http://localhost:8080/api/static/uploads/news/news-1529607317658.png'}
        >
          <IconButton color="primary" className={classes.button} aria-label="linkedin">
            <img src="/src/public/assets/icons/email.png" height="24" />
          </IconButton>
        </EmailShareButton>
      </div>
    )
  }
}

export default  withStyles(styles)(SocialIconsComponent);
