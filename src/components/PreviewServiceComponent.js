import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import GoogleMapsComponent from '../components/GoogleMapsComponent';
import SocialIconsComponent from '../components/SocialIconsComponent';
import i18n from '../services/i18n';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: '3%',
    marginBottom: "5%",
    marginLeft: "3%",
    justify: 'center',
    width: "91%"
  }),
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  buttonAlign: {
    marginTop: 15,
    width: '100%'
  },
  bookButton: {
    marginTop: 15
  },
  infiniteCalendar: {
    marginTop: 25,
    marginBottom: 25
  },
  input: {
    display: 'none',
  },
  contentHeader: {
    margin: '-40px 15px 30px',
    padding: '15px',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    fontWeight: 300,
    lineHeight: '1.5em',
    borderRadius: '3px',
    background: 'linear-gradient(60deg, #ff9800, #8e24aa)',
    boxShadow: '0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2)',
    color: '#fff'
  },
  typographyTitle: {
    color: '#fff'
  },
  typographyOffset: {
    marginTop: 5,
    fontSize: 15,
    color: '#fff'
  },
  title: {
    width: '90%',
    marginLeft: '5%',
    color: '#fc3752',
    fontFamily: 'Avalon,sans-serif',
    fontWeight: 700,
    fontSize: '28px',
    letterSpacing: '-.03em',
    lineHeight: 1,
    padding: '15px',
    paddingLeft: 0,
  },
  description: {
    width: '90%',
    marginLeft: '5%',
    lineHeight: '1.8',
    marginBottom: '1rem',
    fontSize: '1.0rem',
    fontFamily: '"Montserrat", "Helvetica Neue", Arial, sans-serif'
  },
  category: {
    color: '#fc3752',
    width: '90%',
    marginLeft: '5%',
    fontSize: '15px',
    padding: '15px',
    paddingLeft: 0,
  },
  currentDate: {
    marginLeft: '80%',
    marginTop: '-12%',
    fontWeight: 450,
    textTransform: 'uppercase',
    textDecoration: 'none',
    padding: '1%',
    background: '#fc3752',
    color: '#fefefe',
    width: '4em',
    height: '4em',
    borderRadius: '3em',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    opacity: '0.9',
    margin: '1em',
  },
  currentPrice: {
    width: '90%',
    marginLeft: '5%',
    color: '#fc3752',
  },
  currentLocation: {
    color: '#fc3752',
    width: '90%',
    marginLeft: '5%',
  },
  created: {
    fontSize: '13px',
    // color: '#fc3752',
    width: '90%',
    marginLeft: '5%',
    marginBottom: '15px'
  },
  hours: {
    fontSize: '17px',
    color: '#fc3752',
    width: '90%',
    marginLeft: '5%',
  },
  date: {
    color: '#fc3752',
    fontSize: '17px',
    marginLeft: '10%',
    marginBottom: '15px'
  }
});

type Props = {
  orderButton: boolean
};

type State = {};

class PreviewServiceComponent extends Component<Props, State> {
  state = {};

  // componentDidMount (nextProps, nextState) {
  //   // Fetch all events
  //   this.props.fetchServiceCategory('GET');
  // }

  currentDate = (date) => {
    const currentDate = new Date(date).getDate();
    const months = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'August', 'Sept', 'Oct', 'Nov', 'Dec'];
    const month = new Date(date).getMonth();

    return currentDate + " " + months[month]
  };

  render() {
    const { classes, service } = this.props;
    console.log('Preview Service Component - Props:', this.props);


    return (
      <div data-tid="PreviewServiceComponent">
        <Paper className={classes.root} elevation={4}>
          <div className={classes.contentHeader}>
            <Typography variant="headline" component="h3" className={classes.typographyTitle}>
              {i18n.t('core:servicePreview') + ' > ' + service.title}
            </Typography>
          </div>

          <Grid container spacing={40}>
            <Grid item xs={12} sm={12}>

              <div>
                <img
                  className="imgPreview"
                  src={service.imagePreviewURL ? service.imagePreviewURL : service.image || '/src/public/assets/img/image_placeholder.jpg'}
                  width="94%"
                />
              </div>
              <SocialIconsComponent />

              <Grid container xs={12} sm={12}>
                <Grid item xs={12} sm={6}>
                  <Typography variant="headline" component="h3" className={classes.title}>
                    {service.title}
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={6}>
                  {this.props.isAuthenticated && this.props.orderButton && (
                    <Typography component="p" style={{ float: 'right', marginRight: '6%' }}>
                      <Button
                        variant="raised"
                        color="primary"
                        className={classes.bookButton}
                        onClick={() => this.props.handleOrderRequest()}
                      >
                        {i18n.t('core:book')}
                      </Button>
                    </Typography>
                  )}
                </Grid>
                <Grid item xs={12} sm={6}>
                  {service.date && (
                    <Typography variant="headline" component="p" className={classes.currentDate}>
                      {this.currentDate(service.date)}
                    </Typography>
                  )}
                 </Grid>
              </Grid>

              <Typography variant="headline" component="p" className={classes.category}>
                {service.category ? service.category.name + ' > ' + service.type.name : 'Other'}
              </Typography>

              <Typography variant="headline" component="h3">
                <Grid item xs={12} sm={6}>
                  {service.price && (
                    <Typography variant="headline" component="p" className={classes.currentPrice}>
                      {service.currency + ' ' + service.price}
                    </Typography>
                  )}
                </Grid>
              </Typography>

              <Typography variant="headline" component="h3">
                <Grid item xs={12} sm={6}>
                  {service.startDate && (
                    <Typography variant="headline" component="p" className={classes.date}>
                      {i18n.t('core:startAt') + ': ' + this.currentDate(service.startDate)}
                    </Typography>
                  )}

                  {service.endDate && (
                    <Typography variant="headline" component="p" className={classes.date}>
                      {i18n.t('core:endAt') + ': ' + this.currentDate(service.endDate)}
                    </Typography>
                  )}
                </Grid>

                <Grid item xs={12} sm={6}>
                  {service.startHours && (
                    <Typography variant="headline" component="p" className={classes.hours}>
                      {i18n.t('core:startHours') + ': ' + this.currentDate(service.startHours)}
                    </Typography>
                  )}

                  {service.endHours && (
                    <Typography variant="headline" component="p" className={classes.hours}>
                      {i18n.t('core:endHours') + ': ' + this.currentDate(service.endHours)}
                    </Typography>
                  )}
                </Grid>
              </Typography>

              {service.description && (
                <Typography variant="headline" component="p" className={classes.description}>
                  {service.description}
                </Typography>
              )}

              {service.content && (
                <Typography
                  dangerouslySetInnerHTML={{ __html: service.content }}
                  className={classes.currentLocation}
                />
              )}

              {service.created && (
                <Typography variant="headline" component="p" className={classes.created}>
                  {i18n.t('core:created') + ': ' + this.currentDate(service.created)}
                </Typography>
              )}

              {service.location && (
                <Typography variant="headline" component="p" className={classes.currentLocation}>
                  {i18n.t('core:location') + ': ' + service.location}
                </Typography>
              )}
            </Grid>

            <Grid item xs={12} sm={12}>
              <GoogleMapsComponent
                latitude={parseFloat(service.place.latitude)}
                longitude={parseFloat(service.place.longitude)}
              />
            </Grid>
          </Grid>
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(PreviewServiceComponent);
