// @flow
import React from 'react';
import Button from 'material-ui/Button';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogTitle,
  DialogContentText,
} from 'material-ui/Dialog';
import Slide from 'material-ui/transitions/Slide';
// import i18n from '../../services/i18n';

function Transition(props) {
  return <Slide direction="down" {...props} />;
}

// type Props = {
//   open: boolean,
//   fullScreen: boolean,
//   onClose: () => void
// };
//
// type State = {};

class GenericDialog extends React.Component{
  onConfirm = () => { // should be overwritten in the children classes
    this.props.onClose();
  };

  renderTitle = () => (
    <DialogTitle>Dialog</DialogTitle>
  );

  renderContent = () => (
    <DialogContent>
      <DialogContentText>
        Some content
      </DialogContentText>
    </DialogContent>
  );

  renderActions = () => (
    <DialogActions>
      <Button onClick={this.props.onClose} color="primary">
        {/*{i18n.t('core:cancel')}*/}
        Cancel
      </Button>
    </DialogActions>
  );

  handleKeyPress = (event: any) => {
    if (event.key === 'Enter' || event.keyCode === 13) {
      this.onConfirm();
    }
  };

  render() {
    return (
      <Dialog
        open={this.props.open ? this.props.open : false}
        transition={Transition}
        keepMounted
        fullScreen={this.props.fullScreen}
        onClose={this.props.onClose}
        onKeyDown={this.handleKeyPress}
      >
        {this.renderTitle()}
        {this.renderContent()}
        {this.renderActions()}
      </Dialog>
    );
  }
}

export default GenericDialog;
