import React from 'react';
import Button from 'material-ui/Button';
import {
  DialogActions,
  DialogContent,
  DialogTitle,
} from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import { FormControl } from 'material-ui/Form';
import { withStyles } from 'material-ui/styles/index';
import GenericDialog from './GenericDialog';
import { styles } from './configStyle';

type Props = {
  classes: Object,
  open: boolean,
  onClose: () => void
};

type State = {
  errorEmailAddress: boolean,
  email: string,
  firstName: string,
  lastName: string,
  username: string,
};

class InviteUserDialog extends GenericDialog<Props, State> {
  state = {
    firstName: '',
    lastName: '',
    username: '',
    email: '',
    errorEmailAddress: false,
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    }, this.handleValidation());
  };

  handleValidation() {
    // const pathRegex = '^((\.\./|[a-zA-Z0-9_/\-\\])*\.[a-zA-Z0-9]+)$';
    // const nameRegex = '^[A-Z][-a-zA-Z]+$';

    if (this.state.email.length > 6) {
      this.setState({ errorEmailAddress: false, disableConfirmButton: false });
    } else {
      this.setState({ errorEmailAddress: true, disableConfirmButton: true });
    }
  }

  onConfirm = () => {
    this.props.inviteUser({
      fistName: this.state.fistName,
      lastName: this.state.lastName,
      username: this.state.username,
      email: this.state.email
    });
    this.props.onClose();
  };

  renderTitle = () => (
    <DialogTitle>Invite user by email</DialogTitle>
  );

  renderContent = () => (
    <DialogContent className={this.props.classes.root}>
      <FormControl
        fullWidth={true}
      >
        <TextField
          helperText="First Name"
          label="First name"
          name="firstName"
          onChange={this.handleInputChange}
          fullWidth
        />
      </FormControl>

      <FormControl
        fullWidth={true}
      >
        <TextField
          helperText="Last Name"
          label="Last Name"
          name="lastName"
          onChange={this.handleInputChange}
          fullWidth
        />
      </FormControl>

      <FormControl
        fullWidth={true}
      >
        <TextField
          helperText="Username"
          label="Username"
          name="username"
          onChange={this.handleInputChange}
          fullWidth
        />
      </FormControl>

      <FormControl
        fullWidth={true}
        error={this.state.errorEmailAddress}
      >
        <TextField
          helperText="Enter Email Address"
          label="Email Address"
          name="email"
          onChange={this.handleInputChange}
          fullWidth
        />
        {/*{this.state.errorEmailAddress && <FormHelperText>Invalid email address.</FormHelperText>}*/}
      </FormControl>

    </DialogContent>
  );

  renderActions = () => (
    <DialogActions>
      <Button
        data-tid="closeInviteUserDialog"
        onClick={this.props.onClose}
      >
        Cancel
      </Button>
      <Button
        // disabled={this.state.disableConfirmButton}
        onClick={this.onConfirm}
        data-tid="confirmInviteUserDialog"
        id="confirmInviteUserDialog"
        color="primary"
      >
        Invite
      </Button>
    </DialogActions>
  );
}

export default withStyles(styles, {withTheme: true })(InviteUserDialog);
