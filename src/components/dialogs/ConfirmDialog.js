import React from 'react';
import Button from 'material-ui/Button';
import {
  DialogActions,
  DialogContent,
  DialogTitle,
  DialogContentText
} from 'material-ui/Dialog';
import { withStyles } from 'material-ui/styles/index';
import GenericDialog from './GenericDialog';
import i18n from "../../services/i18n";
// import i18n from '../../services/i18n';

const styles = theme => ({
  root: {
    width: 550,
    height: '100%',
    overflowY: 'overlay',
    marginBottom: 30
  }
});

type Props = {
  list: Array<string>
};

class ConfirmDialog extends GenericDialog<Props> {
  onConfirm = (result) => {
    this.props.confirmCallback(result);
    this.props.onClose();
  };

  renderTitle = () => (
    <DialogTitle>{this.props.title}</DialogTitle>
  );

  renderContent = () => (
    <DialogContent>
      <DialogContentText data-tid={this.props.confirmDialogContent}>
        {this.props.content}
        {this.props.list && this.props.list.map((listItem) => <div key={listItem}>{listItem}</div>)}
      </DialogContentText>
    </DialogContent>
  );

  renderActions = () => (
    <DialogActions>
      <Button
        onClick={() => this.onConfirm(false)}
        color="primary"
        data-tid={this.props.cancelDialogActionId}
      >
        {/*{i18n.t('core:no')}*/}
        {i18n.t('core:no')}
      </Button>
      <Button
        data-tid={this.props.confirmDialogActionId}
        onClick={() => this.onConfirm(true)}
        color="primary"
      >
        {/*{i18n.t('core:yes')}*/}
        {i18n.t('core:Yes')}
      </Button>
    </DialogActions>
  );
}

export default withStyles(styles)(ConfirmDialog);
