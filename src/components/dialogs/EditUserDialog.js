import React from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import { FormControl, FormHelperText } from 'material-ui/Form';
import {
DialogActions,
DialogContent,
DialogTitle,
// DialogContentText,
} from 'material-ui/Dialog';
import { withStyles } from 'material-ui/styles/index';
import GenericDialog from './GenericDialog';
import styles from './configStyle';

type Props = {
  classes: Object,
  open: boolean,
  onClose: () => void
};

type State = {
  username: string,
  email: string
};

class EditUserDialog extends GenericDialog<Props, State> {
  state = {
    username: '',
    email: '',
    errorUsername: false,
    errorEmailAddress: false,
  };

  onConfirm = () => {
    // console.log(this.props);
    // this.props.handleBooking();
  };

  // componentWillReceiveProps = (nextProps: any) => {
  //   if (nextProps.open === true) {
  //     this.setState({
  //       selectedEvent: nextProps.selectedEvent || ''
  //     });
  //   }
  // };

  renderTitle = () => (
    <DialogTitle>Create user</DialogTitle>
  );

  renderContent = () => (
    <DialogContent className={this.props.classes.root}>
      <FormControl
        fullWidth={true}
        error={this.state.errorUsername}
      >
        <TextField
          helperText="Enter your username"
          label="Username"
          name="username"
          onChange = {this.handleInputChange}
          fullWidth
        />
        {/*{this.state.errorUsername && <FormHelperText>Invalid username</FormHelperText>}*/}
      </FormControl>
      <FormControl
        fullWidth={true}
        // error={this.state.errorEmailAddress}
      >
        <TextField
          helperText="Enter Email Address"
          label="Email Address"
          name="emailAddress"
          onChange = {this.handleInputChange}
          fullWidth
        />
        {/*{this.state.errorEmailAddress && <FormHelperText>Invalid email address.</FormHelperText>}*/}
      </FormControl>
    </DialogContent>
  );

  renderActions = () => (
    <DialogActions>
      <Button
        data-tid="closeEditUserDialog"
        onClick={this.props.onClose}
      >
        Cancel
      </Button>
      <Button
        // disabled={this.state.disableConfirmButton}
        onClick={this.onConfirm}
        data-tid="confirmEditUserDialog"
        id="confirmEditUserDialog"
        color="primary"
      >
        Create
      </Button>
    </DialogActions>
  );
}

export default withStyles(styles, { withTheme: true })(EditUserDialog);
