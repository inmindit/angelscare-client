import React from 'react';
import Button from 'material-ui/Button';
// import TextField from 'material-ui/TextField';
import {
    DialogActions,
    DialogContent,
    DialogTitle,
    DialogContentText,
} from 'material-ui/Dialog';
import { withStyles } from 'material-ui/styles/index';
import GenericDialog from './GenericDialog';
import styles from './configStyle';
// import PreviewServiceComponent from '../../components/PreviewServiceComponent';
import i18n from '../../services/i18n';

type Props = {
  // selectedEvent: Object,
  classes: Object,
  open: boolean,
  onClose: () => void
};

class LearnMoreDialog extends GenericDialog<Props> {
  onConfirm = () => {
    this.props.handleBooking();
  };

  // componentWillReceiveProps(nextProps: any) {
  //   if (nextProps.open === true) {
  //     this.setState({
  //       selectedEvent: nextProps.selectedEvent || ''
  //     });
  //   }
  // };

  renderTitle = () => (
    <DialogTitle>{this.props.selectedService && this.props.selectedService.title}</DialogTitle>
  );

  renderContent = () => (
    <DialogContent>
      <DialogContentText id="learMoreDialogContent">
        {this.props.selectedService && this.props.selectedService.description}
        </DialogContentText>
    </DialogContent>
  );

  renderActions = () => (
    <DialogActions>
      <Button
        data-tid="closeLearnMoreDialog"
        onClick={this.props.onClose}
      >
        {i18n.t('core:cancel')}
      </Button>
      <Button
        // disabled={this.state.disableConfirmButton}
        onClick={this.onConfirm}
        data-tid="confirmLearnMoreDialog"
        id="confirmLearnMoreDialog"
        color="primary"
      >
        {i18n.t('core:book')}
      </Button>
    </DialogActions>
  );
}

export default withStyles(styles)(LearnMoreDialog);
