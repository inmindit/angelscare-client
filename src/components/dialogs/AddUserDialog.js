import React from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import {
  DialogActions,
  DialogContent,
  DialogTitle,
  // DialogContentText,
} from 'material-ui/Dialog';
import { withStyles } from 'material-ui/styles/index';
import { FormControl, FormHelperText } from 'material-ui/Form';
import GenericDialog from './GenericDialog';
import styles from './configStyle';

type Props = {
  classes: Object,
  open: boolean,
  onClose: () => void
};

type State = {
  username: string,
  email: string,
};

class AddUserDialog extends GenericDialog<Props, State> {
  state = {
    username: '',
    email: '',
    errorUsername: false,
    errorEmailAddress: false,
  };

  // componentWillReceiveProps(nextProps: any) {
  //   if (nextProps.open === true) {
  //     this.setState({
  //       selectedEvent: nextProps.selectedEvent || ''
  //     });
  //   }
  // };

  onConfirm = () => {
    // console.log(this.props);
    // this.props.handleBooking();
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    }, this.handleValidation());
  };

  handleValidation() {
    // const pathRegex = '^((\.\./|[a-zA-Z0-9_/\-\\])*\.[a-zA-Z0-9]+)$';
    // const nameRegex = '^[A-Z][-a-zA-Z]+$';
    if (this.state.username.length > 0) {
      this.setState({ errorUsername: false, disableConfirmButton: false });
    } else {
      this.setState({ errorUsername: true, disableConfirmButton: true });
    }

    if (this.state.email.length > 6) {
      this.setState({ errorEmailAddress: false, disableConfirmButton: false });
    } else {
      this.setState({ errorEmailAddress: true, disableConfirmButton: true });
    }
  }

  renderTitle = () => (
    <DialogTitle>Create user</DialogTitle>
  );

  renderContent = () => (
    <DialogContent className={this.props.classes.root}>
      <FormControl
        fullWidth={true}
        error={this.state.errorUsername}
      >
        <TextField
          helperText="Enter your username"
          label="Username"
          name="username"
          onChange = {this.handleInputChange}
          fullWidth
        />
        {/*{this.state.errorUsername && <FormHelperText>Invalid username</FormHelperText>}*/}
      </FormControl>
      <FormControl
        fullWidth={true}
        // error={this.state.errorEmailAddress}
      >
        <TextField
          helperText="Enter Email Address"
          label="Email Address"
          name="emailAddress"
          onChange = {this.handleInputChange}
          fullWidth
        />
        {/*{this.state.errorEmailAddress && <FormHelperText>Invalid email address.</FormHelperText>}*/}
      </FormControl>
    </DialogContent>
  );

  renderActions = () => (
    <DialogActions>
      <Button
        data-tid="closeAddUserDialog"
        onClick={this.props.onClose}
      >
        Cancel
      </Button>
      <Button
        // disabled={this.state.disableConfirmButton}
        onClick={this.onConfirm}
        data-tid="confirmAddUserDialog"
        id="confirmAddUserDialog"
        color="primary"
      >
        Invite
      </Button>
    </DialogActions>
  );
}

export default (withStyles(styles, { withTheme: true })(AddUserDialog));
