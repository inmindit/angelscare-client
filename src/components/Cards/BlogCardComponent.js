import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import i18n from '../../services/i18n';
import { formatDate } from '../../../utils/misc';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  }
});

function BlogCardComponent(props) {
  const { classes, news } = props;

  if (news.youtube.length > 0) {
    return (
      <div>
        <div className="blog-card">
          <iframe
            className="photo photo1"
            height="200"
            src={news.youtube}
          >
          </iframe>
          <ul className="details">
            {/*<li className="author"><a href="#">John Doe</a></li>*/}
            <li className="date">{formatDate(news.created)}</li>
            <li className="tags">
              <ul>
                {news.tags && news.tags.map((tag) => (
                  <li><a href="#">{tag.name}</a></li>
                ))}
              </ul>
            </li>
          </ul>
          <div className="description">
            <h1>{news.title}</h1>
            {/*<h2>Opening a door to the future</h2>*/}
            <p className="summary">
              {news.description}
            </p>
            <a href="#">{i18n.t('core:readMore')}e</a>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <div>
        <div className="blog-card">
          <img
            className="photo photo1"
            src={news.image ? news.image : '/src/public/assets/img/image_placeholder.jpg'}
            height="200"
          />
          <ul className="details">
            {/*<li className="author"><a href="#">John Doe</a></li>*/}
            <li className="date">{formatDate(news.created)}</li>
            <li className="tags">
              <ul>
                {news.tags && news.tags.map((tag) => (
                  <li><a href="#">{tag.name}</a></li>
                ))}
              </ul>
            </li>
          </ul>
          <div className="description">
            <h1>{news.title}</h1>
            {/*<h2>Opening a door to the future</h2>*/}
            <p className="summary">
              {news.description}
            </p>
            <a href="#">{i18n.t('core:readMore')}</a>
          </div>
        </div>
      </div>
    );
  }
}

export default  withStyles(styles)(BlogCardComponent);
