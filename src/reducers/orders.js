import types from '../actions/types';

const INITIAL_STATE = {
  order: {},
  orders: {},
  providerOrders: {},
  message: '',
  error: ''
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_ORDER:
      return { ...state, order: action.payload };
    case types.FETCH_ORDERS:
      return { ...state, orders: action.payload };
    case types.REMOVE_ORDER:
      return { ...state, news: action.payload };
    case types.EDIT_ORDER:
      return { ...state, news: action.payload };
  }
  return state;
}

// Selectors
export const getOrder = (state) => state.news.order;
export const getAllOrders = (state) => state.news.orders;
export const getProviderOrders = (state) => state.news.providerOrders;
