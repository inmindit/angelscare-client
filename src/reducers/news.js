import types from '../actions/types';

const INITIAL_STATE = {
  news: [
/*    {
      _id: 'v132c13-caw12311-k2opk4ce-va2we123',
      title: 'A guide to the Google Play Console',
      date: '17 May',
      description: 'Whether you’re in a business or technical role, in a team of 1 or 100, ' +
      'the Play Console can help you with more than publishing. This tour of Play Console introduces its many features which ' +
      'can help you improve app quality and business performance.',
      image: 'https://cdn-images-1.medium.com/max/900/1*VRf8qf0oY8dxrdAfBFE3fg.png',
    },
    {
      _id: 'vt62c13-ca93ei11-k2opk4ce-va8we123',
      title: 'What you can do to prevent Alzheimer\'s | Lisa Genova',
      date: '7 April',
      description: 'Alzheimer\'s doesn\'t have to be your brain\'s destiny, says neuroscientist and author of "Still Alice," Lisa Genova. She shares the latest science investigating the disease -- and some promising research on what each of us can do to build an Alzheimer\'s-resistant brain.',
      youtube: 'https://www.youtube.com/embed/twG4mr6Jov0'
    },
    {
      _id: 'c1v673-123oei11-k2opk4ce-vb9iwe123',
      title: 'Alzheimer\'s Research UK\'s #ShareTheOrange with Bryan Cranston',
      date: '12 May',
      description: 'Help us share better understanding about dementia. Too many people still think dementia is just a natural part of ageing, which means they don\'t realise that it is something we could, one day, defeat.',
      youtube: 'https://www.youtube.com/embed/HvCBSGLD1HA'
    },
    {
      _id: 'pl22c13-19va2v4-k2opk4ce-ca0e123',
      title: 'Launchpad Accelerator Tel Aviv',
      date: '19 May',
      description: 'Launchpad Accelerator has just introduced new, standalone regional initiatives. These are kicking off with a presence in Lagos,' +
      ' Nigeria; São Paulo, Brazil; and Tel Aviv, Israel. Localized accelerators allow for customized programs, development of stronger relationships ' +
      'with partners on the ground, and further support for local startup ecosystems.',
      image: 'https://lh3.googleusercontent.com/sts8EtmNoD_v3ARCXwUyQNWi71JCiCpQiukbO4MzeLla2lVtZ_4ZjjsH_gqH6QldzM0k1gaX00QWmh_SQIHTJWqH45ktYVI=s688',
    }*/
  ],
  message: '',
  error: ''
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_NEWS:
      return { ...state, news: action.payload };
    case types.CREATE_NEWS:
      return { ...state, news: action.payload };
    case types.REMOVE_NEWS:
      return { ...state, news: action.payload };
    case types.EDIT_NEWS:
      return { ...state, news: action.payload };
    case types.FETCH_NEWSBYID:
      return { ...state, newsId: action.payload };
    // case types.FETCH_NEWS_PICTURE:
    //   return { ...state, services: action.payload };
  }
  return state;
}

// Selectors
export const getNews = (state) => state.news.news;
export const getNewsById = (state) => state.news.newsId;
