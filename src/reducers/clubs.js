import types from '../actions/types';

const INITIAL_STATE = {
  club: {},
  clubs: {},
  message: '',
  error: ''
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_CLUBS:
      return { ...state, clubs: action.payload };
    case types.CREATE_CLUBS:
      return { ...state, clubs: action.payload };
    case types.REMOVE_CLUBS:
      return { ...state, clubs: action.payload };
    case types.EDIT_CLUBS:
      return { ...state, clubs: action.payload };
    case types.FETCH_CLUBS_PICTURE:
      return { ...state, services: action.payload };
  }
  return state;
}

// Selectors
export const getClub = (state) => state.club;
export const getClubs = (state) => state.clubs;
