import types from '../actions/types';

const INITIAL_STATE = {
  message: '',
  error: '',
  customer: {}
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.CREATE_CUSTOMER:
      return { ...state, message: action.payload.message };
    case types.FETCH_CUSTOMER:
      return { ...state, customer: action.payload.customer };
    case types.CANCEL_SUBSCRIPTION:
      return { ...state, message: action.payload.message };
    case types.CHANGE_SUBSCRIPTION:
      return { ...state, message: action.payload.message };
    case types.UPDATE_BILLING:
      return { ...state, message: action.payload.message };
    case types.BILLING_ERROR:
      return { ...state, error: action.payload };
  }

  return state;
}
