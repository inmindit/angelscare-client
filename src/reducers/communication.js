import types from '../actions/types';

const INITIAL_STATE = {
  conversations: [],
  message: '',
  messages: [],
  recipients: [],
  error: ''
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_CONVERSATIONS:
      return { ...state, conversations: action.payload.conversations };
    case types.FETCH_SINGLE_CONVERSATION:
      return { ...state, messages: action.payload.conversation };
    case types.FETCH_RECIPIENTS:
      return { ...state, recipients: action.payload.recipients };
    case types.START_CONVERSATION:
      return { ...state, message: action.payload.message };
    case types.SEND_REPLY:
      return { ...state, message: action.payload.message };
    case types.SEND_CONTACT_FORM:
      return { ...state, message: action.payload.message };
    case types.CHAT_ERROR:
      return { ...state, error: action.payload };
  }

  return state;
}
