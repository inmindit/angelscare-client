import { combineReducers } from 'redux';
import appReducer from './app';
import authReducer from './auth';
import userReducer from './user';
import newsReducer from './news';
import clubsReducer from './clubs';
import communicationReducer from './communication';
import customerReducer from './customer';
import dialogReducer from './dialogs';

const rootReducer = combineReducers({
  app: appReducer,
  auth: authReducer,
  user: userReducer,
  news: newsReducer,
  clubs: clubsReducer,
  communication: communicationReducer,
  customer: customerReducer,
  dialogs: dialogReducer,
});

export default rootReducer;
