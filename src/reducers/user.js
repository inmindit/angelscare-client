import types from '../actions/types';

const INITIAL_STATE = {
  profile: {},
  message: '',
  error: '',
  users: [],
  service: {},
  services: [],
  order: {},
  orders: [],
  servicesCategories: [],
  serviceTypes: {}
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_USER:
      return { ...state, profile: action.payload };
    case types.FETCH_ALL_USERS:
      return { ...state, users: action.payload };
    case types.FETCH_SERVICE:
      return { ...state, service: action.payload };
    case types.FETCH_SERVICES:
      return { ...state, services: action.payload };
    case types.FETCH_SERVICES_CATEGORIES:
      return { ...state, servicesCategories: action.payload };
    case types.FETCH_SERVICES_TYPES:
      return { ...state, serviceTypes: action.payload };
    case types.GET_SERVICES_TYPES:
      return { ...state, servicesCategories: action.payload };
    case types.DELETE_SERVICES_TYPES:
      return { ...state, servicesCategories: action.payload };
    case types.FETCH_MAIN_SERVICE_TYPE:
      return { ...state, allServicesTypes: action.payload };
    case types.FETCH_CURRENT_SERVICE:
      return { ...state, currentService: action.payload };
    case types.FETCH_ORDER:
      return { ...state, service: action.payload };
    case types.FETCH_ORDERS:
      return { ...state, services: action.payload };
    case types.REMOVE_ORDER:
      return { ...state, services: action.payload };
    case types.ERROR_RESPONSE:
      return { ...state, error: action.payload };
  }
  return state;
}

// Selectors
export const getService = (state) => state.user.service;
export const getServices = (state) => state.user.services;
export const getAdminServices = (state) => state.user.services;
export const getServicesCategories = (state) => state.user.servicesCategories;
export const getCurrentService = (state) => state.user.currentService;
export const getServicesTypes = (state) => state.user.serviceTypes;
export const getAllServicesTypes = (state) => state.user.allServicesTypes;
export const getOrder = (state) => state.user.order;
export const getOrders = (state) => state.user.orders;
