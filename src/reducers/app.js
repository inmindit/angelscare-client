import { red, blue, green } from '@material-ui/core/colors';
import types from '../actions/types';
import i18n from '../services/i18n';
import { fetchLanguage } from '../actions/index';
import { fetchCurrentServices } from '../actions/services';
import { countrySuggestions } from "../components/Config";

const INITIAL_STATE = {
  isOnline: false,
  notificationStatus: {
    visible: false,
    text: 'Test',
    notificationType: '',
    autohide: false
  },
  searchResults: [],
  lastSelectedEntry: null,
  settingsVersion: 1,
  isLoading: false,
  error: null,
  userId: null,
  loggedIn: false,
  contentHash: '',
  isUpdateInProgress: false,
  isUpdateAvailable: false,
  checkForUpdates: true,
  showMainMenu: false,
  interfaceLanguage: 'en',
  userCountry: '',
  currentTheme: 'light',
  enableGlobalKeyboardShortcuts: true,
  supportedThemes: ['light', 'dark'],
  displayWidth: '',
  supportedCountries: countrySuggestions,
  supportedLanguages: [
    {
      iso: 'en',
      title: 'English'
    },
    {
      iso: 'de',
      title: 'Deutsch (German)'
    },
    {
      iso: 'bg',
      title: 'Български (Bulgarian)'
    },
  ],
  mainSlider: [
    {
      uuid: 'c1323c13-ca4woe1i11-k2opk4ce-v1iw3e123',
      title: 'TRAVELING AFTER RETIREMENT',
      description: '',
      button: 'READ MORE',
      image: '/src/public/assets/img/main/slider_0_travel.jpg',
      style: {
        content: 'slick-content-1',
        headline: 'slick-content-headline-1',
        description: 'slick-description-1',
        button: true
      }
    },
    {
      uuid: 'c1372c13-ca1wo2ei11-k2opk4ce-v9iw2e123',
      title: 'What do you have planned for your next vacation?',
      description: '',
      button: 'SEE MORE CLEARLY',
      image: '/src/public/assets/img/main/slider_1_travel.jpg',
      style: {
        content: 'slick-content-2',
        headline: 'slick-content-headline-2',
        description: 'slick-description-2',
        button: true
      }
    },
    {
      uuid: 'c1932c13-ca4wo1ei11-k2opk4ce-vai123we123',
      title: 'HEALTH PREVENTION',
      description: '',
      button: 'SEE MORE',
      image: '/src/public/assets/img/main/slider_2_health_prevention.jpg',
      style: {
        content: 'slick-content-3',
        headline: 'slick-content-headline-3',
        description: 'slick-description-3',
        button: true
      }
    },
    {
      uuid: 'c1320c13-ca4wo6ei11-k2opk4ce-98w2312e123',
      title: 'FORMAL CARE BY SERVICE PROVIDERS AND INFORMAL CARE BY RELATIVES',
      button: 'Button',
      description: '',
      image: '/src/public/assets/img/main/slider_3_caretaker.jpg',
      style: {
        content: 'slick-content-4',
        headline: 'slick-content-headline-4',
        description: 'slick-description-4',
        button: true
      }
    },
    {
      uuid: 'c1320c13-ca4wo6ei11-k2opk4ce-ai122312e123',
      title: 'CAREGIVING',
      button: 'Button',
      description: '',
      image: '/src/public/assets/img/main/slider_4_caretaker.jpg'
    },
    {
      uuid: 'c1320c13-ca4wo6ei11-k2opk4ce-v1iw2312e123',
      title: 'HEALTH PREVENTION',
      button: 'Button',
      description: '',
      image: '/src/public/assets/img/main/slider_5_health.jpg'
    },
    {
      uuid: 'c1320c13-ca32o6ei11-k2opk4ce-v1iw2312e123',
      title: 'SOCIAL LIFE & RETIREMENT CLUB',
      button: 'Button',
      description: '',
      image: '/src/public/assets/img/main/slider_6_retirement_club.jpg'
    },
    {
      uuid: 'c1caw0c13-ca4wo6ei11-k2opk4ce-v1iw2312e123',
      // title: 'RETIREMENT PLAN',
      // button: 'Button',
      description: '',
      image: '/src/public/assets/img/main/slider_7_retirement_plan.jpg'
    }
  ],
  dataSlide: [
    {
      uuid: 'c131213-ca4wo4211-k2opk4ce-ai12422e13',
      media: 'http://www.icons101.com/icon_png/size_256/id_79394/youtube.png',
      mediaBackgroundStyle: { backgroundColor: red[400] },
      style: { backgroundColor: red[600] },
      title: 'This is a very cool feature',
      subtitle: 'Just using this will blow your mind.'
    },
    {
      uuid: 'c153c13-v1awe1i11-k2opk4ce-ai1212e123',
      media: 'http://www.icons101.com/icon_png/size_256/id_80975/GoogleInbox.png',
      mediaBackgroundStyle: { backgroundColor: blue[400] },
      style: { backgroundColor: blue[600] },
      title: 'Ever wanted to be popular?',
      subtitle: 'Well just mix two colors and your are good to go!'
    },
    {
      uuid: 'vr3c13-v1awv8711-k2op24ce-ai121vaw23',
      media: 'http://www.icons101.com/icon_png/size_256/id_76704/Google_Settings.png',
      mediaBackgroundStyle: { backgroundColor: green[400] },
      style: { backgroundColor: green[600] },
      title: 'May the force be with you',
      subtitle: 'The Force is a metaphysical and ubiquitous power in the Star Wars fictional universe.'
    },
  ]
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.DEVICE_ONLINE: {
      return { ...state, isOnline: true, error: null };
    }
    case types.DEVICE_OFFLINE: {
      return { ...state, isOnline: false, error: null };
    }
    case types.SET_LANGUAGE: {
      i18n.changeLanguage(action.language);
      return { ...state, interfaceLanguage: action.language };
    }
    case types.SET_COUNTRY: {
      return { ...state, userCountry: action.country };
    }
    case types.SET_DISPLAYSIZING: {
      return { ...state, displaySizing: action.sizing };
    }
    case types.FETCH_SERVICES_LANGUAGES: {
      return {
        ...state,
        serviceSupportedLanguages: action.payload
      }
    }
    case types.SET_NOTIFICATION: {
      return {
        ...state,
        notificationStatus: {
          visible: action.visible,
          text: action.text,
          notificationType: action.notificationType,
          autohide: action.autohide
        }
      };
    }
  }

  return state;
}

export const action = {
  showNotification: (
    text: string,
    notificationType: string = 'default',
    autohide: boolean = false
  ) => ({
    type: types.SET_NOTIFICATION,
    visible: true,
    text,
    notificationType,
    autohide
  }),
  hideNotifications: () => ({
    type: types.SET_NOTIFICATION,
    visible: false,
    text: null,
    notificationType: 'default',
    autohide: true
  }),
  setLanguage: (language: string) => ({ type: types.SET_LANGUAGE, language }),
  setCountry: (country: string) => ({ type: types.SET_COUNTRY, country }),
  setLandLanguage: (languageISO: string) => (
    dispatch: (language: string) => void
  ) => {
    fetchLanguage('POST', { language: languageISO});
    dispatch(action.setLanguage(languageISO));
  },
  setUserCountry: (country: string) => (
    dispatch: (country: string) => void
  ) => {
    fetchLanguage('POST', { country: country});
    dispatch(action.setCountry(country));
  },
  setDisplaySizing: (sizing) => ({ type: types.SET_DISPLAYSIZING, sizing }),
};

// Selectors
export const getAppSettings = (state: Object) => state.app;
export const isOnline = (state: Object) => state.app.isOnline;
export const getNotificationStatus = (state: Object) => state.app.notificationStatus;
export const getDisplaySizing = (state: Object) => state.app.displayWidth;
export const getServicesLanguages = (state) => state.app.serviceSupportedLanguages;
export const getCurrentUserCountry = (state: Object) => state.app.userCountry;
export const getCurrentLanguage = (state: Object) => state.app.interfaceLanguage;
export const getSupportedLanguages = (state: Object) => state.app.languages;
export const getSupportedCountries = (state: Object) => state.app.supportedCountries;
export const getMainSliderData = (state: Object) => state.app.mainSlider;
export const getMainDataSlide = (state: Object) => state.app.dataSlide;
