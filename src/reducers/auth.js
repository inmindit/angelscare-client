import types from '../actions/types';
import cookie from "react-cookie";

const INITIAL_STATE = {
  error: '',
  message: '',
  content: '',
  isAuthenticated: false,
  user: {}
};

const user = cookie.load('user');

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.AUTH_USER:
      return { ...state, error: '', message: '', isAuthenticated: true, user: user };
    case types.UNAUTH_USER:
      return { ...state, isAuthenticated: false, error: action.payload };
    case types.AUTH_ERROR:
      return { ...state, error: action.payload };
    case types.FORGOT_PASSWORD_REQUEST:
      return { ...state, message: action.payload.message };
    case types.RESET_PASSWORD_REQUEST:
      return { ...state, message: action.payload.message };
    case types.PROTECTED_TEST:
      return { ...state, content: action.payload.message };
  }

  return state;
}

// Selectors

export const getUser = (state) => state.auth.user;
export const isAuthenticated = (state) => state.auth.isAuthenticated;
export const getErrorMessage = (state) => state.auth.error;
export const getContent = (state) => state.auth.content;
