export default {
  settingsVersion: 1,
  isLoading: false,
  error: null,
  userId: null,
  loggedIn: false,
  isOnline: false,
  contentHash: '',
  isUpdateInProgress: false,
  isUpdateAvailable: false,
  checkForUpdates: true,
  coloredFileExtension: false,
  showMainMenu: false,
  interfaceLanguage: 'en',
  currentTheme: 'light',
  enableGlobalKeyboardShortcuts: true,
  supportedThemes: ['light', 'dark'],
  supportedLanguages: [
    {
      iso: 'en',
      title: 'English'
    },
    {
      iso: 'de',
      title: 'Deutsch (German)'
    },
    {
      iso: 'bg',
      title: 'Български (Bulgarian)'
    },
  ]
};
